package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.pipelinetech.dedalo.entities.Mantenimiento;

public interface IMantenimientoRepository extends  JpaRepository<Mantenimiento, Integer> {

}
