package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.ControlXVuelo;
@Repository
public interface ControlXVueloRepository extends JpaRepository<ControlXVuelo, Integer> {

	List<ControlXVuelo> findAllByVuelo(String vuelo);

}
