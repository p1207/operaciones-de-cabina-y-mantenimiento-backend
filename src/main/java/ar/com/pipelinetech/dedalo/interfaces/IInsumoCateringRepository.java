package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.InsumoCatering;

@Repository
public interface IInsumoCateringRepository extends JpaRepository<InsumoCatering, Integer>{
	
	List<InsumoCatering> findAllByTipoMenu (int tipoMenu);
}