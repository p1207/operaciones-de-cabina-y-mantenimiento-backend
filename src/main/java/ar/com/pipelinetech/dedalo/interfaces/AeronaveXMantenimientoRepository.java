package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.AeronaveXMantenimiento;

@Repository
public interface AeronaveXMantenimientoRepository extends  JpaRepository<AeronaveXMantenimiento, Integer> {
	public List<AeronaveXMantenimiento> findAllByMatricula(String matricula);
}
