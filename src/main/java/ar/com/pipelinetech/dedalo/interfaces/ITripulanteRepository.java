package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.Tripulante;

@Repository
public interface ITripulanteRepository extends JpaRepository<Tripulante, Integer> {

}
