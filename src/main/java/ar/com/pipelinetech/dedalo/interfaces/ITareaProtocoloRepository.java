package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.TareaProtocolo;

@Repository
public interface ITareaProtocoloRepository extends JpaRepository<TareaProtocolo, Integer>{

	List<TareaProtocolo> findAllByIdProtocolo(Integer idProtocolo);

}
