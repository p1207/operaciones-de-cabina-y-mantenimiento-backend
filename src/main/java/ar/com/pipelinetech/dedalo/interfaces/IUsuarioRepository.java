package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.Usuario;

@Repository
public interface IUsuarioRepository extends  JpaRepository<Usuario, Integer> {
	
	Usuario findByUsername(String username);
}
