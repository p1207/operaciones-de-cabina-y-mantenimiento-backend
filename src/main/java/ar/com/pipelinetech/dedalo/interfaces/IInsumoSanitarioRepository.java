package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.InsumoSanitario;

@Repository
public interface IInsumoSanitarioRepository extends JpaRepository<InsumoSanitario, Integer>{

}
