package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.InsumoVuelo;

@Repository
public interface IInsumoVueloRepository extends JpaRepository<InsumoVuelo, Integer>{

	@Query("SELECT i FROM InsumoVuelo i WHERE i.vuelo = :vuelo AND i.idInsumo in :ids")
	public List<InsumoVuelo> findAllByVuelo(@Param("vuelo") String vuelo, @Param("ids") List<Integer> ids );

	@Query("SELECT SUM(cantidadFinal) FROM InsumoVuelo i WHERE i.vuelo = :vuelo")
	public Integer obtenerCantidadFinalInsumos(@Param("vuelo") String vuelo);
	
	public List<InsumoVuelo> findAllByVuelo(String vuelo);

}
