package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.Emergencia;

@Repository
public interface IEmergenciaRepository extends JpaRepository<Emergencia, Integer>{
	
	List<Emergencia> findAllByIdVuelo(String idVuelo);

}
