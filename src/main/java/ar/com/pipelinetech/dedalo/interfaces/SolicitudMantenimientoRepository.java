package ar.com.pipelinetech.dedalo.interfaces;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.SolicitudMantenimiento;
@Repository
public interface SolicitudMantenimientoRepository extends  JpaRepository<SolicitudMantenimiento, Integer>{

	public List<SolicitudMantenimiento> findAllById(int id);

	@Query("SELECT s FROM SolicitudMantenimiento s WHERE s.tecnico.id = ?1")
	public List<SolicitudMantenimiento> findSolicitudesByIdTecnico(Integer id);
	
	public List<SolicitudMantenimiento> findAllByMatricula(String matricula);
	
}
