package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ar.com.pipelinetech.dedalo.entities.Notificacion;

@Repository
public interface INotificacionRepository extends JpaRepository<Notificacion, Integer>{

	List<Notificacion> findAllByIdUsuario(Integer idUsuario);
}
