package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.com.pipelinetech.dedalo.entities.TareaControl;

public interface ITareaControlRepository extends JpaRepository<TareaControl, Integer>{

}
