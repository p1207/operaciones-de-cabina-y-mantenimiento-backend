package ar.com.pipelinetech.dedalo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.Tarea;

@Repository
public interface ITareaRepository extends JpaRepository<Tarea, Integer> {

	public List<Tarea> findAllByMantenimiento(int mantenimiento);

}
