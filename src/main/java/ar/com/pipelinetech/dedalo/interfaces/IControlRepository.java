package ar.com.pipelinetech.dedalo.interfaces;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ar.com.pipelinetech.dedalo.entities.Control;


public interface IControlRepository extends  JpaRepository<Control, Integer> {

	@Query("SELECT c FROM Control c WHERE c.activo = ?1")
	public List<Control> findAllActivos(Boolean activo);
}
