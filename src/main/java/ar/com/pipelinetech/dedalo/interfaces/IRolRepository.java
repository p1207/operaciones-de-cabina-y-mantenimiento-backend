package ar.com.pipelinetech.dedalo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.pipelinetech.dedalo.entities.Rol;

@Repository
public interface IRolRepository extends JpaRepository<Rol, Integer> {

	Rol findByNombre(String nombre);
}
