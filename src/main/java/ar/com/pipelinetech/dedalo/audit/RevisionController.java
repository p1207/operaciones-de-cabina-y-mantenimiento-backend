package ar.com.pipelinetech.dedalo.audit;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/Auditoria")
public class RevisionController {

	@Autowired
	private RevisionService revisionService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<RevisionDTO>> getHabilidad() {
		List<RevisionDTO> result = revisionService.getRevisiones();
		ResponseEntity<List<RevisionDTO>> response = new ResponseEntity<List<RevisionDTO>>(result,HttpStatus.OK);
		return response;
	}

}
