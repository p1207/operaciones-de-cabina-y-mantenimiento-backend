package ar.com.pipelinetech.dedalo.audit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRevisonRepository extends JpaRepository<Revision, Integer>{

}
