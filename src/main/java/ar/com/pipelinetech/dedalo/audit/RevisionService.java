package ar.com.pipelinetech.dedalo.audit;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RevisionService {
	@Autowired
	private IRevisonRepository revisionRepository;

	public List<RevisionDTO> getRevisiones() {
		List<Revision> revisiones = revisionRepository.findAll();
		List<RevisionDTO> revisionesDTO = RevisionAssembler.listEntityToDTOList(revisiones);
		return revisionesDTO;
	}

}
