package ar.com.pipelinetech.dedalo.audit;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RevisionDTO {
	
	private int id;
	private Date date;
	private String user;
	private String ip;

	@JsonProperty("Id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("Date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@JsonProperty("User")
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@JsonProperty("Ip")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
