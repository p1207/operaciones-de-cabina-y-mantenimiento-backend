package ar.com.pipelinetech.dedalo.audit;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class RevisionAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static RevisionDTO toDto(Revision revision) {
		RevisionDTO revisionDTO = mapper.map(revision, RevisionDTO.class);
		return revisionDTO;
	}

	public static Revision toEntity(RevisionDTO revisionDTO) {
		Revision revision = mapper.map(revisionDTO, Revision.class);
		return revision;
	}
	
	public static List<Revision> listDTOToEntityList(List<RevisionDTO> revisionesDTO) {
		List<Revision> revisiones = new ArrayList<Revision>();
		for(RevisionDTO r : revisionesDTO) {
			Revision revision = toEntity(r);
			revisiones.add(revision);
		}	
		return revisiones;
	}
	
	public static List<RevisionDTO> listEntityToDTOList(List<Revision> revisiones) {
		List<RevisionDTO> revisionesDTO = new ArrayList<RevisionDTO>();
		for(Revision t : revisiones) {
			RevisionDTO revisionDTO = toDto(t);
			revisionesDTO.add(revisionDTO);
		}	
		return revisionesDTO;
	}

}