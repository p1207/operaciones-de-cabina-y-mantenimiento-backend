package ar.com.pipelinetech.dedalo.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;

public class ControlXVueloDTO {

	private Integer id;
	private Integer id_control;
	private String vuelo;
	private Date fecha_creacion;
	private String observacion;
	private EstadoControlXVuelo estado;
	private ControlDTO control;
	
	public ControlXVueloDTO() {
	
	}
	public ControlXVueloDTO(Integer id, Integer id_control, String id_vuelo, Date fecha_creacion, String observacion, EstadoControlXVuelo estado, ControlDTO control) {
		super();
		this.id = id;
		this.id_control = id_control;
		this.vuelo = id_vuelo;
		this.fecha_creacion = fecha_creacion;
		this.observacion = observacion;
		this.setEstado(estado);
		this.control = control;
	}

	
	@Override
	public String toString() {
		return "ControlXVueloDTO [id=" + id + ", id_control=" + id_control + ", id_vuelo=" + vuelo + ", fecha_creacion=" + fecha_creacion + ", observacion=" + observacion + "]";
	}
	
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}


	@JsonProperty("ControlId")
	public Integer getId_control() {
		return id_control;
	}
	public void setId_control(Integer id_control) {
		this.id_control = id_control;
	}

	@JsonProperty("FlightId")
	public String getVuelo() {
		return vuelo;
	}
	public void setVuelo(String id_vuelo) {
		this.vuelo = id_vuelo;
	}

	@JsonProperty("CreationDate")
	public Date getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	@JsonProperty("Observation")
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	@JsonProperty("Status")
	public EstadoControlXVuelo getEstado() {
		return estado;
	}
	public void setEstado(EstadoControlXVuelo estado) {
		this.estado = estado;
	}
	
	@JsonProperty("Control")
	public ControlDTO getControl() {
		return control;
	}
	public void setControl(ControlDTO control) {
		this.control = control;
	}
	
	
}
