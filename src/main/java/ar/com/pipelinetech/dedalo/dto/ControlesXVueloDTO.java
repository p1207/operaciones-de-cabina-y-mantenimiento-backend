package ar.com.pipelinetech.dedalo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ControlesXVueloDTO {
	
	private boolean aprobados;
	private List<ControlXVueloDTO> controles;
	
	public ControlesXVueloDTO() {
		
	}
	
	public ControlesXVueloDTO(boolean aprobados, List<ControlXVueloDTO> controles) {
		super();
		this.aprobados = aprobados;
		this.controles = controles;
	}

	@JsonProperty("Approved")
	public boolean isAprobados() {
		return aprobados;
	}
	
	public void setAprobados(boolean aprobados) {
		this.aprobados = aprobados;
	}
	
	@JsonProperty("AllControls")
	public List<ControlXVueloDTO> getControles() {
		return controles;
	}
	public void setControles(List<ControlXVueloDTO> controles) {
		this.controles = controles;
	}
	
	
}
