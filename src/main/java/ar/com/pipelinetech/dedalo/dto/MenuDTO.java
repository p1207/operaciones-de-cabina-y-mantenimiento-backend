package ar.com.pipelinetech.dedalo.dto;

import java.util.ArrayList;
import java.util.List;

public class MenuDTO {
	
	private List<String> estandarAsientos;
	private List<String> celiacoAsientos;
	private List<String> vegetarianoAsientos;
	private List<String> veganoAsientos;
	
	public MenuDTO() {
		this.estandarAsientos = new ArrayList<String>();
		this.celiacoAsientos = new ArrayList<String>();
		this.vegetarianoAsientos = new ArrayList<String>();
		this.veganoAsientos = new ArrayList<String>();
	}
	
	public List<String> getEstandarAsientos() {
		return estandarAsientos;
	}
	public void setEstandarAsientos(List<String> estandarAsientos) {
		this.estandarAsientos = estandarAsientos;
	}
	public void addEstandarAsientos(String asiento) {
		this.estandarAsientos.add(asiento);
	}
	
	public List<String> getCeliacoAsientos() {
		return celiacoAsientos;
	}
	public void setCeliacoAsientos(List<String> celiacoAsientos) {
		this.celiacoAsientos = celiacoAsientos;
	}
	public void addCeliadoAsientos(String asiento) {
		this.celiacoAsientos.add(asiento);
	}
	
	
	public List<String> getVegetarianoAsientos() {
		return vegetarianoAsientos;
	}
	public void setVegetarianoAsientos(List<String> vegetarianoAsientos) {
		this.vegetarianoAsientos = vegetarianoAsientos;
	}
	public void addVegetarianoAsientos(String asiento) {
		this.vegetarianoAsientos.add(asiento);
	}
	
	
	public List<String> getVeganoAsientos() {
		return veganoAsientos;
	}
	public void setVeganoAsientos(List<String> veganoAsientos) {
		this.veganoAsientos = veganoAsientos;
	}
	public void addVeganoAsientos(String asiento) {
		this.veganoAsientos.add(asiento);
	}
	
}
