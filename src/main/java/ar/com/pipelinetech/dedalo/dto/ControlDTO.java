package ar.com.pipelinetech.dedalo.dto;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.pipelinetech.dedalo.enums.TipoControl;


public class ControlDTO{
	
	private Integer id;
	private String nombre;
	private boolean activo;
	private TipoControl tipoControl;
	private String descripcion;
	private RolDTO rol;
	private Integer idRol;
	private List<TareaControlDTO> tareas;
	private List<InsumoMantenimientoDTO> insumos;
	private String idVueloSeleccionado;
	
	
	public ControlDTO() {
	}

	public ControlDTO(Integer id, String nombre, boolean activo, TipoControl tipoControl, String descripcion, RolDTO rol) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activo = activo;
		this.tipoControl = tipoControl;
		this.descripcion = descripcion;
		this.rol = new RolDTO();
	}
	
	@Override
	public String toString() {
		return "ControlDTO [id=" + id + ", nombre=" + nombre + ", activo=" + activo + ", tipoControl=" + tipoControl
				+ ", descripcion=" + descripcion + "]";
	}
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Active")
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@JsonProperty("Type")
	public TipoControl getTipoControl() {
		return tipoControl;
	}

	public void setTipoControl(TipoControl tipoControl) {
		this.tipoControl = tipoControl;
	}

	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JsonProperty("Role")
	public RolDTO getRol() {
		return rol;
	}
	
	public void setRol(RolDTO rol) {
		this.rol = rol;
	}

	@JsonProperty("RoleId")
	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	@JsonProperty("Tasks")
	public List<TareaControlDTO> getTareas() {
		return tareas;
	}

	public void setTareas(List<TareaControlDTO> tareas) {
		this.tareas = tareas;
	}

	@JsonProperty("Supplies")
	public List<InsumoMantenimientoDTO> getInsumos() {
		return insumos;
	}

	public void setInsumos(List<InsumoMantenimientoDTO> insumos) {
		this.insumos = insumos;
	}

	@JsonProperty("FligthId")
	public String getIdVueloSeleccionado() {
		return idVueloSeleccionado;
	}

	public void setIdVueloSeleccionado(String idVueloSeleccionado) {
		this.idVueloSeleccionado = idVueloSeleccionado;
	}
	
	

}
