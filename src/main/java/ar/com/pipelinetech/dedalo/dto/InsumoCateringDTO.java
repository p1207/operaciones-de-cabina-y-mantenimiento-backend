package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsumoCateringDTO extends InsumoDTO{
	
	private int tipoMenu;

	public InsumoCateringDTO() {
		
	}

	public InsumoCateringDTO( int tipoMenu) {
		super();
		this.tipoMenu = tipoMenu;
	}

	@JsonProperty("MenuType")
	public int getTipoMenu() {
		return tipoMenu;
	}

	public void setTipoMenu(int tipoMenu) {
		this.tipoMenu = tipoMenu;
	}
}
