package ar.com.pipelinetech.dedalo.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TecnicoDTO extends UsuarioDTO{

	private BigDecimal valorModulo;
	

	public TecnicoDTO() {
	}

	public TecnicoDTO(BigDecimal valorModulo) {
		super();
		this.valorModulo = valorModulo;
	}

	@JsonProperty("ModuleValue")
	public BigDecimal getValorModulo() {
		return valorModulo;
	}

	public void setValorModulo(BigDecimal valorModulo) {
		this.valorModulo = valorModulo;
	}
}
