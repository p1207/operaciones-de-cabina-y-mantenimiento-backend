package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsumoSanitarioDTO extends InsumoDTO {
	
	private int categoriaSanitario;
	
	
	public InsumoSanitarioDTO() {
		
	}

	public InsumoSanitarioDTO(int categoriaSanitario) {
		super();
		this.categoriaSanitario = categoriaSanitario;
	}

	@JsonProperty("SanitaryType")
	public int getCategoriaSanitario() {
		return categoriaSanitario;
	}

	public void setCategoriaSanitario(int categoriaSanitario) {
		this.categoriaSanitario = categoriaSanitario;
	}
}
