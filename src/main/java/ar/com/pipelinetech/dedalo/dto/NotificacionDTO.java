package ar.com.pipelinetech.dedalo.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.pipelinetech.dedalo.enums.NotificacionPrioridad;

public class NotificacionDTO {
	
	private Integer id;
	private Date fecha;
	private NotificacionPrioridad tipo;
	private String asunto;
	private String mensaje;
	private Integer idUsuario;
	
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@JsonProperty("Date")
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@JsonProperty("Priority")
	public NotificacionPrioridad getTipo() {
		return tipo;
	}
	public void setTipo(NotificacionPrioridad tipo) {
		this.tipo = tipo;
	}

	@JsonProperty("Subject")
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	@JsonProperty("Message")
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@JsonProperty("User")
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	

}
