package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TareaControlDTO {


	private Integer id;
	private String descripcion;
	private Integer control;
	private Integer rol;
	private RolDTO role;
	
	public TareaControlDTO() {
		
	}
	public TareaControlDTO(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}
	
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JsonProperty("Control")
	public Integer getControl() {
		return control;
	}
	
	public void setControl(Integer control) {
		this.control = control;
	}
	
	@JsonProperty("RoleId")
	public Integer getRol() {
		return rol;
	}
	
	public void setRol(Integer rol) {
		this.rol = rol;
	}
	
	@JsonProperty("Role")
	public RolDTO getRole() {
		return role;
	}

	public void setRole(RolDTO role) {
		this.role = role;
	}
	
	
}
