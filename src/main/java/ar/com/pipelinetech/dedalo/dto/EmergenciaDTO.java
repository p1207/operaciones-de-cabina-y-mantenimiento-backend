package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;



public class EmergenciaDTO {
	
	private Integer id;
	private String nombre;
	private Integer prioridad;
	private String observacion;
	private TipoEmergenciaDTO tipoEmergencia;
	private String idVuelo;
	private String tripulante;
	
	public EmergenciaDTO () {
	}


	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Priority")
	public Integer getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	@JsonProperty("Observation")
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@JsonProperty("EmergencyType")
	public TipoEmergenciaDTO getTipoEmergencia() {
		return tipoEmergencia;
	}
	public void setTipoEmergencia(TipoEmergenciaDTO tipoEmergenciaDTO) {
		this.tipoEmergencia = tipoEmergenciaDTO;
	}

	@JsonProperty("FlightId")
	public String getIdVuelo() {
		return idVuelo;
	}
	public void setIdVuelo(String idVuelo) {
		this.idVuelo = idVuelo;
	}

	@JsonProperty("CrewMember")
	public String getTripulante() {
		return tripulante;
	}
	public void setTripulante(String tripulante) {
		this.tripulante = tripulante;
	}



}
