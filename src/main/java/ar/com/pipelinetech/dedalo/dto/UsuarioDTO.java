package ar.com.pipelinetech.dedalo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsuarioDTO {

	private Integer id;
	private String nombre;
	private String apellido;
	private String email;
	private String username;
	private String contraseña;
	private RolDTO rol;
	private Integer idTripulacion; //hacer refactor y pasarlo a tripulante como herencia
	private List<HabilidadDTO> habilidades;
	

	public UsuarioDTO(Integer id, String nombre, String apellido, String email, String username, String contraseña,
			RolDTO rol, Integer idTripulacion,  List<HabilidadDTO> habilidades, String dtype) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.username = username;
		this.contraseña = contraseña;
		this.rol = rol;
		this.idTripulacion = idTripulacion;
		this.habilidades = habilidades;
		//this.dtype = dtype;	
	}

	public UsuarioDTO() {
		
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Surname")
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@JsonProperty("Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("Username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("Password")
	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@JsonProperty("Role")
	public RolDTO getRol() {
		return rol;
	}

	public void setRol(RolDTO rol) {
		this.rol = rol;
	}

	@JsonProperty("CrewId")
	public Integer getIdTripulacion() {
		return idTripulacion;
	}

	public void setIdTripulacion(Integer idTripulacion) {
		this.idTripulacion = idTripulacion;
	}
	
	@JsonProperty("Abilities")
	public List<HabilidadDTO> getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(List<HabilidadDTO> habilidades) {
		this.habilidades = habilidades;
	}

	@Override
	public String toString() {
		return "UsuarioDTO [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email
				+ ", username=" + username + ", contraseña=" + contraseña + ", rol=" + rol + ", idTripulacion="
				+ idTripulacion + "]";
	}
	
}
