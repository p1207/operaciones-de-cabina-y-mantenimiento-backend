package ar.com.pipelinetech.dedalo.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ProtocoloDTO {
	
	private Integer id;
	private String nombre;
	private List<TareaProtocoloDTO> tareas;
	
	public ProtocoloDTO() {
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Tasks")
	public List<TareaProtocoloDTO> getTareas() {
		return tareas;
	}

	public void setTareas(List<TareaProtocoloDTO> tareas) {
		this.tareas = tareas;
	}
	

	

}
