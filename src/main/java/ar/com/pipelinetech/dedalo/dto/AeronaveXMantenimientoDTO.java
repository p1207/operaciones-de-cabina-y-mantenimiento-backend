package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AeronaveXMantenimientoDTO {
	
	private Integer id;
	private String matricula;
	private MantenimientoDTO mantenimiento;
	
	public AeronaveXMantenimientoDTO() {
		
	}
	
	public AeronaveXMantenimientoDTO(Integer id, String matricula, MantenimientoDTO mantenimiento) {
		super();
		this.id = id;
		this.matricula = matricula;
		this.mantenimiento = mantenimiento;
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Aircraft")
	public String getMatricula() {
		return matricula;
	}
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	@JsonProperty("Maintenance")
	public MantenimientoDTO getMantenimiento() {
		return mantenimiento;
	}
	
	public void setMantenimiento(MantenimientoDTO mantenimiento) {
		this.mantenimiento = mantenimiento;
	}
	
}
