package ar.com.pipelinetech.dedalo.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsumoVentaDTO extends InsumoDTO {

	private BigDecimal precioVentaPesos;
	private int categoriaVentas;
	private BigDecimal precioVentaDolar;
	
	public InsumoVentaDTO() {

	}
	
	public InsumoVentaDTO(BigDecimal precioVentaPesos, int categoriaVentas) {
		super();
		this.precioVentaPesos = precioVentaPesos;
		this.categoriaVentas = categoriaVentas;
	}
	
	@JsonProperty("PesoSalePrice")
	public BigDecimal getPrecioVentaPesos() {
		return precioVentaPesos;
	}

	public void setPrecioVentaPesos(BigDecimal precioVentaPesos) {
		this.precioVentaPesos = precioVentaPesos;
	}

	@JsonProperty("SaleType")
	public int getCategoriaVentas() {
		return categoriaVentas;
	}

	public void setCategoriaVentas(int categoriaVentas) {
		this.categoriaVentas = categoriaVentas;
	}

	@JsonProperty("DolarSalePrice")
	public BigDecimal getPrecioVentaDolar() {
		return precioVentaDolar;
	}

	public void setPrecioVentaDolar(BigDecimal precioVentaDolar) {
		this.precioVentaDolar = precioVentaDolar;
	}

}

