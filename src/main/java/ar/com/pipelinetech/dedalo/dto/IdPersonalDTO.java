package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdPersonalDTO {
	
	private Integer idpersonal_fk;
	
	public IdPersonalDTO() {
		
	}
	
	
	public IdPersonalDTO(Integer idpersonal_fk) {
		this.setIdpersonal_fk(idpersonal_fk);
	}

	@JsonProperty("idpersonal_fk")
	public Integer getIdpersonal_fk() {
		return idpersonal_fk;
	}

	public void setIdpersonal_fk(Integer idpersonal_fk) {
		this.idpersonal_fk = idpersonal_fk;
	}
}
