//package ar.com.pipelinetech.dedalo.dto;
//
//import java.util.List;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//import ar.com.pipelinetech.dedalo.enums.EstadoVuelo;
//import ar.com.pipelinetech.dedalo.enums.TipoVuelo;
//
//public class VueloDTO {
//
//	private Integer id;
//	private String codigo;
//	private TipoVuelo tipo;
//	private EstadoVuelo estado;
//	private List<TripulanteDTO> tripulacion;
//	
//	public VueloDTO() {
//		
//	}
//	
//	public VueloDTO(Integer id, String codigo, TipoVuelo tipo, EstadoVuelo estado, List<TripulanteDTO> tripulacion) {
//		super();
//		this.id = id;
//		this.codigo = codigo;
//		this.tipo = tipo;
//		this.estado = estado;
//		this.tripulacion = tripulacion;
//	}
//
//	@JsonProperty("Id")
//	public Integer getId() {
//		return id;
//	}
//	
//	public void setId(Integer id) {
//		this.id = id;
//	}
//	
//	@JsonProperty("Code")
//	public String getCodigo() {
//		return codigo;
//	}
//	
//	public void setCodigo(String codigo) {
//		this.codigo = codigo;
//	}
//	
//	@JsonProperty("Type")
//	public TipoVuelo getTipo() {
//		return tipo;
//	}
//	
//	public void setTipo(TipoVuelo tipo) {
//		this.tipo = tipo;
//	}
//	
//	@JsonProperty("Status")
//	public EstadoVuelo getEstado() {
//		return estado;
//	}
//	public void setEstado(EstadoVuelo estado) {
//		this.estado = estado;
//	}
//	
//	@JsonProperty("Crew")
//	public List<TripulanteDTO> getTripulacion() {
//		return tripulacion;
//	}
//	public void setTripulacion(List<TripulanteDTO> tripulacion) {
//		this.tripulacion = tripulacion;
//	}
//	
//}
