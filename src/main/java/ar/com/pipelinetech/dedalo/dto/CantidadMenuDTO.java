package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CantidadMenuDTO {
	
	private int celiaco;
	private int estandar;
	private int vegano;
	private int vegetariano;
	
	
	@JsonProperty("CeliacMenu")
	public int getCeliaco() {
		return celiaco;
	}
	public void setCeliaco(int celiaco) {
		this.celiaco = celiaco;
	}
	
	@JsonProperty("StandardMenu")
	public int getEstandar() {
		return estandar;
	}
	public void setEstandar(int estandar) {
		this.estandar = estandar;
	}
	
	@JsonProperty("VeganMenu")
	public int getVegano() {
		return vegano;
	}
	public void setVegano(int vegano) {
		this.vegano = vegano;
	}
	
	@JsonProperty("VegetarianMenu")
	public int getVegetariano() {
		return vegetariano;
	}
	public void setVegetariano(int vegetariano) {
		this.vegetariano = vegetariano;
	}

}
