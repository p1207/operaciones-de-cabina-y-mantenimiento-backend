package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsumoDTO {
	
	private Integer id;
	private String nombre;
	private String descripcion;
	private double peso;
	private String tipo;
	
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JsonProperty("Weight")
	public double getPeso() {
		return peso;
	}
	
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	@JsonProperty("Type")
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
