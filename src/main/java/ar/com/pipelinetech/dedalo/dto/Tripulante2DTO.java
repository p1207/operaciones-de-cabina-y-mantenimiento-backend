package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tripulante2DTO {
	
	private String idvuelo_fk;
	private String aeropuerto_codiata;
	private String nombre;
	private String apellido;
	private String mail;
	private boolean documentacionenorden;
	private String posicion;
	private boolean activo;
	
	@JsonProperty("FlightId")
	public String getId_vuelo() {
		return idvuelo_fk;
	}
	public void setId_vuelo(String idvuelo_fk) {
		this.idvuelo_fk = idvuelo_fk;
	}
	
	@JsonProperty("AirportCode")
	public String getAeropuerto_codiata() {
		return aeropuerto_codiata;
	}
	public void setAeropuerto_codiata(String aeropuerto_codiata) {
		this.aeropuerto_codiata = aeropuerto_codiata;
	}
	
	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonProperty("Surname")
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@JsonProperty("Email")
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	@JsonProperty("Documentation")
	public boolean isDocumentacionenorden() {
		return documentacionenorden;
	}
	public void setDocumentacionenorden(boolean documentacionenorden) {
		this.documentacionenorden = documentacionenorden;
	}
	
	@JsonProperty("Role")
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	@JsonProperty("Active")
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	
}
