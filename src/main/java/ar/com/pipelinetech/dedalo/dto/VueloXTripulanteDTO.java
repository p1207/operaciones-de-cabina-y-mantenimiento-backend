package ar.com.pipelinetech.dedalo.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VueloXTripulanteDTO {

	private String idvuelo;
	private String codvuelo;
	private String aeronave_matricula_fk;
	private String origenteorico_codiata;
	private String origenreal_codiata;
	private String destinoteorico_codiata;
	private String destinoreal_codiata;
	private String nombrecompania;
    private Integer idestados_fk;
    private String rutateorica;
    private String rutareal;
    private String regladevuelo;
    private String tipodevuelo;
    private String diadespegue;
    private Date fechadespegueestimado;
    private Date fechadespeguereal;
    private Date fechaaterrizajeestimado;
    private Date fechaaterrizajereal;
    private boolean checkin;
    private boolean controlcabina;
    private Integer totalpersonasabordo;
    private Integer pesocargaorigen;
    private Integer pesocargadestino;
    private boolean informado;
    private Integer motivoestado;
    private String [] aeronavesposibles;
	private Integer idpersonal_fk;
    private String idvuelo_fk;
    
    @JsonProperty("FlightId")
	public String getIdvuelo() {
		return idvuelo;
	}
    
	public void setIdvuelo(String idvuelo) {
		this.idvuelo = idvuelo;
	}
	
	@JsonProperty("FlightCode")
	public String getCodvuelo() {
		return codvuelo;
	}
	
	public void setCodvuelo(String codvuelo) {
		this.codvuelo = codvuelo;
	}
	
	@JsonProperty("Aircraft")
	public String getAeronave_matricula_fk() {
		return aeronave_matricula_fk;
	}
	
	public void setAeronave_matricula_fk(String aeronave_matricula_fk) {
		this.aeronave_matricula_fk = aeronave_matricula_fk;
	}
	
	@JsonProperty("TheoricalOrigin")
	public String getOrigenteorico_codiata() {
		return origenteorico_codiata;
	}
	public void setOrigenteorico_codiata(String origenteorico_codiata) {
		this.origenteorico_codiata = origenteorico_codiata;
	}
	
	@JsonProperty("RealOrigin")
	public String getOrigenreal_codiata() {
		return origenreal_codiata;
	}
	
	
	public void setOrigenreal_codiata(String origenreal_codiata) {
		this.origenreal_codiata = origenreal_codiata;
	}
	
	@JsonProperty("TheoricalDestiny")
	public String getDestinoteorico_codiata() {
		return destinoteorico_codiata;
	}
	
	public void setDestinoteorico_codiata(String destinoteorico_codiata) {
		this.destinoteorico_codiata = destinoteorico_codiata;
	}
	
	@JsonProperty("RoyalDestiny")
	public String getDestinoreal_codiata() {
		return destinoreal_codiata;
	}
	
	public void setDestinoreal_codiata(String destinoreal_codiata) {
		this.destinoreal_codiata = destinoreal_codiata;
	}
	
	@JsonProperty("CompanyName")
	public String getNombrecompania() {
		return nombrecompania;
	}
	
	public void setNombrecompania(String nombrecompania) {
		this.nombrecompania = nombrecompania;
	}
	
	@JsonProperty("Status")
	public Integer getIdestados_fk() {
		return idestados_fk;
	}
	
	public void setIdestados_fk(Integer idestados_fk) {
		this.idestados_fk = idestados_fk;
	}
	
	@JsonProperty("TheoricalRoute")
	public String getRutateorica() {
		return rutateorica;
	}
	
	public void setRutateorica(String rutateorica) {
		this.rutateorica = rutateorica;
	}
	
	@JsonProperty("RoyalRoute")
	public String getRutareal() {
		return rutareal;
	}
	
	public void setRutareal(String rutareal) {
		this.rutareal = rutareal;
	}
	
	@JsonProperty("FlightRule")
	public String getRegladevuelo() {
		return regladevuelo;
	}
	
	public void setRegladevuelo(String regladevuelo) {
		this.regladevuelo = regladevuelo;
	}
	
	@JsonProperty("FlightType")
	public String getTipodevuelo() {
		return tipodevuelo;
	}
	
	public void setTipodevuelo(String tipodevuelo) {
		this.tipodevuelo = tipodevuelo;
	}
	
	@JsonProperty("TakeOffDay")
	public String getDiadespegue() {
		return diadespegue;
	}
	
	public void setDiadespegue(String diadespegue) {
		this.diadespegue = diadespegue;
	}
	
	@JsonProperty("TakeOffDate")
	public Date getFechadespegueestimado() {
		return fechadespegueestimado;
	}
	
	public void setFechadespegueestimado(Date fechadespegueestimado) {
		this.fechadespegueestimado = fechadespegueestimado;
	}
	
	@JsonProperty("RoyalTakeOffDate")
	public Date getFechadespeguereal() {
		return fechadespeguereal;
	}
	
	public void setFechadespeguereal(Date fechadespeguereal) {
		this.fechadespeguereal = fechadespeguereal;
	}
	
	@JsonProperty("EstimadedLandingDate")
	public Date getFechaaterrizajeestimado() {
		return fechaaterrizajeestimado;
	}
	
	public void setFechaaterrizajeestimado(Date fechaaterrizajeestimado) {
		this.fechaaterrizajeestimado = fechaaterrizajeestimado;
	}
	
	@JsonProperty("ActualLandingDate")
	public Date getFechaaterrizajereal() {
		return fechaaterrizajereal;
	}
		
	public void setFechaaterrizajereal(Date fechaaterrizajereal) {
		this.fechaaterrizajereal = fechaaterrizajereal;
	}
	
	@JsonProperty("IsCheckIn")
	public boolean isCheckin() {
		return checkin;
	}
	
	public void setCheckin(boolean checkin) {
		this.checkin = checkin;
	}
	
	@JsonProperty("EstimatedMileage")
	public boolean isControlcabina() {
		return controlcabina;
	}
	
	public void setControlcabina(boolean controlcabina) {
		this.controlcabina = controlcabina;
	}
	
	@JsonProperty("TotalPeopleOnBoard")
	public Integer getTotalpersonasabordo() {
		return totalpersonasabordo;
	}
	
	public void setTotalpersonasabordo(Integer totalpersonasabordo) {
		this.totalpersonasabordo = totalpersonasabordo;
	}
	
	@JsonProperty("OriginalLoadWaight")
	public Integer getPesocargaorigen() {
		return pesocargaorigen;
	}
	
	public void setPesocargaorigen(Integer pesocargaorigen) {
		this.pesocargaorigen = pesocargaorigen;
	}
	
	@JsonProperty("DestinationLoadWaight")
	public Integer getPesocargadestino() {
		return pesocargadestino;
	}
	
	public void setPesocargadestino(Integer pesocargadestino) {
		this.pesocargadestino = pesocargadestino;
	}
	
	@JsonProperty("isInformed")
	public boolean isInformado() {
		return informado;
	}
	
	public void setInformado(boolean informado) {
		this.informado = informado;
	}
	
	@JsonProperty("StatusReason")
	public Integer getMotivoestado() {
		return motivoestado;
	}
	
	public void setMotivoestado(Integer motivoestado) {
		this.motivoestado = motivoestado;
	}
	
	@JsonProperty("PossibleAircraft")
	public String[] getAeronavesposibles() {
		return aeronavesposibles;
	}
	
	public void setAeronavesposibles(String[] aeronavesposibles) {
		this.aeronavesposibles = aeronavesposibles;
	}
	
	@JsonProperty("idpersonal_fk")
	public Integer getIdpersonal_fk() {
		return idpersonal_fk;
	}
	
	public void setIdpersonal_fk(Integer idpersonal_fk) {
		this.idpersonal_fk = idpersonal_fk;
	}
	
	@JsonProperty("AircraftId")
	public String getIdvuelo_fk() {
		return idvuelo_fk;
	}
	
	public void setIdvuelo_fk(String idvuelo_fk) {
		this.idvuelo_fk = idvuelo_fk;
	}
    
    
}
