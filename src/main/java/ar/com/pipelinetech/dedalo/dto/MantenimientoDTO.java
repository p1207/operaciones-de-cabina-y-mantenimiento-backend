package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import ar.com.pipelinetech.dedalo.enums.TipoMantenimiento;

public class MantenimientoDTO {
	
	private Integer id;
	private String nombre;
	private int activo;
	private TipoMantenimiento tipoMantenimiento;
	private String descripcion;
	private List<TareaDTO> listaTareas;
	private List<InsumoMantenimientoDTO> listaInsumos;
	private List<HabilidadDTO> habilidades;
	
	public MantenimientoDTO() {
		
	}
	public MantenimientoDTO(Integer id, String nombre, int activo, TipoMantenimiento tipoMantenimiento,
			String descripcion, List<TareaDTO> listaTareas, List<InsumoMantenimientoDTO> listaInsumos, List<HabilidadDTO> habilidades) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activo = activo;
		this.tipoMantenimiento = tipoMantenimiento;
		this.descripcion = descripcion;
		this.listaTareas = listaTareas;
		this.listaInsumos = listaInsumos;
		this.habilidades = habilidades;
		}

	@Override
	public String toString() {
		return "MantenimientoDTO [id=" + id + ", nombre=" + nombre + ", activo=" + activo + ", tipoMantenimiento="
				+ tipoMantenimiento + ", descripcion=" + descripcion + ", ListaTareas=" + listaTareas
				+ ", ListaInsumos=" + listaInsumos + "]";
	}

	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Active")
	public int isActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@JsonProperty("Type")
	public TipoMantenimiento getTipoMantenimiento() {
		return tipoMantenimiento;
	}

	public void setTipoMantenimiento(TipoMantenimiento tipoMantenimiento) {
		this.tipoMantenimiento = tipoMantenimiento;
	}

	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@JsonProperty("Tasks")
	public List<TareaDTO> getListaTareas() {
		return listaTareas;
	}

	public void setListaTareas(List<TareaDTO> listaTareas) {
		this.listaTareas = listaTareas;
	}

	@JsonProperty("Supplies")
	public List<InsumoMantenimientoDTO> getListaInsumos() {
		return listaInsumos;
	}

	public void setListaInsumos(List<InsumoMantenimientoDTO> listaInsumos) {
		this.listaInsumos = listaInsumos;
	}
	
	@JsonProperty("Abilities")
	public List<HabilidadDTO> getHabilidades() {
		return habilidades;
	}
	
	public void setHabilidades(List<HabilidadDTO> habilidades) {
		this.habilidades = habilidades;
	}
}
