package ar.com.pipelinetech.dedalo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MantenimientoXAeronaveDTO {

	private boolean aprobados;
	private List<SolicitudMantenimientoDTO> mantenimientos;
	
	
	public MantenimientoXAeronaveDTO() {

	}
	
	public MantenimientoXAeronaveDTO(boolean aprobados, List<SolicitudMantenimientoDTO> mantenimientos) {
		super();
		this.aprobados = aprobados;
		this.mantenimientos = mantenimientos;
	}

	@JsonProperty("Approved")
	public boolean isAprobados() {
		return aprobados;
	}
	
	public void setAprobados(boolean aprobados) {
		this.aprobados = aprobados;
	}

	@JsonProperty("AllMaintenance")
	public List<SolicitudMantenimientoDTO> getMantenimientos() {
		return mantenimientos;
	}
	
	public void setMantenimientos(List<SolicitudMantenimientoDTO> mantenimientos) {
		this.mantenimientos = mantenimientos;
	}
	
	
}
