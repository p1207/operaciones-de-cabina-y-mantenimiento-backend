package ar.com.pipelinetech.dedalo.dto;

public class ConversionMonedaDTO {
private String fecha;
private String compra;
private String venta;


public String getFecha() {
	return fecha;
}
public void setFecha(String fecha) {
	this.fecha = fecha;
}
public String getCompra() {
	return compra;
}
public void setCompra(String compra) {
	this.compra = compra;
}
public String getVenta() {
	return venta;
}
public void setVenta(String venta) {
	this.venta = venta;
}


}
