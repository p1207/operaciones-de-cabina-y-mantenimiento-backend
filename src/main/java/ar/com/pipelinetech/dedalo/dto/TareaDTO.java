package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TareaDTO {
	
	private Integer id;
	private String descripcion;
	private Integer mantenimiento;
	private Integer solicitud;
	
	public TareaDTO() {
		
	}
	public TareaDTO(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Maintenance")
	public Integer getMantenimiento() {
		return mantenimiento;
	}
	public void setMantenimiento(Integer mantenimiento) {
		this.mantenimiento = mantenimiento;
	}
	
	@JsonProperty("Request")
	public Integer getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(Integer solicitud) {
		this.solicitud = solicitud;
	}

}
