package ar.com.pipelinetech.dedalo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Vuelo2DTO {

	private String idvuelo;
	//private String idvuelo2;
	private String codvuelo;
	private String estado;
	private String aeronave_matricula_fk;
	private String tipodevuelo;
	private List<TripulanteDTO> tripulacion;
	private String fechadespegueestimado;
	
	@JsonProperty("FlightId")
	public String getIdvuelo() {
		return idvuelo;
	}
	public void setIdvuelo(String idvuelo) {
		this.idvuelo = idvuelo;
	}

	@JsonProperty("Code")
	public String getCodvuelo() {
		return codvuelo;
	}
	public void setCodvuelo(String codvuelo) {
		this.codvuelo = codvuelo;
	}
	
	@JsonProperty("Type")
	public String getTipodevuelo() {
		return tipodevuelo;
	}
	public void setTipodevuelo(String tipodevuelo) {
		this.tipodevuelo = tipodevuelo;
	}	
	
	@JsonProperty("Status")
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@JsonProperty("Domain")
	public String getAeronave_matricula_fk() {
		return aeronave_matricula_fk;
	}
	public void setAeronave_matricula_fk(String aeronave_matricula_fk) {
		this.aeronave_matricula_fk = aeronave_matricula_fk;
	}
		
	@JsonProperty("Crew")
	public List<TripulanteDTO> getTripulacion() {
		return tripulacion;
	}
	public void setTripulacion(List<TripulanteDTO> tripulacion) {
		this.tripulacion = tripulacion;
	}
	
	@JsonProperty("Date")
	public String getFechadespegueestimado() {
		return fechadespegueestimado;
	}
	public void setFechadespegueestimado(String fechadespegueestimado) {
		this.fechadespegueestimado = fechadespegueestimado;
	}
	@Override
	public String toString() {
		return "Vuelo2DTO [idvuelo=" + idvuelo + ", codvuelo=" + codvuelo + ", estado=" + estado
				+ ", aeronave_matricula_fk=" + aeronave_matricula_fk + ", tipodevuelo=" + tipodevuelo + ", tripulacion="
				+ tripulacion + ", fechadespegueestimado=" + fechadespegueestimado + "]";
	}
	
	
}
