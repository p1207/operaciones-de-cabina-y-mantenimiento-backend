package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsumoVueloDTO {
	
	private Integer id;
	private Integer idInsumo;
	private String vuelo;
	private int cantidadInicial;
	private int cantidadFinal;
	private InsumoDTO insumo;
	
	public InsumoVueloDTO() {
	}

	public InsumoVueloDTO(Integer id, Integer idInsumo, String vuelo, int cantidadInicial, int cantidadFinal,
			InsumoDTO insumo) {
		super();
		this.id = id;
		this.idInsumo = idInsumo;
		this.vuelo = vuelo;
		this.cantidadInicial = cantidadInicial;
		this.cantidadFinal = cantidadFinal;
		this.insumo = insumo;
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("SupplyId")
	public Integer getIdInsumo() {
		return idInsumo;
	}

	public void setIdInsumo(Integer idInsumo) {
		this.idInsumo = idInsumo;
	}

	@JsonProperty("FlightId")
	public String getVuelo() {
		return vuelo;
	}

	public void setVuelo(String vuelo) {
		this.vuelo = vuelo;
	}

	@JsonProperty("InitialQuantity")
	public int getCantidadInicial() {
		return cantidadInicial;
	}

	public void setCantidadInicial(int cantidadInicial) {
		this.cantidadInicial = cantidadInicial;
	}

	@JsonProperty("FinalQuantity")
	public int getCantidadFinal() {
		return cantidadFinal;
	}

	public void setCantidadFinal(int cantidadFinal) {
		this.cantidadFinal = cantidadFinal;
	}
	
	@JsonProperty("Supply")
	public InsumoDTO getInsumo() {
		return insumo;
	}

	public void setInsumo(InsumoDTO insumo) {
		this.insumo = insumo;
	}
}
