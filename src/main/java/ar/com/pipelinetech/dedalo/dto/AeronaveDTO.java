//package ar.com.pipelinetech.dedalo.dto;
//
//import java.util.List;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//import ar.com.pipelinetech.dedalo.enums.TipoAeronave;
//
//public class AeronaveDTO {
//	
//	private Integer id;
//	private TipoAeronave tipo;
//	private String matricula;
//	private ModeloAeronaveDTO modelo;
//	private List<MantenimientoDTO> mantenimientos;
//
//	public AeronaveDTO() {
//
//	}
//
//	public AeronaveDTO(Integer id, String matricula, TipoAeronave tipo, ModeloAeronaveDTO modelo, List<MantenimientoDTO> mantenimientos) {
//		super();
//		this.id = id;
//		this.matricula = matricula;
//		this.tipo = tipo;
//		this.modelo = modelo;
//		this.mantenimientos = mantenimientos;
//	}
//	
//	@JsonProperty("Id")
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//	
//	@JsonProperty("Domain")
//	public String getMatricula() {
//		return matricula;
//	}
//
//	public void setMatricula(String matricula) {
//		this.matricula = matricula;
//	}
//
//	@JsonProperty("Type")
//	public TipoAeronave getTipo() {
//		return tipo;
//	}
//
//	public void setTipo(TipoAeronave tipo) {
//		this.tipo = tipo;
//	}
//
//	@JsonProperty("Maintenances")
//	public List<MantenimientoDTO> getMantenimientos() {
//		return mantenimientos;
//	}
//
//	public void setMantenimientos(List<MantenimientoDTO> mantenimientos) {
//		this.mantenimientos = mantenimientos;
//	}
//	
//	@JsonProperty("Model")
//	public ModeloAeronaveDTO getModelo() {
//		return modelo;
//	}
//
//	public void setModelo(ModeloAeronaveDTO modelo) {
//		this.modelo = modelo;
//	}
//
//}
