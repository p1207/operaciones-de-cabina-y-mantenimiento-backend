package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TipoEmergenciaDTO {
	
	private Integer id;
	private String nombre;
	private ProtocoloDTO protocolo;
		
	
	public TipoEmergenciaDTO () {
	}


	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonProperty("Protocol")
	public ProtocoloDTO getProtocolo() {
		return protocolo;
	}
	public void setProtocolo(ProtocoloDTO protocoloDTO) {
		this.protocolo = protocoloDTO;
	}

}
