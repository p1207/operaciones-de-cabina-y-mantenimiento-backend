package ar.com.pipelinetech.dedalo.dto;

import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Aeronave2DTO {
	
	private String matricula;
	private String aeropuerto_codiata;
	private String modeloaeronave;
	private List<MantenimientoDTO> mantenimientos;
	private Integer kmrecorridos;
	private int capacidadreal;
	private boolean autorizado;
	private boolean activo;
	private Date ultimomantenimiento;

	
	@JsonProperty("Id")
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	@JsonProperty("Airport")
	public String getAeropuerto_codiata() {
		return aeropuerto_codiata;
	
	}
	public void setAeropuerto_codiata(String aeropuerto_codiata) {
		this.aeropuerto_codiata = aeropuerto_codiata;
	}
	
	@JsonProperty("Model")
	public String getModeloaeronave() {
		return modeloaeronave;
	}
	public void setModeloaeronave(String modeloaeronave) {
		this.modeloaeronave = modeloaeronave;
	}
	
	@JsonProperty("Maintenances")
	public List<MantenimientoDTO> getMantenimientos() {
		return mantenimientos;
	}
	public void setMantenimientos(List<MantenimientoDTO> mantenimientos) {
		this.mantenimientos = mantenimientos;
	}
	
	@JsonProperty("KmTraveled")
	public Integer getKmrecorridos() {
		return kmrecorridos;
	}
	public void setKmrecorridos(Integer kmrecorridos) {
		this.kmrecorridos = kmrecorridos;
	}
	
	@JsonProperty("Authorized")
	public boolean isAutorizado() {
		return autorizado;
	}	
	public void setAutorizado(boolean autorizado) {
		this.autorizado = autorizado;
	}
	
	@JsonProperty("Active")
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	@JsonProperty("DateLastMaintenance")
	public Date getUltimomantenimiento() {
		return ultimomantenimiento;
	}
	public void setUltimomantenimiento(Date ultimomantenimiento) {
		this.ultimomantenimiento = ultimomantenimiento;
	}
	
	@JsonProperty("ActualCapacity")
	public int getCapacidadreal() {
		return capacidadreal;
	}
	public void setCapacidadreal(int capacidadreal) {
		this.capacidadreal = capacidadreal;
	}
		

}
