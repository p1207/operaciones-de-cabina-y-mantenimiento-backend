package ar.com.pipelinetech.dedalo.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TripulanteDTO {
	
	private Integer id;
	private String nombre;
	private BigDecimal valorModulo;
	private RolDTO rol;
	
	public TripulanteDTO() {
		
	}
	public TripulanteDTO(Integer id, String nombre, BigDecimal valorModulo, RolDTO rol) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.valorModulo = valorModulo;
		this.rol = rol;
	}
	
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonProperty("ModuleValue")
	public BigDecimal getValorModulo() {
		return valorModulo;
	}
	
	public void setValorModulo(BigDecimal valorModulo) {
		this.valorModulo = valorModulo;
	}
	
	@JsonProperty("Role")
	public RolDTO getRol() {
		return rol;
	}
	public void setRol(RolDTO rol) {
		this.rol = rol;
	}


	
}
