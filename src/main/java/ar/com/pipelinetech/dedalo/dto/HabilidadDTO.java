package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HabilidadDTO {

	private Integer id;
	private String nombre;
	
	public HabilidadDTO() {
		
	}

	public HabilidadDTO(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
