package ar.com.pipelinetech.dedalo.dto;

import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.pipelinetech.dedalo.enums.EstadoSolicitudMantenimiento;
public class SolicitudMantenimientoDTO {
	
	private Integer id;
	private String descripcion;
	private String matricula;
	private MantenimientoDTO mantenimiento;
	private Integer idMantenimiento;
	private Date fecha;
	private Integer prioridad;
	private Integer modulo;
	private String observacion;
	private EstadoSolicitudMantenimiento estado;
	private Integer estadoId;
	private TecnicoDTO tecnico;
	private Integer idTecnico;
	private List<TareaDTO> tareas;
	private int activo;
	
	public SolicitudMantenimientoDTO() {
		
	}
	
	public SolicitudMantenimientoDTO(Integer id, String descripcion, String matricula, MantenimientoDTO mantenimiento,
			Integer idMantenimiento, Date fecha, Integer prioridad, Integer modulo,
			String observacion, EstadoSolicitudMantenimiento estado, Integer estadoId, TecnicoDTO tecnico,
			Integer idTecnico, List<TareaDTO> tareas, int activo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.matricula = matricula;
		this.mantenimiento = mantenimiento;
		this.idMantenimiento = idMantenimiento;
		this.fecha = fecha;
		this.prioridad = prioridad;
		this.modulo = modulo;
		this.observacion = observacion;
		this.estado = estado;
		this.estadoId = estadoId;
		this.tecnico = tecnico;
		this.idTecnico = idTecnico;
		this.tareas = tareas;
		this.activo = activo;
	}


	@JsonProperty("Module")
	public int getModulo() {
		return modulo;
	}

	public void setDuracion(Integer modulo) {
		this.modulo = modulo;
	}

	@JsonProperty("Priority")
	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	@JsonProperty("Date")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@JsonProperty("Description")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@JsonProperty("Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("Aircraft")
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	@JsonProperty("Observation")
	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@JsonProperty("Status")
	public EstadoSolicitudMantenimiento getEstado() {
		return estado;
	}

	public void setEstado(EstadoSolicitudMantenimiento estado) {
		this.estado = estado;
	}
	
	@JsonProperty("Tasks")
	public List<TareaDTO> getTareas() {
		return tareas;
	}
	public void setTareas(List<TareaDTO> tareas) {
		this.tareas = tareas;
	}

	@JsonProperty("Technician")
	public TecnicoDTO getTecnico() {
		return tecnico;
	}
	public void setTecnico(TecnicoDTO tecnico) {
		this.tecnico = tecnico;
	}
	
	@JsonProperty("TechnicianId")
	public int getIdTecnico() {
		return idTecnico;
	}
	public void setIdTecnico(Integer idTecnico) {
		this.idTecnico = idTecnico;
	}
	
	
	public void setModulo(Integer modulo) {
		this.modulo = modulo;
	}
	
	@JsonProperty("Maintenance")
	public MantenimientoDTO getMantenimiento() {
		return mantenimiento;
	}
	public void setMantenimiento(MantenimientoDTO mantenimiento) {
		this.mantenimiento = mantenimiento;
	}
	
	@JsonProperty("MaintenanceId")
	public Integer getIdMantenimiento() {
		return idMantenimiento;
	}
	
	public void setIdMantenimiento(Integer idMantenimiento) {
		this.idMantenimiento = idMantenimiento;
	}
	
	@JsonProperty("Activo")
	public Integer getActivo() {
		return activo;
	}
	
	public void setActivo(Integer activo) {
		this.activo = activo;
	}
	
	@JsonProperty("StatusId")
	public Integer getEstadoId() {
		return estadoId;
	}
	
	public void setEstadoId(Integer estadoId) {
		this.estadoId = estadoId;
	}

}
