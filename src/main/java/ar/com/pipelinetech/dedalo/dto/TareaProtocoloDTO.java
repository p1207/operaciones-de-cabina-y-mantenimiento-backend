package ar.com.pipelinetech.dedalo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TareaProtocoloDTO {
	
	private Integer id;
	private String descripcion;
	private String posicion;
	private Integer idProtocolo;
	private Integer idPadre;

	@JsonProperty("IdReal")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("Id")
	public String getPosicion() {
		if(this.idPadre ==null) {
			return posicion;	
		}
		return this.idPadre+posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	@JsonProperty("Name")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JsonProperty("IdProtocol")
	public Integer getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(Integer idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	
	@JsonProperty("IdFather")
	public Integer getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(Integer idPadre) {
		this.idPadre = idPadre;
	}
}
