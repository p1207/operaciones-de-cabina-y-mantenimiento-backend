package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="sanitario")
public class InsumoSanitario extends Insumo{

	private static final long serialVersionUID = 5741179218599107817L;
	private int categoriaSanitario;
	
	public InsumoSanitario() {
	}

	public InsumoSanitario(int categoriaSanitario) {
		super();
		this.categoriaSanitario = categoriaSanitario;
	}

	@Column(name = "categoria_sanitario")
	public int getCategoriaSanitario() {
		return categoriaSanitario;
	}

	public void setCategoriaSanitario(int categoriaSanitario) {
		this.categoriaSanitario = categoriaSanitario;
	}
	
}
