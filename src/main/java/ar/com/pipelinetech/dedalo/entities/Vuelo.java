//package ar.com.pipelinetech.dedalo.entities;
//
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
//import javax.persistence.Table;
//
//import ar.com.pipelinetech.dedalo.enums.EstadoVuelo;
//import ar.com.pipelinetech.dedalo.enums.TipoVuelo;
//
//@Entity
//@Table(name = "vuelo", schema = "operacionesymantenimiento")
//public class Vuelo implements java.io.Serializable  {
//
//	private static final long serialVersionUID = -6825596906247285106L;
//	private Integer id;
//	private String codigo;
//	private TipoVuelo tipo;
//	private EstadoVuelo estado;
//	private List<Tripulante> tripulacion;
//	
//	public Vuelo() {
//		
//	}
//	public Vuelo(Integer id, String codigo, TipoVuelo tipo, EstadoVuelo estado, List<Tripulante> tripulacion) {
//		super();
//		this.id = id;
//		this.codigo = codigo;
//		this.tipo = tipo;
//		this.estado = estado;
//		this.tripulacion = tripulacion;
//	}
//
//	@Id
//	@Column(name = "id")
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	public Integer getId() {
//		return id;
//	}
//	
//	public void setId(Integer id) {
//		this.id = id;
//	}
//	
//	@Column(name = "codigo")
//	public String getCodigo() {
//		return codigo;
//	}
//	
//	public void setCodigo(String codigo) {
//		this.codigo = codigo;
//	}
//	
//	@Column(name = "tipo")
//	public TipoVuelo getTipo() {
//		return tipo;
//	}
//	
//	public void setTipo(TipoVuelo tipo) {
//		this.tipo = tipo;
//	}
//	
//	@Column(name = "estado")
//	public EstadoVuelo getEstado() {
//		return estado;
//	}
//	
//	public void setEstado(EstadoVuelo estado) {
//		this.estado = estado;
//	}
//	
//	@ManyToMany()
//	@JoinTable(name = "tripulante_x_vuelo", joinColumns = {
//	@JoinColumn(name = "id_vuelo") }, inverseJoinColumns = { @JoinColumn(name = "id_tripulante") })
//	public List<Tripulante> getTripulacion() {
//		return tripulacion;
//	}
//	
//	public void setTripulacion(List<Tripulante> tripulacion) {
//		this.tripulacion = tripulacion;
//	}
//	
//	
//}
