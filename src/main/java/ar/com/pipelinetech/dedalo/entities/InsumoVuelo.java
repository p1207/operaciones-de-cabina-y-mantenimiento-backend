package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "insumo_x_vuelo", schema = "operacionesymantenimiento")
public class InsumoVuelo {
	
	private Integer id;
	private Integer idInsumo;
	private String vuelo;
	private int cantidadInicial;
	private int cantidadFinal;
	
	public InsumoVuelo() {
		
	}

	public InsumoVuelo(Integer id, Integer idInsumo, String vuelo, int cantidadInicial, int cantidadFinal) {
		super();
		this.id = id;
		this.idInsumo = idInsumo;
		this.vuelo = vuelo;
		this.cantidadInicial = cantidadInicial;
		this.cantidadFinal = cantidadFinal;
	}

	// ------------------Getters y Setters-------------------//
	
	@Id	
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "id_insumo")
	public Integer getIdInsumo() {
		return idInsumo;
	}

	public void setIdInsumo(Integer idInsumo) {
		this.idInsumo = idInsumo;
	}

	
	@Column(name = "cantidad_inicial")
	public int getCantidadInicial() {
		return cantidadInicial;
	}

	public void setCantidadInicial(int cantidadInicial) {
		this.cantidadInicial = cantidadInicial;
	}

	@Column(name = "cantidad_final")
	public int getCantidadFinal() {
		return cantidadFinal;
	}

	public void setCantidadFinal(int cantidadFinal) {
		this.cantidadFinal = cantidadFinal;
	}
	
	@Column(name = "id_vuelo")
	public String getVuelo() {
		return vuelo;
	}

	public void setVuelo(String vuelo) {
		this.vuelo = vuelo;
	}
	
}
