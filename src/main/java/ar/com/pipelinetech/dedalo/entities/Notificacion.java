package ar.com.pipelinetech.dedalo.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import ar.com.pipelinetech.dedalo.enums.NotificacionPrioridad;

@Entity
@Table(name = "notificacion", schema = "operacionesymantenimiento")
@Audited
public class Notificacion {
	
	private Integer id;
	private Date fecha;
	private NotificacionPrioridad tipo;
	private String asunto;
	private String mensaje;
	private Integer idUsuario;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "fecha")
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@Column(name = "prioridad")
	public NotificacionPrioridad getTipo() {
		return tipo;
	}
	public void setTipo(NotificacionPrioridad tipo) {
		this.tipo = tipo;
	}
	
	@Column(name = "asunto")
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	
	@Column(name = "mensaje")
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@Column(name = "id_usuario")
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}
