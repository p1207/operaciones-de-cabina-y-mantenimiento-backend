package ar.com.pipelinetech.dedalo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import ar.com.pipelinetech.dedalo.enums.TipoMantenimiento;

@Entity
@Audited
@Table(name = "mantenimiento", schema = "operacionesymantenimiento")
@SQLDelete(sql = "UPDATE mantenimiento SET activo=0 WHERE id = ?")
@Where(clause="activo=1")
public class Mantenimiento implements java.io.Serializable {

	private static final long serialVersionUID = -6704723355895306699L;

	private Integer id;
	private String nombre;
	private int activo;
	private TipoMantenimiento tipo;
	private String descripcion;
	private List<Tarea> tareas;
	private List<InsumoMantenimiento> insumos;
	private List<Habilidad> habilidades;

//------------------Getters y Setters-------------------//
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "activo")
	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "tipo")
	public TipoMantenimiento getTipo() {
		return tipo;
	}

	public void setTipo(TipoMantenimiento tipo) {
		this.tipo = tipo;
	}

	@NotAudited
	@ManyToMany()
	@JoinTable(name = "insumo_x_mantenimiento", joinColumns = {
			@JoinColumn(name = "id_mantenimiento") }, inverseJoinColumns = { @JoinColumn(name = "id_insumo") })
	public List<InsumoMantenimiento> getInsumos() {
		return insumos;
	}

	public void setInsumos(List<InsumoMantenimiento> insumos) {
		this.insumos = insumos;
	}

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "id_mantenimiento")
	public List<Tarea> getTareas() {
		return tareas;
	}

	public void setTareas(List<Tarea> tareas) {
		this.tareas = tareas;
	}

	@NotAudited
	@ManyToMany()
	@JoinTable(name = "mantenimiento_x_habilidad", joinColumns = {
			@JoinColumn(name = "id_mantenimiento") }, inverseJoinColumns = { @JoinColumn(name = "id_habilidad") })
	public List<Habilidad> getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(List<Habilidad> habilidades) {
		this.habilidades = habilidades;
	}
//------------------Getters y Setters-------------------//
}
