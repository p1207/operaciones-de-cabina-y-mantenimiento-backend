package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.envers.Audited;


@Entity
@Audited
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "insumo", schema = "operacionesymantenimiento")
public abstract class Insumo implements java.io.Serializable {
	
	private static final long serialVersionUID = -1757521313434913341L;
	private Integer id;
	private String nombre;
	private String descripcion;
	private double peso;
	
	public Insumo() {

	}
	
	public Insumo(String nombre, String descripcion, double peso) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.peso = peso;
	}


	// ------------------Getters y Setters-------------------//
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name = "peso")	
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

}	