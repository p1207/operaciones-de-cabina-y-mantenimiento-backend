package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "rol", schema = "operacionesymantenimiento")
public class Rol implements java.io.Serializable {

	private static final long serialVersionUID = -2388514342970769188L;
	private Integer id;
	private String nombre;
	
	//------------------Getters y Setters-------------------//

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	//------------------Getters y Setters-------------------//
	
}
