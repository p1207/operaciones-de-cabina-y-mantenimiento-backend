package ar.com.pipelinetech.dedalo.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;

@Entity
@Audited
@Table(name = "control_x_vuelo", schema = "operacionesymantenimiento")
public class ControlXVuelo {
	private Integer id;
	private Integer id_control;
	private String vuelo;
	private Date fecha_creacion;
	private String observacion;
	private EstadoControlXVuelo estado;
	

	public ControlXVuelo() {
		
	}

	public ControlXVuelo(Integer id, Integer id_control, String vuelo, Date fecha_creacion, String observacion, EstadoControlXVuelo estado) {
		super();
		this.id = id;
		this.id_control = id_control;
		this.vuelo = vuelo;
		this.fecha_creacion = fecha_creacion;
		this.observacion = observacion;
		this.setEstado(estado);
	}

	// ------------------Getters y Setters-------------------//

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	
	@Column(name = "id_control")
	public Integer getId_control() {
		return id_control;
	}
	public void setId_control(Integer id_control) {
		this.id_control = id_control;
	}

	@Column(name = "id_vuelo")
	public String getVuelo() {
		return vuelo;
	}
	public void setVuelo(String id_vuelo) {
		this.vuelo = id_vuelo;
	}

	@Column(name = "fecha_creacion")
	public Date getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	
	@Column(name = "observacion")
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(name = "estado")
	public EstadoControlXVuelo getEstado() {
		return estado;
	}

	public void setEstado(EstadoControlXVuelo estado) {
		this.estado = estado;
	}

}
