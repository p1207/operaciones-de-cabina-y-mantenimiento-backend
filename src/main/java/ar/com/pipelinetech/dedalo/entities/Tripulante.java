package ar.com.pipelinetech.dedalo.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tripulante", schema = "operacionesymantenimiento")
public class Tripulante implements java.io.Serializable {

	private static final long serialVersionUID = -3338999747510105921L;
	private Integer id;
	private String nombreYApellido;
	private BigDecimal valorModulo;
	private Rol rol;
	
//------------------Getters y Setters-------------------//
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombreYApellido() {
		return nombreYApellido;
	}

	public void setNombreYApellido(String nombreYApellido) {
		this.nombreYApellido = nombreYApellido;
	}

	@Column(name = "valor_modulo")
	public BigDecimal getValorModulo() {
		return valorModulo;
	}

	public void setValorModulo(BigDecimal valorModulo) {
		this.valorModulo = valorModulo;
	}

	@ManyToOne
	@JoinColumn(name = "id_rol", insertable = false, updatable = false)
	public Rol getRol() {
		return rol;
	}
	
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	

}
