package ar.com.pipelinetech.dedalo.entities;

import java.util.List;

import ar.com.pipelinetech.dedalo.dto.TripulanteDTO;

public class Vuelo2 {
	
	private String idvuelo;
	private String codvuelo;
	private String estado;
	private String aeronave_matricula_fk;
	private String tipodevuelo;
	private List<TripulanteDTO> tripulacion;
	private String fechadespegueestimado;
		
	
	public String getIdvuelo() {
		return idvuelo;
	}

	public void setIdVuelo(String idvuelo) {
		this.idvuelo = idvuelo;
	}
	
	public String getFechadespegueestimado() {
		return fechadespegueestimado;
	}
	public void setFechadespegueestimado(String fechadespegueestimado) {
		this.fechadespegueestimado = fechadespegueestimado;
	}
	
	public String getCodvuelo() {
		return codvuelo;
	}
	public void setCodvuelo(String codvuelo) {
		this.codvuelo = codvuelo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getAeronave_matricula_fk() {
		return aeronave_matricula_fk;
	}
	public void setAeronave_matricula_fk(String aeronave_matricula_fk) {
		this.aeronave_matricula_fk = aeronave_matricula_fk;
	}
	public String getTipodevuelo() {
		return tipodevuelo;
	}
	public void setTipodevuelo(String tipodevuelo) {
		this.tipodevuelo = tipodevuelo;
	}
	public List<TripulanteDTO> getTripulacion() {
		return tripulacion;
	}
	public void setTripulacion(List<TripulanteDTO> tripulacion) {
		this.tripulacion = tripulacion;
	}
	
//	public String getIdvuelo2() {
//		return idvuelo2;
//	}
//	public void setIdvuelo2(String idvuelo2) {
//		this.idvuelo2 = idvuelo2;
//	}
	
}
