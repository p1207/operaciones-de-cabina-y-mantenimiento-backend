package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "aeronave_x_mantenimiento", schema = "operacionesymantenimiento")
public class AeronaveXMantenimiento implements java.io.Serializable {

	private static final long serialVersionUID = 9181234468578985116L;
	private Integer id;
	private String matricula;
	private Mantenimiento mantenimiento;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	@Column(name = "matricula")
	public String getMatricula() {
		return matricula;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mantenimiento")
	public Mantenimiento getMantenimiento() {
		return mantenimiento;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public void setMantenimiento(Mantenimiento mantenimiento) {
		this.mantenimiento = mantenimiento;
	}
	
}
