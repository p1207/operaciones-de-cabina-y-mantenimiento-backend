package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tarea_protocolo", schema = "operacionesymantenimiento")
public class TareaProtocolo {

	private Integer id;
	private String descripcion;
	private String posicion;
	private Integer idProtocolo;
	private Integer idPadre;
	
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name = "posicion")
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	@Column(name = "id_protocolo")
	public Integer getIdProtocolo() {
		return idProtocolo;
	}
	public void setIdProtocolo(Integer idProtocolo) {
		this.idProtocolo = idProtocolo;
	}
	
	@Column(name = "id_padre")
	public Integer getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(Integer idPadre) {
		this.idPadre = idPadre;
	}
	
}
