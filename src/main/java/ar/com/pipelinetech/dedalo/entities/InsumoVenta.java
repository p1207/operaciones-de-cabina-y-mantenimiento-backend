package ar.com.pipelinetech.dedalo.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="venta")
public class InsumoVenta extends Insumo{

	private static final long serialVersionUID = 7052765930350784716L;
	private BigDecimal precioVentaPesos;
	private int categoriaVenta;
	
	public InsumoVenta() {
	
	}

	public InsumoVenta(BigDecimal precioVentaPesos, int categoriaVenta) {
		super();
		this.precioVentaPesos = precioVentaPesos;
		this.categoriaVenta = categoriaVenta;
	}

	@Column(name = "precio_venta_pesos")
	public BigDecimal getPrecioVentaPesos() {
		return precioVentaPesos;
	}

	public void setPrecioVentaPesos(BigDecimal precioVentaPesos) {
		this.precioVentaPesos = precioVentaPesos;
	}

	@Column(name = "categoria_venta")
	public int getCategoriaVenta() {
		return categoriaVenta;
	}

	public void setCategoriaVenta(int categoriaVenta) {
		this.categoriaVenta = categoriaVenta;
	}
	
}

