package ar.com.pipelinetech.dedalo.entities;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;


@Entity
@Audited
@Table(name = "insumo_mantenimiento", schema = "operacionesymantenimiento")
public class InsumoMantenimiento implements java.io.Serializable{

	private static final long serialVersionUID = 4894589449809406998L;
	private Long id;
	private String nombre;
	private BigDecimal precio;
	private List<Mantenimiento> mantenimientos;
	private List<Control> controles;

//------------------Getters y Setters-------------------//
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "precio")
	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	@NotAudited
	@ManyToMany(mappedBy = "insumos")
	public List<Mantenimiento> getMantenimientos() {
		return mantenimientos;
	}

	public void setMantenimientos(List<Mantenimiento> mantenimientos) {
		this.mantenimientos = mantenimientos;
	}

	@NotAudited
	@ManyToMany(mappedBy = "insumos")
	public List<Control> getControles() {
		return controles;
	}

	public void setControles(List<Control> controles) {
		this.controles = controles;
	}
	
//------------------Getters y Setters-------------------//
}
