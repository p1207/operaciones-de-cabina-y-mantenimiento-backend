package ar.com.pipelinetech.dedalo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "protocolo", schema = "operacionesymantenimiento")
public class Protocolo implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombre;
	private List<TareaProtocolo> tareas;
	
	public Protocolo() {
	}

	
	public Protocolo(Integer id, String nombre, List<TareaProtocolo> tareas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tareas = tareas;
	}


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToMany(targetEntity = TareaProtocolo.class , cascade = CascadeType.ALL)
	@JoinColumn(name = "id_protocolo", referencedColumnName = "id")
	public List<TareaProtocolo> getTareas() {
		return tareas;
	}
	
	public void setTareas(List<TareaProtocolo> tareas) {
		this.tareas = tareas;
	}
	
}
