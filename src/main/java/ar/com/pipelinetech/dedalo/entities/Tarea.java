package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tarea", schema = "operacionesymantenimiento")
public class Tarea implements java.io.Serializable {

	private static final long serialVersionUID = 765110870579030477L;

	private Long id;
	private String descripcion;
	private Mantenimiento mantenimiento;
	private SolicitudMantenimiento solicitud;
//------------------Getters y Setters-------------------//
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long idTarea) {
		this.id = idTarea;
	}

	@Column(name = "descripcion", length = 300)
	public String getDescripcion() {
		return descripcion;
	}
	

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@ManyToOne
	@JoinColumn(name = "id_mantenimiento", insertable = false, updatable = false)
	public Mantenimiento getMantenimiento() {
		return mantenimiento;
	}

	public void setMantenimiento(Mantenimiento mantenimiento) {
		this.mantenimiento = mantenimiento;
	}

	@ManyToOne
	@JoinColumn(name = "id_solicitud", insertable = false, updatable = false)
	public SolicitudMantenimiento getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudMantenimiento solicitud) {
		this.solicitud = solicitud;
	}
	
//------------------Getters y Setters-------------------//

}