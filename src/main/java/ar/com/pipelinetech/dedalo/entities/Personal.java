package ar.com.pipelinetech.dedalo.entities;

import java.util.Date;

public class Personal {
	private Integer personal_legajo_fk;
	private String username;
	private String pass;
	private boolean activo;
	private Date fecha_alta;
	private Integer idpersonal;
	private String aeropuerto_codiata;
	private String nombre;
	private String apellido;
	private String codpais;
	private String codarea;
	private String nrocontacto;
	private String mail;
	private boolean documentacionenorden;
	private String posicion;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	
	public Integer getPersonal_legajo_fk() {
		return personal_legajo_fk;
	}
	public void setPersonal_legajo_fk(Integer personal_legajo_fk) {
		this.personal_legajo_fk = personal_legajo_fk;
	}
	public Date getFecha_alta() {
		return fecha_alta;
	}
	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}
	public Integer getIdpersonal() {
		return idpersonal;
	}
	public void setIdpersonal(Integer idpersonal) {
		this.idpersonal = idpersonal;
	}
	public String getAeropuerto_codiata() {
		return aeropuerto_codiata;
	}
	public void setAeropuerto_codiata(String aeropuerto_codiata) {
		this.aeropuerto_codiata = aeropuerto_codiata;
	}
	public String getCodpais() {
		return codpais;
	}
	public void setCodpais(String codpais) {
		this.codpais = codpais;
	}
	public String getCodarea() {
		return codarea;
	}
	public void setCodarea(String codarea) {
		this.codarea = codarea;
	}
	public String getNrocontacto() {
		return nrocontacto;
	}
	public void setNrocontacto(String nrocontacto) {
		this.nrocontacto = nrocontacto;
	}
	public boolean isDocumentacionenorden() {
		return documentacionenorden;
	}
	public void setDocumentacionenorden(boolean documentacionenorden) {
		this.documentacionenorden = documentacionenorden;
	}		


}
