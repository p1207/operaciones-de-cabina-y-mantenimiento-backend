package ar.com.pipelinetech.dedalo.entities;


import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

@Entity
@DiscriminatorValue(value="catering")
public class InsumoCatering extends Insumo{
	
	private static final long serialVersionUID = -1992007130511223576L;
	private int tipoMenu;
	
	public InsumoCatering() {
		
	}

	public InsumoCatering(int tipoMenu) {
		super();
		this.tipoMenu = tipoMenu;
	}
	
	@Column(name = "tipo_menu")
	public int getTipoMenu() {
		return tipoMenu;
	}

	public void setTipoMenu(int tipoMenu) {
		this.tipoMenu = tipoMenu;
	}
}
