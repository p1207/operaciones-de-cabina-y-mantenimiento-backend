package ar.com.pipelinetech.dedalo.entities;

import java.util.Date;
import java.util.List;

import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;

public class Aeronave2 {
	
	private String matricula;
	private String aeropuerto_codiata;
	private String modeloaeronave;
	private List<MantenimientoDTO> mantenimientos;
	private Integer kmrecorridos;
	private boolean autorizado;
	private boolean activo;
	private int capacidadreal;
	private Date ultimomantenimiento;

	
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getAeropuerto_codiata() {
		return aeropuerto_codiata;
	
	}
	public void setAeropuerto_codiata(String aeropuerto_codiata) {
		this.aeropuerto_codiata = aeropuerto_codiata;
	}
	
	public String getModeloaeronave() {
		return modeloaeronave;
	}
	public void setModeloaeronave(String modeloaeronave) {
		this.modeloaeronave = modeloaeronave;
	}
	
	public List<MantenimientoDTO> getMantenimientos() {
		return mantenimientos;
	}
	public void setMantenimientos(List<MantenimientoDTO> mantenimientos) {
		this.mantenimientos = mantenimientos;
	}
	
	public Integer getKmrecorridos() {
		return kmrecorridos;
	}
	public void setKmrecorridos(Integer kmrecorridos) {
		this.kmrecorridos = kmrecorridos;
	}
	
	public boolean isAutorizado() {
		return autorizado;
	}	
	public void setAutorizado(boolean autorizado) {
		this.autorizado = autorizado;
	}
	
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	public Date getUltimomantenimiento() {
		return ultimomantenimiento;
	}
	public void setUltimomantenimiento(Date ultimomantenimiento) {
		this.ultimomantenimiento = ultimomantenimiento;
	}
	public int getCapacidadreal() {
		return capacidadreal;
	}
	public void setCapacidadreal(int capacidadreal) {
		this.capacidadreal = capacidadreal;
	}
	
}
