package ar.com.pipelinetech.dedalo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "usuario", schema = "operacionesymantenimiento")
public class Usuario implements java.io.Serializable {

	private static final long serialVersionUID = 3579103356565717382L;
	private Integer id;
	private String nombre;
	private String apellido;
	private String email;
	private String username;
	private String contraseña;
	private Rol rol;
	private Integer idTripulacion;
	private List<Habilidad> habilidades;
	public Usuario() {
		
	}
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "apellido")
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "contraseña")
	public String getContraseña() {
		return contraseña;
	}
	
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_rol")
	public Rol getIdRol() {
		return rol;
	}

	public void setIdRol(Rol rol) {
		this.rol = rol;
	}
	
	@Column(name = "id_tripulacion")
	public Integer getIdTripulacion() {
		return idTripulacion;
	}
	
	public void setIdTripulacion(Integer idTripulacion) {
		this.idTripulacion = idTripulacion;
	}
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "tecnico_x_habilidad", joinColumns = { @JoinColumn(name = "id_tecnico") }, inverseJoinColumns = {
			@JoinColumn(name = "id_habilidad") })
	public List<Habilidad> getHabilidades() {
		return habilidades;
	}
	
	public void setHabilidades(List<Habilidad> habilidades) {
		this.habilidades = habilidades;
	}
	
}
