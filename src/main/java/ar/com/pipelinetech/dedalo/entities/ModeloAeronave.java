package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "modelo", schema = "operacionesymantenimiento")
public class ModeloAeronave implements java.io.Serializable{

	private static final long serialVersionUID = -786939042653115447L;
	private int id;
	private String descripcion;

	public ModeloAeronave() {
		
	}

	//------------------Getters y Setters-------------------//
		@Id
		@Column(name = "id")
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
		@Column(name = "descripcion")
		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;

		}
//------------------Getters y Setters-------------------//
}
