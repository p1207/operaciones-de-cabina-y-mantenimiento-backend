package ar.com.pipelinetech.dedalo.entities;

public class Tripulante2 {
	
	private String idvuelo_fk;
	private String aeropuerto_codiata;
	private String nombre;
	private String apellido;
	private String mail;
	private boolean documentacionenorden;
	private String posicion;
	private boolean activo;
	
	
	public String getId_vuelo() {
		return idvuelo_fk;
	}
	public void setId_vuelo(String idvuelo_fk) {
		this.idvuelo_fk = idvuelo_fk;
	}
	public String getAeropuerto_codiata() {
		return aeropuerto_codiata;
	}
	public void setAeropuerto_codiata(String aeropuerto_codiata) {
		this.aeropuerto_codiata = aeropuerto_codiata;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public boolean isDocumentacionenorden() {
		return documentacionenorden;
	}
	public void setDocumentacionenorden(boolean documentacionenorden) {
		this.documentacionenorden = documentacionenorden;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
}
