package ar.com.pipelinetech.dedalo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import ar.com.pipelinetech.dedalo.enums.TipoControl;

@Entity
@Audited
@Table(name = "control", schema = "operacioneymantenimiento")
@SQLDelete(sql = "UPDATE control SET activo=0 WHERE id = ?")
public class Control  implements java.io.Serializable {

	private static final long serialVersionUID = -5509058780701926932L;
	private Integer id;
	private String nombre ;
	private boolean activo;
	private TipoControl tipoControl;
	private String descripcion;
	private Rol rol;
	private List<TareaControl> tareas;
	private List<InsumoMantenimiento> insumos;
	
//----------------------- Getters y Setters --------------------------------//
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "activo")
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	@Column(name = "tipo")
	public TipoControl getTipoControl() {
		return tipoControl;
	}
	
	public void setTipoControl(TipoControl tipoControl) {
		this.tipoControl = tipoControl;
	}
	
	@Column(name = "descripcion")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_rol")
	@NotAudited
	public Rol getRol() {
		return rol;
	}
	
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "id_control")
	@NotAudited
	public List<TareaControl> getTareas() {
		return tareas;
	}
	
	public void setTareas(List<TareaControl> tareas) {
		this.tareas = tareas;
	}
	
	@ManyToMany()
	@JoinTable(name = "insumo_x_control", joinColumns = {
			@JoinColumn(name = "id_control") }, inverseJoinColumns = { @JoinColumn(name = "id_insumo") })
	@NotAudited
	public List<InsumoMantenimiento> getInsumos() {
		return insumos;
	}
	
	public void setInsumos(List<InsumoMantenimiento> insumos) {
		this.insumos = insumos;
	}
	
	
}
