package ar.com.pipelinetech.dedalo.entities;

import java.util.Date;

public class PersonalDTO {

		private Integer idLegajo;
		private String username;
		private String pass;
		private boolean activo;
		private Integer idPersonal;
		private Date fechaAlta;
		private String aeropuertoCodiata;
		private String nombre;
		private String apellido;
		private String codPais;
		private String codArea;
		private String nroContacto;
		private String mail;
		private boolean documentacionEnOrden;
		private String posicion;
		
		public Integer getIdLegajo() {
			return idLegajo;
		}
		public void setIdLegajo(Integer idLegajo) {
			this.idLegajo = idLegajo;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPass() {
			return pass;
		}
		public void setPass(String pass) {
			this.pass = pass;
		}
		public boolean isActivo() {
			return activo;
		}
		public void setActivo(boolean activo) {
			this.activo = activo;
		}
		public Integer getIdPersonal() {
			return idPersonal;
		}
		public void setIdPersonal(Integer idPersonal) {
			this.idPersonal = idPersonal;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellido() {
			return apellido;
		}
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
		public String getCodPais() {
			return codPais;
		}
		public void setCodPais(String codPais) {
			this.codPais = codPais;
		}
		public String getCodArea() {
			return codArea;
		}
		public void setCodArea(String codArea) {
			this.codArea = codArea;
		}
		public String getNroContacto() {
			return nroContacto;
		}
		public void setNroContacto(String nroContacto) {
			this.nroContacto = nroContacto;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public boolean isDocumentacionEnOrden() {
			return documentacionEnOrden;
		}
		public void setDocumentacionEnOrden(boolean documentacionEnOrden) {
			this.documentacionEnOrden = documentacionEnOrden;
		}
		public String getPosicion() {
			return posicion;
		}
		public void setPosicion(String posicion) {
			this.posicion = posicion;
		}
		public String getAeropuertoCodiata() {
			return aeropuertoCodiata;
		}
		public void setAeropuertoCodiata(String aeropuertoCodiata) {
			this.aeropuertoCodiata = aeropuertoCodiata;
		}
		public Date getFechaAlta() {
			return fechaAlta;
		}
		public void setFechaAlta(Date fechaAlta) {
			this.fechaAlta = fechaAlta;
		}			
}
