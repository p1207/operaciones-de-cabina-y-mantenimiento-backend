package ar.com.pipelinetech.dedalo.entities;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
//extender a tecnico de usuario, hacer refactor
@Entity
public class Tecnico extends Usuario {

	private static final long serialVersionUID = 3444144861848758226L;
	private BigDecimal valorModulo;


//------------------Getters y Setters-------------------//

	
	@Column(name = "valor_modulo")
	public BigDecimal getValorModulo() {
		return valorModulo;
	}

	public void setValorModulo(BigDecimal valorModulo) {
		this.valorModulo = valorModulo;
	}

	public Tecnico(BigDecimal valorModulo) {
		super();
		this.valorModulo = valorModulo;
	}

	public Tecnico() {
		
	}

	@Override
	public String toString() {
		return super.toString() + super.getUsername()+ "Tecnico [valorModulo=" + valorModulo + "]";
	}
	
	
//------------------Getters y Setters-------------------//
}
