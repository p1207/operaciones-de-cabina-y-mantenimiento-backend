package ar.com.pipelinetech.dedalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_emergencia", schema = "operacionesymantenimiento")
public class TipoEmergencia {
	
	private Integer id;
	private String nombre;
	private Protocolo protocolo;
	
	
	public TipoEmergencia () {
	}


	// ------------------Getters y Setters-------------------//
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_protocolo")
	public Protocolo getIdProtocolo() {
		return protocolo;
	}
	public void setIdProtocolo(Protocolo protocolo) {
		this.protocolo = protocolo;
	}

}
