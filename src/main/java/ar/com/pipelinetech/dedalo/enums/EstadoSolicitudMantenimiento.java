package ar.com.pipelinetech.dedalo.enums;

public enum EstadoSolicitudMantenimiento {
	
	REGISTRADA,
		
	EN_PROGRESO,
	
	PENDIENTE,
	
	APROBADA,

}
