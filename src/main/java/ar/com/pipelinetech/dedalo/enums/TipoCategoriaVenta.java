package ar.com.pipelinetech.dedalo.enums;

public enum TipoCategoriaVenta {
	BEBIDA,
	PERFUMERIA,
	ELECTRONICA,
	JOYERIA,
	COMESTIBLE
}
