package ar.com.pipelinetech.dedalo.enums;

public enum NotificacionPrioridad {
	ALTA,
	MEDIA,
	BAJA,
}
