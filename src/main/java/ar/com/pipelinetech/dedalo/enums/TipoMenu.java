package ar.com.pipelinetech.dedalo.enums;

public enum TipoMenu {

	VEGANO,
	VEGETARIANO,
	CELIACO,
	ESTANDAR,
}
