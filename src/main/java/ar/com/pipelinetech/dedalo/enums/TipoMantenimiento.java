package ar.com.pipelinetech.dedalo.enums;

public enum TipoMantenimiento {

	CORRECTIVO("CORRECTIVO"),

	PREVENTIVO("PREVENTIVO");

	private String mensaje;

	private TipoMantenimiento(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return this.mensaje;
	}

}
