package ar.com.pipelinetech.dedalo.enums;

public enum EstadoVuelo {
	EN_VUELO,
	EN_PISTA,
	CANCELADO,
}
