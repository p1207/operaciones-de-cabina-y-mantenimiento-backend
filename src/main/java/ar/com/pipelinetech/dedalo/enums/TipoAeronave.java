package ar.com.pipelinetech.dedalo.enums;

public enum TipoAeronave {
	Tripulada,
	NoTripulada;
}
