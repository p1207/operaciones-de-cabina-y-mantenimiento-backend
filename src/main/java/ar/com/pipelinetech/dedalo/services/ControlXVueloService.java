package ar.com.pipelinetech.dedalo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.pipelinetech.dedalo.assembler.ControlAssembler;
import ar.com.pipelinetech.dedalo.assembler.ControlXVueloAssembler;
import ar.com.pipelinetech.dedalo.dto.ControlDTO;
import ar.com.pipelinetech.dedalo.dto.ControlXVueloDTO;
import ar.com.pipelinetech.dedalo.dto.ControlesXVueloDTO;
import ar.com.pipelinetech.dedalo.entities.Control;
import ar.com.pipelinetech.dedalo.entities.ControlXVuelo;
import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;
import ar.com.pipelinetech.dedalo.interfaces.ControlXVueloRepository;
import ar.com.pipelinetech.dedalo.interfaces.IControlRepository;

@Service
public class ControlXVueloService {

	@Autowired
	private ControlXVueloRepository controlXVueloRepository;
	
	@Autowired
	private IControlRepository controlRepository;
	
	public List<ControlXVueloDTO> getControles() {
		List<ControlXVuelo> controles = controlXVueloRepository.findAll();
		List <ControlXVuelo> controlesActivos = new ArrayList<ControlXVuelo>();
		for (ControlXVuelo c : controles) {
			Control control = controlRepository.getById(c.getId_control());
			if(control.isActivo() == true) {
				controlesActivos.add(c);
			}
		}
		
		List<ControlXVueloDTO> controlesxvueloDTO = ControlXVueloAssembler.listEntityToDTOList(controlesActivos);
		 this.setControles(controlesxvueloDTO);
		 return controlesxvueloDTO;
	}

	@Transactional
	public ControlXVueloDTO saveControlXVuelo(ControlXVueloDTO controlVuelo) {
		ControlXVuelo c = ControlXVueloAssembler.toEntity(controlVuelo);
		return ControlXVueloAssembler.toDTO(controlXVueloRepository.save(c));
	}

	public void deleteControlXVuelo(Integer id) {
		controlXVueloRepository.deleteById(id);
	}

	public void updateControlXVuelo(ControlXVueloDTO controlXVuelo) {
		ControlXVuelo m = ControlXVueloAssembler.toEntity(controlXVuelo);
		controlXVueloRepository.saveAndFlush(m);
	}

	public ControlesXVueloDTO controlesXVuelo(String vuelo) {
		List<ControlXVueloDTO> controlesxvueloDTO = ControlXVueloAssembler.listEntityToDTOList(controlXVueloRepository.findAllByVuelo(vuelo));
		this.setControles(controlesxvueloDTO);
		ControlesXVueloDTO result = new ControlesXVueloDTO();
		boolean aprobados = true;
		for(ControlXVueloDTO c : controlesxvueloDTO) {
			aprobados = aprobados && (c.getEstado() == EstadoControlXVuelo.CHEQUEADO);
		}
		
		result.setAprobados(aprobados);
		result.setControles(controlesxvueloDTO);

		return result;
	}

	private void setControles(List<ControlXVueloDTO> controlesxvueloDTO) {
		for (ControlXVueloDTO c: controlesxvueloDTO) {
			ControlDTO control = ControlAssembler.toDto(controlRepository.getById(c.getId_control()));
			c.setControl(control);
		}
		
	}
}
