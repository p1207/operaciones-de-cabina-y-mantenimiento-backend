package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.NotificacionAssembler;
import ar.com.pipelinetech.dedalo.dto.NotificacionDTO;
import ar.com.pipelinetech.dedalo.entities.Notificacion;
import ar.com.pipelinetech.dedalo.interfaces.INotificacionRepository;

@Service
public class NotificacionService {

	@Autowired
	private INotificacionRepository notificacionRepository;

	public List<NotificacionDTO> obtenerNotificaciones() {
		List<Notificacion> notificaciones = notificacionRepository.findAll();
		return NotificacionAssembler.listEntityToDTOList(notificaciones);
	}
	
	public List<NotificacionDTO> obtenerNotificacionesPorUsuario(Integer idUsuario) {
		List<Notificacion> notificaciones = notificacionRepository.findAllByIdUsuario(idUsuario);
		return NotificacionAssembler.listEntityToDTOList(notificaciones);
	}
	
	public NotificacionDTO crearNotificacion(NotificacionDTO notificacionDTO) {
		Notificacion notificacion = NotificacionAssembler.toEntity(notificacionDTO);
		notificacionRepository.save(notificacion);
		return NotificacionAssembler.toDto(notificacion);
	}
	
	public boolean eliminarNotificacion(Integer idNotificacion){
		if(notificacionRepository.existsById(idNotificacion)) {
			notificacionRepository.deleteById(idNotificacion);
			return true;
		}
		return false;
	}
	
}
