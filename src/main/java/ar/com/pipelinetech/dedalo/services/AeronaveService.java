package ar.com.pipelinetech.dedalo.services;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import ar.com.pipelinetech.dedalo.CallExternalServices;
import ar.com.pipelinetech.dedalo.assembler.Aeronave2Assembler;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.entities.Aeronave2;

@Service
public class AeronaveService {

	@SuppressWarnings({"unchecked","rawtypes"})
	public List<Aeronave2DTO> getAeronaves2(){
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		
		Object response = CallExternalServices.doGetService("https://proyecto-icarus.herokuapp.com/aeronaves", headers, new ParameterizedTypeReference<List<Aeronave2>>() {
        });
		
		List<Aeronave2> resp = (List<Aeronave2>)((ResponseEntity)response).getBody(); 
		List<Aeronave2DTO> respuesta = Aeronave2Assembler.listEntityToDTOList(resp); 
		
		return respuesta; 
	}
}
