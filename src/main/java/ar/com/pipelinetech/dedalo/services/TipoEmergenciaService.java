package ar.com.pipelinetech.dedalo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.pipelinetech.dedalo.assembler.ProtocoloAssembler;
import ar.com.pipelinetech.dedalo.assembler.TipoEmergenciaAssembler;
import ar.com.pipelinetech.dedalo.dto.TipoEmergenciaDTO;
import ar.com.pipelinetech.dedalo.entities.Protocolo;
import ar.com.pipelinetech.dedalo.entities.TipoEmergencia;
import ar.com.pipelinetech.dedalo.interfaces.IProtocoloRepository;
import ar.com.pipelinetech.dedalo.interfaces.ITipoEmergenciaRepository;

@Service
public class TipoEmergenciaService {
	
	@Autowired
	private ITipoEmergenciaRepository tipoEmergenciaRepository;
	@Autowired
	private IProtocoloRepository protocoloRepository;
	
	public List<TipoEmergenciaDTO> getTipoEmergencias(){
		List<TipoEmergencia> tipoEmergencias = tipoEmergenciaRepository.findAll();
		return TipoEmergenciaAssembler.listEntityToDTOList(tipoEmergencias);
	}
	
	public TipoEmergenciaDTO getTipoEmergenciaByID(Integer id) {
		TipoEmergencia tipoEmergencia = tipoEmergenciaRepository.getById(id);
		return TipoEmergenciaAssembler.toDto(tipoEmergencia);
	}
	
	@Transactional
	public TipoEmergenciaDTO saveTipoEmergencia(TipoEmergenciaDTO tipoEmergenciaDTO) {
		Protocolo p = ProtocoloAssembler.toEntity(tipoEmergenciaDTO.getProtocolo());
		protocoloRepository.save(p);
		TipoEmergencia e = TipoEmergenciaAssembler.toEntity(tipoEmergenciaDTO);
		tipoEmergenciaRepository.save(e);
		TipoEmergenciaDTO respuesta = TipoEmergenciaAssembler.toDto(e);
		return respuesta;
	}
	
	@Transactional
	public void softDeleteTipoEmergencia(Integer id) {
		tipoEmergenciaRepository.deleteById(id);		
	}

}
