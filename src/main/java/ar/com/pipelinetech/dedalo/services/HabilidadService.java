package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.HabilidadAssembler;
import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.entities.Habilidad;
import ar.com.pipelinetech.dedalo.interfaces.IHabilidadRepository;

@Service
public class HabilidadService {
	@Autowired
	private IHabilidadRepository habilidadRepository;
	
	public List<HabilidadDTO> getHabilidades(){
		List<Habilidad> habilidades = habilidadRepository.findAll();
		return HabilidadAssembler.listEntityToDTOList(habilidades);
	}
	
}
