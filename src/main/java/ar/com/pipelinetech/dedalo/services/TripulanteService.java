package ar.com.pipelinetech.dedalo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import ar.com.pipelinetech.dedalo.CallExternalServices;
import ar.com.pipelinetech.dedalo.assembler.TripulanteAssembler;
import ar.com.pipelinetech.dedalo.dto.TripulanteDTO;
import ar.com.pipelinetech.dedalo.entities.Tripulante;
import ar.com.pipelinetech.dedalo.dto.Tripulante2DTO;
import ar.com.pipelinetech.dedalo.interfaces.ITripulanteRepository;

@Service
public class TripulanteService {

	@Autowired
	private ITripulanteRepository tripulanteRepository;
	
	public List<TripulanteDTO> getTripulantes() {
		List<Tripulante> tripulantes = tripulanteRepository.findAll();
		return TripulanteAssembler.listEntityToDTOList(tripulantes);
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	public Tripulante2DTO [] getTripulantesVuelo(String idvuelo) {
		Object response = CallExternalServices.doPostService("https://proyecto-icarus.herokuapp.com/tripulacionPorIdVuelo", idvuelo, MediaType.APPLICATION_JSON , String.class);		
		Gson gson = new Gson();
		Tripulante2DTO []  lista = null;
		
		if (response  != null) {
			
			lista = gson.fromJson((String)response, Tripulante2DTO[].class); 
		}
		return lista; 
	}

}
