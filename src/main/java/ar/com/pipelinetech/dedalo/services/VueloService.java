package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ar.com.pipelinetech.dedalo.CallExternalServices;
import ar.com.pipelinetech.dedalo.assembler.Vuelo2Assembler;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.dto.InsumoSanitarioDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoVentaDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoVueloDTO;
import ar.com.pipelinetech.dedalo.dto.Vuelo2DTO;
import ar.com.pipelinetech.dedalo.entities.Vuelo2;

@Service
public class VueloService {

	@Autowired
	private AeronaveService aeronaveService;

	@Autowired
	private InsumoSanitarioService insumoSanitarioService;
	
	@Autowired
	private InsumoVentaService insumoVentaService;
	
	@Autowired InsumoVueloService insumoVueloService;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Vuelo2DTO> getVuelos2() {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");

		Object response = CallExternalServices.doGetService("https://proyecto-icarus.herokuapp.com/vuelos", headers,
				new ParameterizedTypeReference<List<Vuelo2>>() {
				});

		List<Vuelo2> resp = (List<Vuelo2>) ((ResponseEntity) response).getBody();
		List<Vuelo2DTO> respuesta = Vuelo2Assembler.listEntityToDTOList(resp);

		return respuesta;
	}

	public Vuelo2DTO getEstado(String codigoVuelo) {

		List<Vuelo2DTO> vuelosDTO = this.getVuelos2();
		Vuelo2DTO respuesta = new Vuelo2DTO();

		for (Vuelo2DTO v : vuelosDTO) {
			if (v.getIdvuelo().equals(codigoVuelo)) {
				respuesta = v;
			}
		}
		return respuesta;
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public String setearInsumos(String codigoVuelo) {
		List<InsumoSanitarioDTO> insumosS = insumoSanitarioService.getInsumosSanitario();
		List<InsumoVentaDTO> insumosV = insumoVentaService.getInsumosVenta();
		
		if(this.getCapacidadMaxima(codigoVuelo) == 0) {
			return "No se pudo obtener la capacidad máxima de la aeronave";
		}
		else {
		for(int i=0; i<insumosS.size(); i++) {
			if((insumosS.get(i)).getCategoriaSanitario()==0) {
				insumoVueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, insumosS.get(i).getId(), codigoVuelo, this.getCapacidadMaxima(codigoVuelo), this.getCapacidadMaxima(codigoVuelo), insumosS.get(i)));
			}
			else {
			Random r = new Random();
			int result = r.nextInt(10-1) + 1;
			insumoVueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, insumosS.get(i).getId(), codigoVuelo, result, result, insumosS.get(i)));	
			}		
		}
		for(int i=0; i<insumosV.size(); i++) {
			Random r = new Random();
			if((insumosV.get(i).getCategoriaVentas()==0 || insumosV.get(i).getCategoriaVentas()==4)) {
				int result = r.nextInt(this.getCapacidadMaxima(codigoVuelo)*30/100-this.getCapacidadMaxima(codigoVuelo)*10/100) + this.getCapacidadMaxima(codigoVuelo)*10/100;
				insumoVueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, insumosV.get(i).getId(), codigoVuelo, result, result, insumosV.get(i)));	
			}
			else {
				int result = r.nextInt(this.getCapacidadMaxima(codigoVuelo)*10/100-1) + 1;
				insumoVueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, insumosV.get(i).getId(), codigoVuelo, result, result, insumosV.get(i)));	
			}
			
			}
		}
		return "Carga exitosa de insumos";
		
}
	
	private int getCapacidadMaxima(String codigoVuelo) {
		List<Vuelo2DTO> vuelosDTO = this.getVuelos2();
		String matriculaAeronave = "";
		int capacidad = 0;
		for (Vuelo2DTO v : vuelosDTO) {
			if (v.getIdvuelo().equals(codigoVuelo)) {
				matriculaAeronave = v.getAeronave_matricula_fk();
			}
		}
		if (matriculaAeronave != "" || matriculaAeronave != null) {
			List<Aeronave2DTO> aeronaves = aeronaveService.getAeronaves2();
			for (Aeronave2DTO a : aeronaves) {
				if (a.getMatricula().equals(matriculaAeronave)) {
					capacidad = a.getCapacidadreal();
				}
			}
		}
		return capacidad;
	}
}
