package ar.com.pipelinetech.dedalo.services;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class EncriptadorService {

	 public static SecureRandom sr = new SecureRandom();

	    public static String encriptar(String clave, byte[] iv, String value) {
	        try {
	        	//EncriptadorService.sr.nextBytes(iv);
	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            SecretKeySpec sks = new SecretKeySpec(clave.getBytes("UTF-8"), "AES");
	            cipher.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(iv));

	            byte[] encriptado = cipher.doFinal(value.getBytes());
	            return DatatypeConverter.printBase64Binary(encriptado);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return null;
	    }

	    public static String decriptar(String clave, byte[] iv, String encriptado) {
	        try {
	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            SecretKeySpec sks = new SecretKeySpec(clave.getBytes("UTF-8"), "AES");
	            cipher.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(iv));

	            byte[] dec = cipher.doFinal(DatatypeConverter.parseBase64Binary(encriptado));
	            return new String(dec);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return null;
	    }
}
