package ar.com.pipelinetech.dedalo.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.InsumoCateringAssembler;
import ar.com.pipelinetech.dedalo.assembler.InsumoSanitarioAssembler;
import ar.com.pipelinetech.dedalo.assembler.InsumoVentaAssembler;
import ar.com.pipelinetech.dedalo.assembler.InsumoVueloAssembler;
import ar.com.pipelinetech.dedalo.dto.InsumoCateringDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoSanitarioDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoVentaDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoVueloDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoCatering;
import ar.com.pipelinetech.dedalo.entities.InsumoSanitario;
import ar.com.pipelinetech.dedalo.entities.InsumoVenta;
import ar.com.pipelinetech.dedalo.entities.InsumoVuelo;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoCateringRepository;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoSanitarioRepository;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoVentaRepository;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoVueloRepository;


@Service
public class InsumoVueloService {
	
	@Autowired
	private IInsumoVueloRepository insumoVueloRepository;
	@Autowired
	private IInsumoVentaRepository insumoVentaRepository;
	@Autowired
	private IInsumoCateringRepository insumoCateringRepository;
	@Autowired
	private IInsumoSanitarioRepository insumoSanitarioRepository;
	
	public List <InsumoVueloDTO> getInsumosVuelos(){
		List<InsumoVuelo> insumosVuelos = insumoVueloRepository.findAll();
		List <InsumoVueloDTO> insumosVueloDTO = InsumoVueloAssembler.listEntityToDTOList(insumosVuelos);
		this.setearInsumosDeCadaTipo(insumosVueloDTO);
		return insumosVueloDTO;
	}
	
	private List<InsumoVueloDTO> setearInsumosDeCadaTipo(List <InsumoVueloDTO> insumosVueloDTO){
		for(InsumoVueloDTO i : insumosVueloDTO) {
			if(insumoSanitarioRepository.existsById(i.getIdInsumo()) == true) {
				InsumoSanitarioDTO insumo = InsumoSanitarioAssembler.toDto(insumoSanitarioRepository.getById(i.getIdInsumo()));
				insumo.setTipo("Sanitario");
				i.setInsumo(insumo);
			}
			
			if(insumoCateringRepository.existsById(i.getIdInsumo()) == true) {
				InsumoCateringDTO insumo =InsumoCateringAssembler.toDto(insumoCateringRepository.getById(i.getIdInsumo()));
				insumo.setTipo("Catering");

				i.setInsumo(insumo);
			}
			
			if(insumoVentaRepository.existsById(i.getIdInsumo()) == true) {
				InsumoVenta insumo = insumoVentaRepository.getById(i.getIdInsumo());
				InsumoVentaDTO insumoDTO = InsumoVentaAssembler.toDto(insumo);
				ConversionMonedaService conversion = new ConversionMonedaService();
				try {
					insumoDTO.setPrecioVentaDolar(conversion.getValorEnDolares(insumo.getPrecioVentaPesos()));
				} catch (IOException e) {
					e.printStackTrace();
				}
				insumoDTO.setTipo("Venta");

				i.setInsumo(insumoDTO);
			}
		}
		return insumosVueloDTO;
	}
	
	@Transactional
	public void updateCantidadFinal(InsumoVueloDTO insumoVueloDTO) {
		InsumoVuelo i = insumoVueloRepository.getById(insumoVueloDTO.getId());
		i.setCantidadFinal(insumoVueloDTO.getCantidadFinal());
		insumoVueloRepository.saveAndFlush(i);		
	}

	@Transactional
	public void nuevoInsumoVuelo(InsumoVueloDTO insumoVueloDTO) {
		
		List<InsumoVuelo> todos = insumoVueloRepository.findAll();
		boolean existe = false;
		for(InsumoVuelo vuelo : todos) {
			if(vuelo.getVuelo().equals(insumoVueloDTO.getVuelo()) && vuelo.getIdInsumo() == insumoVueloDTO.getIdInsumo()) {
				existe= true;
			}
		}
		
		if (!existe) {
			InsumoVuelo i = InsumoVueloAssembler.toEntity(insumoVueloDTO);
			insumoVueloRepository.save(i);
		}
	}

	public void deleteInsumoXVuelo(Integer id) {
		 insumoVueloRepository.deleteById(id);	
	}

	public List<InsumoVueloDTO> getInsumosVentaXVueloByCodigo(String vuelo) {
		List<InsumoVenta> insumosVenta= insumoVentaRepository.findAll();
		List<Integer> ids = new ArrayList<Integer>();
		for(InsumoVenta v: insumosVenta) {
			ids.add(v.getId());
		}
			List<InsumoVuelo> insumosXVuelo = insumoVueloRepository.findAllByVuelo(vuelo, ids);
			List<InsumoVueloDTO> insumosXVueloDTO = InsumoVueloAssembler.listEntityToDTOList(insumosXVuelo);
			this.setearInsumosDeCadaTipo(insumosXVueloDTO);
			return insumosXVueloDTO;
	}

	public List<InsumoVueloDTO> getInsumosSanitarioXVueloByCodigo(String vuelo) {
		List<InsumoSanitario> insumosSanitario= insumoSanitarioRepository.findAll();
		List<Integer> ids = new ArrayList<Integer>();
		for(InsumoSanitario v: insumosSanitario) {
			ids.add(v.getId());
		}
			List<InsumoVuelo> insumosXVuelo = insumoVueloRepository.findAllByVuelo(vuelo, ids);
			List<InsumoVueloDTO> insumosXVueloDTO = InsumoVueloAssembler.listEntityToDTOList(insumosXVuelo);
			this.setearInsumosDeCadaTipo(insumosXVueloDTO);
			return insumosXVueloDTO;
	}	
	
	public List<InsumoVueloDTO> getInsumosCateringXVueloByCodigo(String vuelo) {
		List<InsumoCatering> insumosCatering= insumoCateringRepository.findAll();
		List<Integer> ids = new ArrayList<Integer>();
		for(InsumoCatering v: insumosCatering) {
			ids.add(v.getId());
		}
			List<InsumoVuelo> insumosXVuelo = insumoVueloRepository.findAllByVuelo(vuelo, ids);
			List<InsumoVueloDTO> insumosXVueloDTO = InsumoVueloAssembler.listEntityToDTOList(insumosXVuelo);
			this.setearInsumosDeCadaTipo(insumosXVueloDTO);
			return insumosXVueloDTO;
	}

	public Integer obtenerCantidadFinalInsumos(String vuelo) {
		return insumoVueloRepository.obtenerCantidadFinalInsumos(vuelo);
	}

	public List<InsumoVueloDTO> getInsumosByVuelo(String vuelo) {
		List<InsumoVuelo> insumosVuelos = insumoVueloRepository.findAllByVuelo(vuelo);
		List <InsumoVueloDTO> insumosVueloDTO = InsumoVueloAssembler.listEntityToDTOList(insumosVuelos);
		this.setearInsumosDeCadaTipo(insumosVueloDTO);
		return insumosVueloDTO;
	}

}
