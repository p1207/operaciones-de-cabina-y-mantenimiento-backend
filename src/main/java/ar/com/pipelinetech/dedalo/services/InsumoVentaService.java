package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.InsumoVentaAssembler;
import ar.com.pipelinetech.dedalo.dto.InsumoVentaDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoVenta;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoVentaRepository;


@Service
public class InsumoVentaService {

	@Autowired
	private IInsumoVentaRepository insumoVentaRepository;
	
	public List<InsumoVentaDTO> getInsumosVenta(){
		List<InsumoVenta> insumosVenta = insumoVentaRepository.findAll();
		return InsumoVentaAssembler.listEntityToDTOList(insumosVenta);
	}

	@Transactional
	public InsumoVentaDTO saveInsumo(InsumoVentaDTO insumo) {
		InsumoVenta i = InsumoVentaAssembler.toEntity(insumo);
		return InsumoVentaAssembler.toDto(insumoVentaRepository.save(i));
	}
}
