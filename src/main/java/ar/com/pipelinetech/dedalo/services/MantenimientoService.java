package ar.com.pipelinetech.dedalo.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.pipelinetech.dedalo.assembler.AeronaveXMantenimientoAssembler;
import ar.com.pipelinetech.dedalo.assembler.MantenimientoAssembler;
import ar.com.pipelinetech.dedalo.assembler.SolicitudMantenimientoAssembler;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.dto.AeronaveXMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoXAeronaveDTO;
import ar.com.pipelinetech.dedalo.dto.SolicitudMantenimientoDTO;
import ar.com.pipelinetech.dedalo.entities.AeronaveXMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Mantenimiento;
import ar.com.pipelinetech.dedalo.entities.SolicitudMantenimiento;
import ar.com.pipelinetech.dedalo.enums.EstadoSolicitudMantenimiento;
import ar.com.pipelinetech.dedalo.interfaces.AeronaveXMantenimientoRepository;
import ar.com.pipelinetech.dedalo.interfaces.IMantenimientoRepository;
import ar.com.pipelinetech.dedalo.interfaces.SolicitudMantenimientoRepository;

@Service
public class MantenimientoService {

	@Autowired
	private IMantenimientoRepository mantenimientoRepository;
	
	@Autowired
	private SolicitudMantenimientoRepository solicitudMantenimientoRepository;
	
	@Autowired
	private AeronaveXMantenimientoRepository aeronaveXMantenimientoRepository;
	
	@Autowired
	private AeronaveService aeronaveService;
	public List<MantenimientoDTO> getMantenimientos(){
		List<Mantenimiento> mantenimientos = mantenimientoRepository.findAll();
		return MantenimientoAssembler.listEntityToDTOList(mantenimientos);
	}
	
	@Transactional
	public void saveMantenimiento(MantenimientoDTO mantenimiento) {
		Mantenimiento m = MantenimientoAssembler.toEntity(mantenimiento);
		m.setActivo(1);
		mantenimientoRepository.save(m);
	}
	
	public void softDeleteMantenimiento(Integer id) {
		mantenimientoRepository.deleteById(id);
	}
	
	public void updateMantenimiento(MantenimientoDTO mantenimiento) {
		Mantenimiento m = MantenimientoAssembler.toEntity(mantenimiento);
		m.setActivo(1);
		mantenimientoRepository.saveAndFlush(m);
	}
	
	public MantenimientoXAeronaveDTO solicitudMantenimientoxAeronave(String matricula) {
		
		List<SolicitudMantenimiento> solicitudes = solicitudMantenimientoRepository.findAllByMatricula(matricula);
		MantenimientoXAeronaveDTO result = new MantenimientoXAeronaveDTO();
		List<SolicitudMantenimientoDTO> solicitudesDto = SolicitudMantenimientoAssembler.listEntityToDTOList(solicitudes);
		
		boolean aprobados = true;
		for(SolicitudMantenimientoDTO s : solicitudesDto) {
			aprobados = aprobados && (s.getEstado() == EstadoSolicitudMantenimiento.APROBADA);
		}
		
		result.setAprobados(aprobados);
		result.setMantenimientos(solicitudesDto);

		return result;
	}
	
	public void saveAeronaveXMantenimiento(AeronaveXMantenimientoDTO aeronavexmantenimiento) {
		AeronaveXMantenimiento aeronavexmantenimientoDTO = AeronaveXMantenimientoAssembler.toEntity(aeronavexmantenimiento);
		aeronaveXMantenimientoRepository.save(aeronavexmantenimientoDTO);
	}

	public List<AeronaveXMantenimientoDTO> mantenimientosxAeronave(String matricula) {
		List<AeronaveXMantenimiento> mantenimientosXAeronave = aeronaveXMantenimientoRepository.findAllByMatricula(matricula);
		return AeronaveXMantenimientoAssembler.listEntityToDTOList(mantenimientosXAeronave);
	}

	public void saveAllAeronaveXMantenimiento() {
		List<Aeronave2DTO> aeronaves = aeronaveService.getAeronaves2();
		List<MantenimientoDTO> mantenimientos = MantenimientoAssembler.listEntityToDTOList(mantenimientoRepository.findAll());
		for(Aeronave2DTO a: aeronaves) {
			for(MantenimientoDTO m : mantenimientos) {
				AeronaveXMantenimiento aeronavexmantenimientoDTO = AeronaveXMantenimientoAssembler.toEntity(new AeronaveXMantenimientoDTO(null,a.getMatricula(),m));
				aeronaveXMantenimientoRepository.save(aeronavexmantenimientoDTO);
			}
		}
	}
	
}
