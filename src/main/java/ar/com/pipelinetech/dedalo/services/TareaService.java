package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.TareaAssembler;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.entities.Tarea;
import ar.com.pipelinetech.dedalo.interfaces.ITareaRepository;

@Service
public class TareaService {
	
	@Autowired
	private ITareaRepository tareaRepository;
	
	public List<TareaDTO> getTareas(){
		List<Tarea> tareas = tareaRepository.findAll();
		return TareaAssembler.listEntityToDTOList(tareas);
	}
	
	public void saveHabilidad(MantenimientoDTO mantenimiento) {
	}
}
