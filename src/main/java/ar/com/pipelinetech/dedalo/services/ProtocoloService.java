package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.ProtocoloAssembler;
import ar.com.pipelinetech.dedalo.dto.ProtocoloDTO;
import ar.com.pipelinetech.dedalo.entities.Protocolo;
import ar.com.pipelinetech.dedalo.interfaces.IProtocoloRepository;

@Service
public class ProtocoloService {
	@Autowired
	private IProtocoloRepository protocoloRepository;
	
	public List<ProtocoloDTO> getProtocolos(){
		List<Protocolo> protocolos = protocoloRepository.findAll();
		List<ProtocoloDTO> protocolosDTO = ProtocoloAssembler.listEntityToDTOList(protocolos); 
		return protocolosDTO;
	}

	public ProtocoloDTO getProtocoloId(Integer id) {
		Protocolo protocolo = protocoloRepository.getById(id);
		return ProtocoloAssembler.toDto(protocolo);
	}

	@Transactional
	public void saveProtocolo(ProtocoloDTO protocoloDTO) {
		Protocolo p = ProtocoloAssembler.toEntity(protocoloDTO);
		protocoloRepository.save(p);
	}
		


}
