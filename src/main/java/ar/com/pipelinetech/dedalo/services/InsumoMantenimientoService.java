package ar.com.pipelinetech.dedalo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.pipelinetech.dedalo.assembler.InsumoMantenimientoAssembler;
import ar.com.pipelinetech.dedalo.dto.InsumoMantenimientoDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoMantenimiento;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoMantenimientoRepository;


@Service
public class InsumoMantenimientoService {

	@Autowired
	private IInsumoMantenimientoRepository insumoMantenimientoRepository;
	
	public List<InsumoMantenimientoDTO> getInsumos(){
		List<InsumoMantenimiento> insumos = insumoMantenimientoRepository.findAll();
		return InsumoMantenimientoAssembler.listEntityToDTOList(insumos);
	}
}
