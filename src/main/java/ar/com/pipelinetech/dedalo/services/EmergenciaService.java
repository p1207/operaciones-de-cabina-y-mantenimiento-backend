package ar.com.pipelinetech.dedalo.services;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.EmergenciaAssembler;
import ar.com.pipelinetech.dedalo.dto.EmergenciaDTO;
import ar.com.pipelinetech.dedalo.entities.Emergencia;
import ar.com.pipelinetech.dedalo.interfaces.IEmergenciaRepository;


@Service
public class EmergenciaService {
	
	@Autowired
	private IEmergenciaRepository emergenciaRepository;
	
	public List<EmergenciaDTO> getEmergencias(){
		List<Emergencia> emergencias = emergenciaRepository.findAll();
		return EmergenciaAssembler.listEntityToDTOList(emergencias);
	}
	
	public List<EmergenciaDTO> getEmergenciasByVuelo(String codigo) {
		List<Emergencia> emergencias = emergenciaRepository.findAllByIdVuelo(codigo);
		return EmergenciaAssembler.listEntityToDTOList(emergencias);
	}
	
	@Transactional
	public void saveEmergencia(EmergenciaDTO emergenciaDTO) {
		Emergencia e = EmergenciaAssembler.toEntity(emergenciaDTO);
		emergenciaRepository.save(e);
	}

}

