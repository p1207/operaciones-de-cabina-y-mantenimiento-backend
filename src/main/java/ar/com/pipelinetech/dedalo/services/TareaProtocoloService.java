package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.TareaProtocoloAssembler;
import ar.com.pipelinetech.dedalo.dto.TareaProtocoloDTO;
import ar.com.pipelinetech.dedalo.entities.TareaProtocolo;
import ar.com.pipelinetech.dedalo.interfaces.ITareaProtocoloRepository;

@Service
public class TareaProtocoloService {
	
	@Autowired
	private ITareaProtocoloRepository tareaProtocoloRepository;
	
	public List<TareaProtocoloDTO> getTareasProtocolo(){
		List<TareaProtocolo> tareasProtocolo = tareaProtocoloRepository.findAll();
		return TareaProtocoloAssembler.listEntityToDTOList(tareasProtocolo);
	}

	public List<TareaProtocoloDTO> getTareasProtocoloId(Integer id) {
		List<TareaProtocolo> tareasProtocolo = tareaProtocoloRepository.findAllByIdProtocolo(id);
		return TareaProtocoloAssembler.listEntityToDTOList(tareasProtocolo);
	}

	@Transactional
	public void saveTareaProtocolo(TareaProtocoloDTO tareaProtocoloDTO) {
			TareaProtocolo t = TareaProtocoloAssembler.toEntity(tareaProtocoloDTO);
			tareaProtocoloRepository.save(t);
	}
		
}

