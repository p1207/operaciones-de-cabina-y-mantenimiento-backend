package ar.com.pipelinetech.dedalo.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.pipelinetech.dedalo.CallExternalServices;
import ar.com.pipelinetech.dedalo.assembler.InsumoCateringAssembler;
import ar.com.pipelinetech.dedalo.assembler.PasajeroAssembler;
import ar.com.pipelinetech.dedalo.dto.CantidadMenuDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoCateringDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoVueloDTO;
import ar.com.pipelinetech.dedalo.dto.MenuDTO;
import ar.com.pipelinetech.dedalo.dto.PasajeroDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoCatering;
import ar.com.pipelinetech.dedalo.entities.Pasajero;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoCateringRepository;

@Service
public class InsumoCateringService {

	@Autowired
	private IInsumoCateringRepository insumoCateringRepository;
	
	@Autowired
	private InsumoVueloService insumoxvueloService;
	
	public List<InsumoCateringDTO> getInsumosCatering(){
		List<InsumoCatering> insumosCatering = insumoCateringRepository.findAll();
		return InsumoCateringAssembler.listEntityToDTOList(insumosCatering);
	}
	
	public List<InsumoCateringDTO> getInsumosCateringByTipoMenu(int tipoMenu){
		List<InsumoCatering> insumosCatering = insumoCateringRepository.findAllByTipoMenu(tipoMenu);
		return InsumoCateringAssembler.listEntityToDTOList(insumosCatering);
	}

	@Transactional
	public InsumoCateringDTO saveInsumo(InsumoCateringDTO insumo) {
		InsumoCatering i = InsumoCateringAssembler.toEntity(insumo);
		return InsumoCateringAssembler.toDto(insumoCateringRepository.save(i));

	}
	
	@SuppressWarnings({"unchecked","rawtypes"})
	public List<PasajeroDTO> getPasajeros(String codigoVuelo){
		
		Map<String, String> params = new HashMap<>();
		params.put("codigoVuelo", codigoVuelo);
		
		Object response = CallExternalServices.doGetService("https://opr-terrestres.herokuapp.com/v1/losilegales/pasajeros/{codigoVuelo}", params, MediaType.APPLICATION_JSON,
				new ParameterizedTypeReference <List<Pasajero>>() {
        });
		
		List<Pasajero> resp = (List<Pasajero>)((ResponseEntity)response).getBody();
		List<PasajeroDTO> respuesta = PasajeroAssembler.listEntityToDTOList(resp);
		
		return respuesta; 
	}
	
	private MenuDTO getAsientosMenu(String codigoVuelo) {
		List<PasajeroDTO> pasajeros = this.getPasajeros(codigoVuelo);
		MenuDTO menus = new MenuDTO();
		
		for(PasajeroDTO p :pasajeros) {
			
			String menu = p.getAlimentacion();
			String asiento = p.getAsiento();
			
			if(menu.equals("celiaco")) {
				menus.addCeliadoAsientos(asiento);
			}
			if(menu.equals("vegetariano")) {
				menus.addVegetarianoAsientos(asiento);
			}
			if(menu.equals("estandar")) {
				menus.addEstandarAsientos(asiento);
			}
			if(menu.equals("vegano")) {
				menus.addVeganoAsientos(asiento);
			}
		}
		
		return menus; 
	}
	
	public CantidadMenuDTO getMenus (String codigoVuelo) {
		MenuDTO menus = this.getAsientosMenu(codigoVuelo);
		CantidadMenuDTO respuesta = new CantidadMenuDTO();
		respuesta.setCeliaco(menus.getCeliacoAsientos().size());
		respuesta.setEstandar(menus.getEstandarAsientos().size());;
		respuesta.setVegano(menus.getVeganoAsientos().size());
		respuesta.setVegetariano(menus.getVegetarianoAsientos().size());
		return respuesta;
	}
	
	public String setearAlimentacion(String codigoVuelo) {
		List<InsumoCateringDTO> veganos = getInsumosCateringByTipoMenu(0);
		List<InsumoCateringDTO> vegetarianos = getInsumosCateringByTipoMenu(1);
		List<InsumoCateringDTO> celiacos = getInsumosCateringByTipoMenu(2);
		List<InsumoCateringDTO> estandars = getInsumosCateringByTipoMenu(3);
		Random r = new Random();
		InsumoCateringDTO seleccionVegana = veganos.get(r.nextInt(veganos.size()-1) + 0);
		InsumoCateringDTO seleccionVegetariana = vegetarianos.get(r.nextInt(vegetarianos.size()-1) + 0);
		InsumoCateringDTO seleccionCeliacos = celiacos.get(r.nextInt(celiacos.size()-1) + 0);
		InsumoCateringDTO seleccionEstandar = estandars.get(r.nextInt(estandars.size()-1) + 0);
		MenuDTO menues = getAsientosMenu(codigoVuelo);
		insumoxvueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, seleccionVegana.getId(), codigoVuelo, menues.getVeganoAsientos().size(), menues.getVeganoAsientos().size(), seleccionVegana));
		insumoxvueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, seleccionVegetariana.getId(), codigoVuelo, menues.getVegetarianoAsientos().size(), menues.getVegetarianoAsientos().size(), seleccionVegetariana));
		insumoxvueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, seleccionCeliacos.getId(), codigoVuelo, menues.getCeliacoAsientos().size(), menues.getCeliacoAsientos().size(), seleccionCeliacos));
		insumoxvueloService.nuevoInsumoVuelo(new InsumoVueloDTO(null, seleccionEstandar.getId(), codigoVuelo, menues.getEstandarAsientos().size(), menues.getEstandarAsientos().size(), seleccionEstandar));
		
		return "Guardados con éxito";
	}
	
}
