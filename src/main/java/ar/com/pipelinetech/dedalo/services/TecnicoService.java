package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.TecnicoAssembler;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.entities.Tecnico;
import ar.com.pipelinetech.dedalo.interfaces.ITecnicoRepository;

@Service
public class TecnicoService {
	@Autowired
	private ITecnicoRepository tecnicoRepository;

	public List<TecnicoDTO> getTecnicos() {
		List<Tecnico> tecnicos = tecnicoRepository.findAll();
		return TecnicoAssembler.listEntityToDTOList(tecnicos);
	}
}
