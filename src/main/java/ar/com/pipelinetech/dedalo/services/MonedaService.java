package ar.com.pipelinetech.dedalo.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ar.com.pipelinetech.dedalo.dto.ConversionMonedaDTO;

@Service
public class MonedaService {

	private static RestTemplate rest = new RestTemplate();

	public BigDecimal getValorEnPesos(BigDecimal valor) throws IOException {
		ConversionMonedaDTO conversion = conversion();
		return new BigDecimal(conversion.getCompra()).multiply(valor);
	}

	public BigDecimal getValorEnDolares(BigDecimal valor) throws IOException {
		ConversionMonedaDTO conversion = conversion();
		if(valor.compareTo(new BigDecimal(conversion.getCompra())) >= 0) {
		return valor.divide(new BigDecimal(conversion.getCompra()),2, RoundingMode.HALF_UP);
		}
		return new BigDecimal(0);
	}
	
	private static ConversionMonedaDTO conversion() {
		String URL = "https://api-dolar-argentina.herokuapp.com/api/dolaroficial";
		try {
			ResponseEntity<ConversionMonedaDTO> response = rest.exchange(URL, HttpMethod.GET, null,
					ConversionMonedaDTO.class);
			ConversionMonedaDTO conversion = response.getBody();
			return conversion;
		} catch (Exception e) {
			System.out.println("Error:" + e);
		}
		return null;

	}

}
