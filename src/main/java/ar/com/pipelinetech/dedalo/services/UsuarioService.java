package ar.com.pipelinetech.dedalo.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.google.gson.Gson;
import ar.com.pipelinetech.dedalo.CallExternalServices;
import ar.com.pipelinetech.dedalo.assembler.PasajeroAssembler;
import ar.com.pipelinetech.dedalo.assembler.RolAssembler;
import ar.com.pipelinetech.dedalo.assembler.TecnicoAssembler;
import ar.com.pipelinetech.dedalo.assembler.UsuarioAssembler;
import ar.com.pipelinetech.dedalo.config.CustomRevisionListener;
import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.dto.IdPersonalDTO;
import ar.com.pipelinetech.dedalo.dto.PasajeroDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.dto.UsuarioDTO;
import ar.com.pipelinetech.dedalo.dto.VueloXTripulanteDTO;
import ar.com.pipelinetech.dedalo.entities.Pasajero;
import ar.com.pipelinetech.dedalo.entities.Personal;
import ar.com.pipelinetech.dedalo.entities.Tecnico;
import ar.com.pipelinetech.dedalo.entities.Usuario;
import ar.com.pipelinetech.dedalo.entities.VueloXTripulante;
import ar.com.pipelinetech.dedalo.interfaces.IRolRepository;
import ar.com.pipelinetech.dedalo.interfaces.ITecnicoRepository;
import ar.com.pipelinetech.dedalo.interfaces.IUsuarioRepository;

@Service
public class UsuarioService {

	private static String clave = "FooBar1234567890"; // 128 bit
	private static  byte[] iv = new byte[16];
	
	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	@Autowired
	private ITecnicoRepository tecnicoRepository;
	
	@Autowired
	private IRolRepository rolRepository;
	
	//@Autowired
	//private CustomRevisionListener customRevisionListener;
	
	@Transactional
	public UsuarioDTO saveUsuario(UsuarioDTO usuarioDTO) {
  
		String contraseña = usuarioDTO.getContraseña();
		String contraseñaEncriptada = EncriptadorService.encriptar(clave, iv, usuarioDTO.getUsername()+ contraseña);
		Usuario u = UsuarioAssembler.toEntity(usuarioDTO);
		
		u.setContraseña(contraseñaEncriptada);
		return UsuarioAssembler.toDto(usuarioRepository.save(u));
	}
	
	@Transactional
	public TecnicoDTO saveUsuarioTecnico(TecnicoDTO usuarioDTO) {
  
		String contraseña = usuarioDTO.getContraseña();
		String contraseñaEncriptada = EncriptadorService.encriptar(clave, iv, usuarioDTO.getUsername()+ contraseña);
		Tecnico u = TecnicoAssembler.toEntity(usuarioDTO);
		
		u.setContraseña(contraseñaEncriptada);
		return TecnicoAssembler.toDto(tecnicoRepository.save(u));
	}

	public UsuarioDTO autenticarUsuario(UsuarioDTO usuarioDTO) {
		String contraseña = usuarioDTO.getContraseña();
		String contraseñaEncriptada = EncriptadorService.encriptar(clave, iv, usuarioDTO.getUsername()+ contraseña);
		Usuario usuarioCoincidente = this.usuarioRepository.findByUsername(usuarioDTO.getUsername());
		UsuarioDTO usuarioCoincidenteDTO = new UsuarioDTO();
		if(usuarioCoincidente != null) {
			usuarioCoincidenteDTO = UsuarioAssembler.toDto(this.usuarioRepository.findByUsername(usuarioDTO.getUsername()));
			if(!usuarioCoincidenteDTO.getContraseña().equals(contraseñaEncriptada)) {
				return new UsuarioDTO();
			}
			usuarioCoincidenteDTO.setContraseña("");
			usuarioCoincidenteDTO.setUsername("");
		}
		else {
			return new UsuarioDTO();
		}
		//customRevisionListener.setUsuarioActual(usuarioCoincidenteDTO.getUsername());
		return usuarioCoincidenteDTO;
	}

	public List<UsuarioDTO> getUsuarios() {
		List<Usuario> usuario = usuarioRepository.findAll();
		return UsuarioAssembler.listEntityToDTOList(usuario);
	}
	
	public String saveUsuariosTripulantes() {
		List<Personal> personales = this.getPersonal();
		for(Personal p : personales) {
			if(!existeEnUsuarios(p.getIdpersonal())) {
				if(p.getPosicion().equals("auxiliar")) {
					RolDTO rol = RolAssembler.toDto(rolRepository.findByNombre("Auxiliar"));
					this.saveUsuario(new UsuarioDTO(null, p.getNombre(), p.getApellido(), p.getMail(), p.getNombre()+p.getApellido()+p.getIdpersonal(), p.getApellido(), rol, p.getIdpersonal(), new ArrayList<HabilidadDTO>(),"tripulante"));
				}
				if(p.getPosicion().equals("piloto")) {
					RolDTO rol = RolAssembler.toDto(rolRepository.findByNombre("Piloto"));
					this.saveUsuario(new UsuarioDTO(null, p.getNombre(), p.getApellido(), p.getMail(), p.getNombre()+p.getApellido()+p.getIdpersonal(), p.getApellido(), rol,  p.getIdpersonal(), new ArrayList<HabilidadDTO>(), "tripulante"));
				}
				if(p.getPosicion().equals("co-piloto")) {
					RolDTO rol = RolAssembler.toDto(rolRepository.findByNombre("Copiloto"));
					this.saveUsuario(new UsuarioDTO(null, p.getNombre(), p.getApellido(), p.getMail(), p.getNombre()+p.getApellido()+p.getIdpersonal(), p.getApellido(), rol, p.getIdpersonal(), new ArrayList<HabilidadDTO>(), "tripulante"));
				}
			}
		}
		return "Se guardan los usuarios tripulantes";
	}
	
	private boolean existeEnUsuarios(Integer idPersonal) {
		boolean existe = false;
		List<Usuario> usuarios = usuarioRepository.findAll();
		for(Usuario u : usuarios) {
			if(u.getIdTripulacion()!= null && idPersonal == u.getIdTripulacion()) {
				existe = true;
			}
		}
		return existe;
	}
	@SuppressWarnings({"unchecked","rawtypes"})
	public List<Personal> getPersonal(){
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");

		Object response = CallExternalServices.doGetService("https://proyecto-icarus.herokuapp.com/InfoUsuariosPersonal", headers,
				new ParameterizedTypeReference<List<Personal>>() {
				});

		List<Personal> resp = (List<Personal>) ((ResponseEntity) response).getBody();		
		return resp; 
	}
	
	@SuppressWarnings({"unchecked","rawtypes"})
	public VueloXTripulante [] getTripulantesVuelo(IdPersonalDTO idpersonal_fk) {
		Object response = CallExternalServices.doPostService("https://proyecto-icarus.herokuapp.com/VuelosPorTripulante", idpersonal_fk, MediaType.APPLICATION_JSON , String.class);		
		Gson gson = new Gson();
		VueloXTripulante []  lista = null;
		
		if (response  != null) {
			lista = gson.fromJson((String)response, VueloXTripulante[].class); 
			
		}
		return lista; 
	}

}
