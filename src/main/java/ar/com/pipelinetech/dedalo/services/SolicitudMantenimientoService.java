package ar.com.pipelinetech.dedalo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.MantenimientoAssembler;
import ar.com.pipelinetech.dedalo.assembler.SolicitudMantenimientoAssembler;
import ar.com.pipelinetech.dedalo.assembler.TecnicoAssembler;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.dto.AeronaveXMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.SolicitudMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.entities.SolicitudMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Tecnico;
import ar.com.pipelinetech.dedalo.enums.EstadoSolicitudMantenimiento;
import ar.com.pipelinetech.dedalo.interfaces.IMantenimientoRepository;
import ar.com.pipelinetech.dedalo.interfaces.ITecnicoRepository;
import ar.com.pipelinetech.dedalo.interfaces.SolicitudMantenimientoRepository;

@Service
public class SolicitudMantenimientoService {

	@Autowired
	private SolicitudMantenimientoRepository solicitudMantenimientoRepository;

	@Autowired
	private ITecnicoRepository tecnicoRepository;
	
	@Autowired
	private IMantenimientoRepository mantenimientoRepository;
	
	@Autowired
	private AeronaveService aeronaveService;
	
	@Autowired
	private MantenimientoService mantenimientoService;
	
	public List<SolicitudMantenimientoDTO> getSolicitudes() {
		List<SolicitudMantenimiento> solicitudes =  solicitudMantenimientoRepository.findAll();
		return SolicitudMantenimientoAssembler.listEntityToDTOList(solicitudes);
	}
	
	public SolicitudMantenimientoDTO getSolicitudById(Integer id) {
		SolicitudMantenimiento solicitud = solicitudMantenimientoRepository.getById(id);
		return SolicitudMantenimientoAssembler.toDto(solicitud);
	}

	public SolicitudMantenimientoDTO saveSolicitudMantenimiento(SolicitudMantenimientoDTO solicitud) {
		solicitud.setTecnico(TecnicoAssembler.toDto(tecnicoRepository.getById(solicitud.getIdTecnico())));
		solicitud.setMantenimiento(MantenimientoAssembler.toDto(mantenimientoRepository.getById(solicitud.getIdMantenimiento())));
		
		SolicitudMantenimiento s = SolicitudMantenimientoAssembler.toEntity(solicitud);
		Optional<SolicitudMantenimiento> guardada = solicitudMantenimientoRepository.findById(solicitud.getId());
		s.setActivo(1);
		if(guardada == null) {
			s.setFecha(new Date());
		}
		else {
			s.setFecha(guardada.get().getFecha());
		}
		
		return SolicitudMantenimientoAssembler.toDto(solicitudMantenimientoRepository.save(s));
	}
	
	public ITecnicoRepository getTecnicoRepository() {
		return tecnicoRepository;
	}

	public void setTecnicoRepository(ITecnicoRepository tecnicoRepository) {
		this.tecnicoRepository = tecnicoRepository;
	}

	public void deleteSolicitudMantenimiento(Integer id) {
		solicitudMantenimientoRepository.deleteById(id);
	}

	public List<SolicitudMantenimientoDTO> getSolicitudByIdTecnico(Integer id) {
		List<SolicitudMantenimiento> solicitudes =  solicitudMantenimientoRepository.findSolicitudesByIdTecnico(id);
		return SolicitudMantenimientoAssembler.listEntityToDTOList(solicitudes);
	}
		
	public String saveSolicitudesMasivo() {
		List<Aeronave2DTO> aeronaves = aeronaveService.getAeronaves2();
		for(Aeronave2DTO a : aeronaves) {
			List<AeronaveXMantenimientoDTO> mantenimientos = this.mantenimientoService.mantenimientosxAeronave(a.getMatricula());
			Integer axm = (int) (Math.random()*(mantenimientos.size()-1));
				int contador=0;
				while(contador<1) {
					Integer numeroAleatorio = (int) (Math.random()*2+1);
					Integer modulo = (int) (Math.random()*10+1);
					TecnicoDTO tecnico = this.traerTecnicoAlAzar();
					String[] descripciones = {"Inspección rápida en la que se comprueban aspectos generales de la aeronave.","Revisión, arreglo o reemplazo del tren de aterrizaje", "Revisiones que se realizan proximadamente cada 6-8 meses. Tiene una duración de entre 1 y 3 días.", "Revisar el nivel de aceite entre 15 y 30 minutos después de apagar los motores.", 
							"Pueden incluir controles más detallados que los controles diarios."};
					Integer descripcion =  (int) (Math.random()*(descripciones.length-1));
						this.saveSolicitudMantenimiento(new SolicitudMantenimientoDTO (null,descripciones[descripcion], a.getMatricula(), mantenimientos.get(axm).getMantenimiento(), mantenimientos.get(axm).getMantenimiento().getId(), new Date(), numeroAleatorio, modulo, "Ninguna", EstadoSolicitudMantenimiento.REGISTRADA, Integer.valueOf(0),tecnico, tecnico.getId(), new ArrayList<TareaDTO>(),1 ));
					contador = contador+1;
				}
		}
		return "Se guardaron todas las solicitudes";
	}

	private TecnicoDTO traerTecnicoAlAzar() {
		List<Tecnico> tecnicos = tecnicoRepository.findAll();
		Integer seleccionado = (int) (Math.random()*(tecnicos.size()-1));
		System.out.println("tecnicos: "+ tecnicos.get(seleccionado));
		return TecnicoAssembler.toDto(tecnicos.get(seleccionado));
	}
}
