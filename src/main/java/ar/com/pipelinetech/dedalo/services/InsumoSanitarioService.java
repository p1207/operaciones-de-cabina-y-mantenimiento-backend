package ar.com.pipelinetech.dedalo.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ar.com.pipelinetech.dedalo.assembler.InsumoSanitarioAssembler;
import ar.com.pipelinetech.dedalo.dto.InsumoSanitarioDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoSanitario;
import ar.com.pipelinetech.dedalo.interfaces.IInsumoSanitarioRepository;

@Service
public class InsumoSanitarioService {

	@Autowired
	private IInsumoSanitarioRepository insumoSanitarioRepository;
	
	public List<InsumoSanitarioDTO> getInsumosSanitario(){
		List<InsumoSanitario> insumosSanitario = insumoSanitarioRepository.findAll();
		return InsumoSanitarioAssembler.listEntityToDTOList(insumosSanitario);
	}
	
	@Transactional
	public InsumoSanitarioDTO saveInsumo(InsumoSanitarioDTO insumo) {
		InsumoSanitario m = InsumoSanitarioAssembler.toEntity(insumo);
		return InsumoSanitarioAssembler.toDto(insumoSanitarioRepository.save(m));
	}
}