package ar.com.pipelinetech.dedalo.services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.com.pipelinetech.dedalo.assembler.ControlAssembler;
import ar.com.pipelinetech.dedalo.assembler.ControlXVueloAssembler;
import ar.com.pipelinetech.dedalo.assembler.RolAssembler;
import ar.com.pipelinetech.dedalo.dto.ControlDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.ControlXVueloDTO;
import ar.com.pipelinetech.dedalo.entities.Control;
import ar.com.pipelinetech.dedalo.entities.Rol;
import ar.com.pipelinetech.dedalo.entities.TareaControl;
import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;
import ar.com.pipelinetech.dedalo.enums.TipoControl;
import ar.com.pipelinetech.dedalo.interfaces.ControlXVueloRepository;
import ar.com.pipelinetech.dedalo.interfaces.IControlRepository;
import ar.com.pipelinetech.dedalo.interfaces.IRolRepository;

@Service
public class ControlService {
	
	@Autowired
	private IControlRepository controlRepository;
	
	@Autowired
	private ControlXVueloRepository controlXVueloRepository;
	
	@Autowired
	private IRolRepository rolRepository;

	public List<ControlDTO> getControles() {
		List<Control> controles = controlRepository.findAllActivos(true);
		List<ControlDTO> controlesDTO= ControlAssembler.listEntityToDTOList(controles);
		return controlesDTO;
	}
	
	@Transactional
	public ControlDTO saveControl(ControlDTO control) {	
		Control c = ControlAssembler.toEntity(control);
		c.setRol(RolAssembler.toEntity(control.getRol()));
		c.setActivo(true);
		ControlDTO controlDTO= ControlAssembler.toDto(controlRepository.save(c));
		if(controlDTO.getTipoControl() == TipoControl.EVENTUAL) {
			controlDTO.setIdVueloSeleccionado(control.getIdVueloSeleccionado());
			asignarAVuelo(controlDTO);
		}
		controlDTO.setIdRol(controlDTO.getRol().getId());
		return controlDTO;
	}

	private void asignarAVuelo(ControlDTO controlDTO) {
		ControlXVueloDTO controlXVuelo = new ControlXVueloDTO();
		controlXVuelo.setId_control(controlDTO.getId());
		controlXVuelo.setVuelo(controlDTO.getIdVueloSeleccionado());
		controlXVuelo.setEstado(EstadoControlXVuelo.PENDIENTE);
		controlXVuelo.setFecha_creacion(new Date());
		controlXVueloRepository.save(ControlXVueloAssembler.toEntity(controlXVuelo));
	}

	public void softDeleteControl(Integer id) {
		controlRepository.deleteById(id);		
	}
	
	private void setRolTareas(List<TareaControl> list) {
		for(TareaControl t: list) {
			t.setRol(rolRepository.getById(t.getId()));
		}
	}
	
	public List<RolDTO> getRoles() {
		List<Rol> roles = rolRepository.findAll();
		return RolAssembler.listEntityToDTOList(roles);
	}

}