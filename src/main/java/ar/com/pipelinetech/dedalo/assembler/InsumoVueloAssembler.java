package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.InsumoVueloDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoVuelo;


@Component
public class InsumoVueloAssembler {


	private static ModelMapper mapper = new ModelMapper();
	
	
	public static InsumoVueloDTO toDto(InsumoVuelo insumoVuelo) {
		InsumoVueloDTO insumoVueloDTO = mapper.map(insumoVuelo, InsumoVueloDTO.class);
		return insumoVueloDTO;
	}

	public static InsumoVuelo toEntity(InsumoVueloDTO insumoVueloDTO) {
		InsumoVuelo insumoVuelo = mapper.map(insumoVueloDTO, InsumoVuelo.class);
		return insumoVuelo;
	}

	public static List<InsumoVuelo> listDTOToEntityList(List<InsumoVueloDTO> insumosVueloDTO) {
		List<InsumoVuelo> insumosVuelo = new ArrayList<InsumoVuelo>();
		for(InsumoVueloDTO i : insumosVueloDTO) {
			InsumoVuelo insumoVuelo = toEntity(i);
			insumosVuelo.add(insumoVuelo);
		}	
		return insumosVuelo;
	}

	public static List<InsumoVueloDTO> listEntityToDTOList(List<InsumoVuelo> insumosVuelo) {
		List<InsumoVueloDTO> insumosVueloDTO = new ArrayList<InsumoVueloDTO>();
		for(InsumoVuelo i : insumosVuelo) {
			InsumoVueloDTO insumoVueloDTO = toDto(i);
			insumosVueloDTO.add(insumoVueloDTO);
		}	
		return insumosVueloDTO;
	}
}
