package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.TripulanteDTO;
import ar.com.pipelinetech.dedalo.entities.Tripulante;

@Component
public class TripulanteAssembler {
	
private static ModelMapper mapper = new ModelMapper();
	
	public static TripulanteDTO toDto(Tripulante tripulante) {
		RolDTO rol= RolAssembler.toDto(tripulante.getRol());
		PropertyMap<Tripulante, TripulanteDTO> map = new PropertyMap<Tripulante, TripulanteDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		    }
		};
		TypeMap<Tripulante, TripulanteDTO> typeMap = mapper.getTypeMap(Tripulante.class, TripulanteDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		TripulanteDTO tripulanteDTO = mapper.map(tripulante, TripulanteDTO.class);
		tripulanteDTO.setRol(rol);
		return tripulanteDTO;
	}

	public static Tripulante toEntity(TripulanteDTO tripulanteDTO) {
		Tripulante tripulante = mapper.map(tripulanteDTO, Tripulante.class);
		return tripulante;
	}
	
	public static List<Tripulante> listDTOToEntityList(List<TripulanteDTO> tripulantesDTO) {
		List<Tripulante> tripulantes = new ArrayList<Tripulante>();
		for(TripulanteDTO t : tripulantesDTO) {
			Tripulante tripulante = toEntity(t);
			tripulantes.add(tripulante);
		}	
		return tripulantes;
	}
	
	public static List<TripulanteDTO> listEntityToDTOList(List<Tripulante> tripulantes) {
		List<TripulanteDTO> tripulantesDTO = new ArrayList<TripulanteDTO>();
		for(Tripulante t : tripulantes) {
			TripulanteDTO tripulanteDTO = toDto(t);
			tripulantesDTO.add(tripulanteDTO);
		}	
		return tripulantesDTO;
	}

}