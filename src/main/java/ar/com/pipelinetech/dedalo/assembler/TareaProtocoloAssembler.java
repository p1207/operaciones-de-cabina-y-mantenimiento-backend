package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.TareaProtocoloDTO;
import ar.com.pipelinetech.dedalo.entities.TareaProtocolo;


@Component
public class TareaProtocoloAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static TareaProtocoloDTO toDto(TareaProtocolo tareaProtocolo) {
		TareaProtocoloDTO tareaProtocoloDTO = mapper.map(tareaProtocolo, TareaProtocoloDTO.class);
		return tareaProtocoloDTO;
	}

	public static TareaProtocolo toEntity(TareaProtocoloDTO tareaProtocoloDTO) {
		TareaProtocolo tareaProtocolo = mapper.map(tareaProtocoloDTO, TareaProtocolo.class);
		return tareaProtocolo;
	}
	
	public static List<TareaProtocolo> listDTOToEntityList(List<TareaProtocoloDTO> tareasProtocoloDTO) {
		List<TareaProtocolo> tareas = new ArrayList<TareaProtocolo>();
		for(TareaProtocoloDTO t : tareasProtocoloDTO) {
			TareaProtocolo  tarea = toEntity(t);
			tareas.add(tarea);
		}	
		return tareas;
	}
	
	public static List<TareaProtocoloDTO> listEntityToDTOList(List<TareaProtocolo> tareasProtocolo) {
		List<TareaProtocoloDTO> tareasProtocoloDTO = new ArrayList<TareaProtocoloDTO>();
		for(TareaProtocolo t : tareasProtocolo) {
			TareaProtocoloDTO tareaProtocoloDTO = toDto(t);
			tareasProtocoloDTO.add(tareaProtocoloDTO);
		}	
		return tareasProtocoloDTO;
	}

}
