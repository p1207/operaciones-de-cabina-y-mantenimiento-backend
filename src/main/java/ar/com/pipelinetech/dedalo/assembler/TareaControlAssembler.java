package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.TareaControlDTO;
import ar.com.pipelinetech.dedalo.entities.TareaControl;


@Component
public class TareaControlAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	
	public static TareaControlDTO toDto(TareaControl tarea) {
		mapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
		.setAmbiguityIgnored(true);
		PropertyMap<TareaControl, TareaControlDTO> map = new PropertyMap<TareaControl, TareaControlDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getControl());
		        skip(destination.getRol());
		        skip(destination.getRole());
		    }
		};
		TypeMap<TareaControl, TareaControlDTO> typeMap = mapper.getTypeMap(TareaControl.class, TareaControlDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		TareaControlDTO tareaDTO = mapper.map(tarea, TareaControlDTO.class);
		tareaDTO.setControl(tarea.getControl() != null ? tarea.getControl().getId(): null);
		tareaDTO.setRole(tarea.getRol() != null? RolAssembler.toDto(tarea.getRol()) : null);
		tareaDTO.setRol(tarea.getRol() != null ? tarea.getRol().getId() : null);
	
		return tareaDTO;
	}
	
	public static TareaControl toEntity(TareaControlDTO tareaDTO) {
		TareaControl tarea = new TareaControl();
		tarea.setDescripcion(tareaDTO.getDescripcion());
		tarea.setRol(RolAssembler.toEntity(tareaDTO.getRole()));
	/*	Rol rol = new Rol();
		rol.setId(tareaDTO.getRol());
		tarea.setRol(rol);*/
		return tarea;
	}
	
	public static List<TareaControl> listDTOToEntityList(List<TareaControlDTO> tareasDTO) {
		List<TareaControl> tareas = new ArrayList<TareaControl>();
		for(TareaControlDTO t : tareasDTO) {
			TareaControl tarea = toEntity(t);
			tareas.add(tarea);
		}	
		return tareas;
	}
	
	public static List<TareaControlDTO> listEntityToDTOList(List<TareaControl> tareas) {
		List<TareaControlDTO> tareasDTO = new ArrayList<TareaControlDTO>();
		for(TareaControl t : tareas) {
			TareaControlDTO tareaDTO = toDto(t);
			tareasDTO.add(tareaDTO);
		}	
		return tareasDTO;
	}
}
