//package ar.com.pipelinetech.dedalo.assembler;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.modelmapper.ModelMapper;
//import org.modelmapper.PropertyMap;
//import org.modelmapper.TypeMap;
//import org.springframework.stereotype.Component;
//
//import ar.com.pipelinetech.dedalo.dto.AeronaveDTO;
//import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
//import ar.com.pipelinetech.dedalo.dto.ModeloAeronaveDTO;
//import ar.com.pipelinetech.dedalo.entities.Aeronave;
//import ar.com.pipelinetech.dedalo.entities.Mantenimiento;
//import ar.com.pipelinetech.dedalo.entities.ModeloAeronave;
//
//@Component
//public class AeronaveAssembler {
//
//	private static ModelMapper mapper = new ModelMapper();
//	
//	public static AeronaveDTO toDto(Aeronave aeronave) {
//		List<MantenimientoDTO> mantenimientosDTO = MantenimientoAssembler.listEntityToDTOList(aeronave.getMantenimientos());
//		ModeloAeronaveDTO modelo = ModeloAeronaveAssembler.toDto(aeronave.getModelo());
//		PropertyMap<AeronaveDTO, Aeronave> aeronaveMap = new PropertyMap<AeronaveDTO, Aeronave>() {
//		    @Override
//		    protected void configure() {
//		        skip(destination.getTipo());
//		    }
//		};
//		TypeMap<AeronaveDTO, Aeronave> typeMapAeronave = mapper.getTypeMap(AeronaveDTO.class, Aeronave.class);
//		if (typeMapAeronave == null) { // if not  already added
//			mapper.addMappings(aeronaveMap);
//		}
//		AeronaveDTO aeronaveDTO = mapper.map(aeronave, AeronaveDTO.class);
//		aeronaveDTO.setTipo(aeronave.getTipo());
//		aeronaveDTO.setMantenimientos(mantenimientosDTO);
//		aeronaveDTO.setModelo(modelo);
//		return aeronaveDTO;
//	}
//	
//	public static Aeronave toEntity(AeronaveDTO aeronaveDTO) {
//		List<Mantenimiento> mantenimientosDTO = MantenimientoAssembler.listDTOToEntityList(aeronaveDTO.getMantenimientos());
//		ModeloAeronave modelo = ModeloAeronaveAssembler.toEntity(aeronaveDTO.getModelo());
//		PropertyMap<AeronaveDTO, Aeronave> aeronaveMap = new PropertyMap<AeronaveDTO, Aeronave>() {
//		    @Override
//		    protected void configure() {
//		        skip(destination.getTipo());
//		    }
//		};
//		TypeMap<AeronaveDTO, Aeronave> typeMapAeronave = mapper.getTypeMap(AeronaveDTO.class, Aeronave.class);
//		if (typeMapAeronave == null) {
//			mapper.addMappings(aeronaveMap);
//		}
//		Aeronave aeronave = mapper.map(aeronaveDTO, Aeronave.class);
//		
//		aeronave.setMantenimientos(mantenimientosDTO);
//		aeronave.setModelo(modelo);
//		aeronave.setTipo(aeronaveDTO.getTipo());
//		return aeronave;
//	}
//	
//	public static List<Aeronave> listDTOToEntityList(List<AeronaveDTO> aeronavesDTO) {
//		List<Aeronave> aeronaves = new ArrayList<Aeronave>();
//		for(AeronaveDTO a : aeronavesDTO) {
//			Aeronave aeronave = toEntity(a);
//			aeronaves.add(aeronave);
//		}	
//		return aeronaves;
//	}
//	
//	public static List<AeronaveDTO> listEntityToDTOList(List<Aeronave> aeronaves) {
//		List<AeronaveDTO> aeronavesDTO = new ArrayList<AeronaveDTO>();
//		for(Aeronave a : aeronaves) {
//			AeronaveDTO aeronaveDTO = toDto(a);
//			aeronavesDTO.add(aeronaveDTO);
//		}	
//		return aeronavesDTO;
//	}
//}
