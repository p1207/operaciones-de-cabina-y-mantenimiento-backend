package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import ar.com.pipelinetech.dedalo.dto.PasajeroDTO;
import ar.com.pipelinetech.dedalo.entities.Pasajero;

public class PasajeroAssembler {

	private static ModelMapper mapper = new ModelMapper();

	public static PasajeroDTO toDto(Pasajero Pasajero) {
		PasajeroDTO PasajeroDTO = mapper.map(Pasajero, PasajeroDTO.class);
		return PasajeroDTO;
	}

	public static Pasajero toEntity(PasajeroDTO PasajeroDTO) {
		Pasajero Pasajero = mapper.map(PasajeroDTO, Pasajero.class);
		return Pasajero;
	}

	public static List<Pasajero> listDTOToEntityList(List<PasajeroDTO> PasajeroesDTO) {
		List<Pasajero> Pasajeroes = new ArrayList<Pasajero>();
		for (PasajeroDTO t : PasajeroesDTO) {
			Pasajero Pasajero = toEntity(t);
			Pasajeroes.add(Pasajero);
		}
		return Pasajeroes;
	}

	public static List<PasajeroDTO> listEntityToDTOList(List<Pasajero> Pasajeroes) {
		List<PasajeroDTO> PasajeroesDTO = new ArrayList<PasajeroDTO>();
		for (Pasajero t : Pasajeroes) {
			PasajeroDTO PasajeroDTO = toDto(t);
			PasajeroesDTO.add(PasajeroDTO);
		}
		return PasajeroesDTO;
	}

}
