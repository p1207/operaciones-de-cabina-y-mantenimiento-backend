package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.EmergenciaDTO;
import ar.com.pipelinetech.dedalo.entities.Emergencia;


@Component
public class EmergenciaAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static EmergenciaDTO toDto(Emergencia emergencia) {
		EmergenciaDTO emergenciaDTO = mapper.map(emergencia, EmergenciaDTO.class);
		return emergenciaDTO;
	}

	public static Emergencia toEntity(EmergenciaDTO emergenciaDTO) {
		Emergencia emergencia = mapper.map(emergenciaDTO, Emergencia.class);
		emergencia.setTripulante(emergenciaDTO.getTripulante());
		return emergencia;
	}
	
	public static List<Emergencia> listDTOToEntityList(List<EmergenciaDTO> emergenciasDTO) {
		List<Emergencia> emergencias = new ArrayList<Emergencia>();
		for(EmergenciaDTO e : emergenciasDTO) {
			Emergencia emergencia = toEntity(e);
			emergencias.add(emergencia);
		}	
		return emergencias;
	}
	
	public static List<EmergenciaDTO> listEntityToDTOList(List<Emergencia> emergencias) {
		List<EmergenciaDTO> emergenciasDTO = new ArrayList<EmergenciaDTO>();
		for(Emergencia e : emergencias) {
			EmergenciaDTO emergenciaDTO = toDto(e);
			emergenciasDTO.add(emergenciaDTO);
		}	
		return emergenciasDTO;
	}

}
