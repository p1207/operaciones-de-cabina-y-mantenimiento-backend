package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.entities.Tarea;

@Component
public class TareaAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static TareaDTO toDto(Tarea tarea) {
		mapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
		.setAmbiguityIgnored(true);
		PropertyMap<Tarea, TareaDTO> map = new PropertyMap<Tarea, TareaDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getMantenimiento());
		        skip(destination.getSolicitud());
		    }
		};
		TypeMap<Tarea, TareaDTO> typeMap = mapper.getTypeMap(Tarea.class, TareaDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		TareaDTO tareaDTO = mapper.map(tarea, TareaDTO.class);
		tareaDTO.setMantenimiento(tarea.getMantenimiento() != null ? tarea.getMantenimiento().getId(): null);
		tareaDTO.setSolicitud(tarea.getSolicitud() != null ? tarea.getSolicitud().getId() : null);
		return tareaDTO;
	}
	
	public static Tarea toEntity(TareaDTO tareaDTO) {
		Tarea tarea = mapper.map(tareaDTO, Tarea.class);
		return tarea;
	}
	
	public static List<Tarea> listDTOToEntityList(List<TareaDTO> tareasDTO) {
		List<Tarea> tareas = new ArrayList<Tarea>();
		for(TareaDTO t : tareasDTO) {
			Tarea tarea = toEntity(t);
			tareas.add(tarea);
		}	
		return tareas;
	}
	
	public static List<TareaDTO> listEntityToDTOList(List<Tarea> tareas) {
		List<TareaDTO> tareasDTO = new ArrayList<TareaDTO>();
		for(Tarea t : tareas) {
			TareaDTO tareaDTO = toDto(t);
			tareasDTO.add(tareaDTO);
		}	
		return tareasDTO;
	}

	
}
