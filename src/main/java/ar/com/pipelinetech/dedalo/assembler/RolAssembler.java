package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.entities.Rol;


@Component
public class RolAssembler {

	
	private static ModelMapper mapper = new ModelMapper();
	
	public static RolDTO toDto(Rol rol) {
		RolDTO rolDTO = mapper.map(rol, RolDTO.class);
		return rolDTO;
	}

	public static Rol toEntity(RolDTO rolDTO) {
		Rol rol = mapper.map(rolDTO, Rol.class);
		return rol;
	}
	
	public static List<Rol> listDTOToEntityList(List<RolDTO> rolesDTO) {
		List<Rol> roles = new ArrayList<Rol>();
		for(RolDTO r : rolesDTO) {
			Rol rol = toEntity(r);
			roles.add(rol);
		}	
		return roles;
	}
	
	public static List<RolDTO> listEntityToDTOList(List<Rol> roles) {
		List<RolDTO> rolesDTO = new ArrayList<RolDTO>();
		for(Rol r : roles) {
			RolDTO rolDTO = toDto(r);
			rolesDTO.add(rolDTO);
		}	
		return rolesDTO;
	}
}
