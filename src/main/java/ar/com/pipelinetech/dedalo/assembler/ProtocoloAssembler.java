package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.ProtocoloDTO;
import ar.com.pipelinetech.dedalo.entities.Protocolo;

@Component
public class ProtocoloAssembler {
	

	private static ModelMapper mapper = new ModelMapper();
	
	
	public static ProtocoloDTO toDto(Protocolo protocolo) {
		ProtocoloDTO protocoloDTO = mapper.map(protocolo, ProtocoloDTO.class);
		return protocoloDTO;
	}

	public static Protocolo toEntity(ProtocoloDTO ProtocoloDTO) {
		
		Protocolo Protocolo = mapper.map(ProtocoloDTO, Protocolo.class);
		return Protocolo;
	}
	
	public static List<Protocolo> listDTOToEntityList(List<ProtocoloDTO> ProtocoloesDTO) {
		List<Protocolo> Protocoloes = new ArrayList<Protocolo>();
		for(ProtocoloDTO t : ProtocoloesDTO) {
			Protocolo Protocolo = toEntity(t);
			Protocoloes.add(Protocolo);
		}	
		return Protocoloes;
	}
	
	public static List<ProtocoloDTO> listEntityToDTOList(List<Protocolo> protocolos) {
		List<ProtocoloDTO> protocolosDTO = new ArrayList<ProtocoloDTO>();
		for(Protocolo t : protocolos) {
			ProtocoloDTO protocoloDTO = toDto(t);
			protocolosDTO.add(protocoloDTO);
		}	
		return protocolosDTO;
	}


}
