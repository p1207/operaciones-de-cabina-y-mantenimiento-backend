package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.NotificacionDTO;
import ar.com.pipelinetech.dedalo.entities.Notificacion;


@Component
public class NotificacionAssembler {
	
private static ModelMapper mapper = new ModelMapper();
	
	public static NotificacionDTO toDto(Notificacion notificacion) {
		NotificacionDTO notificacionDTO = mapper.map(notificacion, NotificacionDTO.class);
		return notificacionDTO;
	}

	public static Notificacion toEntity(NotificacionDTO notificacionDTO) {
		Notificacion notificacion = mapper.map(notificacionDTO, Notificacion.class);
		return notificacion;
	}
	
	public static List<Notificacion> listDTOToEntityList(List<NotificacionDTO> notificacionesDTO) {
		List<Notificacion> notificaciones = new ArrayList<Notificacion>();
		for(NotificacionDTO t : notificacionesDTO) {
			Notificacion notificacion = toEntity(t);
			notificaciones.add(notificacion);
		}	
		return notificaciones;
	}
	
	public static List<NotificacionDTO> listEntityToDTOList(List<Notificacion> notificaciones) {
		List<NotificacionDTO> notificacionesDTO = new ArrayList<NotificacionDTO>();
		for(Notificacion t : notificaciones) {
			NotificacionDTO notificacionDTO = toDto(t);
			notificacionesDTO.add(notificacionDTO);
		}	
		return notificacionesDTO;
	}
}
