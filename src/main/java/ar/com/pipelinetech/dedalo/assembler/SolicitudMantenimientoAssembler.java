package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

//import ar.com.pipelinetech.dedalo.dto.AeronaveDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.SolicitudMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
//import ar.com.pipelinetech.dedalo.entities.Aeronave;
import ar.com.pipelinetech.dedalo.entities.Mantenimiento;
import ar.com.pipelinetech.dedalo.entities.SolicitudMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Tarea;
import ar.com.pipelinetech.dedalo.entities.Tecnico;
import ar.com.pipelinetech.dedalo.enums.EstadoSolicitudMantenimiento;

@Component
public class SolicitudMantenimientoAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static SolicitudMantenimientoDTO toDto(SolicitudMantenimiento solicitud) {
		List<TareaDTO> tareasDTO = TareaAssembler.listEntityToDTOList(solicitud.getTareas());
		//AeronaveDTO aeronave = AeronaveAssembler.toDto(solicitud.getMatricula());
		MantenimientoDTO mantenimiento = MantenimientoAssembler.toDto(solicitud.getMantenimiento());
		EstadoSolicitudMantenimiento estado = solicitud.getEstado();
		TecnicoDTO tecnico = TecnicoAssembler.toDto(solicitud.getTecnico());
		PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> map = new PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getTareas());
		        //skip(destination.getAeronave());
		        skip(destination.getMantenimiento());
		        skip(destination.getTecnico());
		        skip(destination.getEstadoId());
		    }
		};
		TypeMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> typeSolicitudMap = mapper.getTypeMap(SolicitudMantenimiento.class, SolicitudMantenimientoDTO.class);
		if (typeSolicitudMap == null) {
			mapper.addMappings(map);
		}
		SolicitudMantenimientoDTO solicitudDTO = mapper.map(solicitud, SolicitudMantenimientoDTO.class);
		//solicitudDTO.setAeronave(aeronave);
		solicitudDTO.setTecnico(tecnico);
		solicitudDTO.setMantenimiento(mantenimiento);
		solicitudDTO.setTareas(tareasDTO);
		solicitudDTO.setEstado(estado);
		solicitudDTO.setEstadoId(solicitud.getEstado()!= null ? solicitud.getEstado().ordinal(): null);
		return solicitudDTO;
	}
	
	public static SolicitudMantenimiento toEntity(SolicitudMantenimientoDTO solicitudDTO) {
		List<Tarea> tareas = TareaAssembler.listDTOToEntityList(solicitudDTO.getTareas());
		//Aeronave aeronave = AeronaveAssembler.toEntity(solicitudDTO.getAeronave());
		Mantenimiento mantenimiento = MantenimientoAssembler.toEntity(solicitudDTO.getMantenimiento());
		Tecnico tecnico = TecnicoAssembler.toEntity(solicitudDTO.getTecnico());
		PropertyMap<SolicitudMantenimientoDTO, SolicitudMantenimiento> map = new PropertyMap<SolicitudMantenimientoDTO, SolicitudMantenimiento>() {
		    @Override
		    protected void configure() {
		        skip(destination.getTareas());
		        //skip(destination.getAeronave());
		        skip(destination.getMantenimiento());
		        skip(destination.getTecnico());
		        
		    }
		};
		TypeMap<SolicitudMantenimientoDTO, SolicitudMantenimiento> typeSolicitudMap = mapper.getTypeMap(SolicitudMantenimientoDTO.class, SolicitudMantenimiento.class);
		if (typeSolicitudMap == null) {
			mapper.addMappings(map);
		}
		SolicitudMantenimiento solicitud = mapper.map(solicitudDTO, SolicitudMantenimiento.class);
		solicitud.setTareas(tareas);
		//solicitud.setAeronave(aeronave);
		solicitud.setMantenimiento(mantenimiento);
		solicitud.setTecnico(tecnico);
		return solicitud;
	}
	
	public static List<SolicitudMantenimiento> listDTOToEntityList(List<SolicitudMantenimientoDTO> solicitudesDTO) {
		List<SolicitudMantenimiento> solicitudes = new ArrayList<SolicitudMantenimiento>();
		for(SolicitudMantenimientoDTO s : solicitudesDTO) {
			SolicitudMantenimiento solicitud = toEntity(s);
			solicitudes.add(solicitud);
		}	
		return solicitudes;
	}
	
	public static List<SolicitudMantenimientoDTO> listEntityToDTOList(List<SolicitudMantenimiento> solicitudes) {
		List<SolicitudMantenimientoDTO> solicitudesDTO = new ArrayList<SolicitudMantenimientoDTO>();
		for(SolicitudMantenimiento s : solicitudes) {
			SolicitudMantenimientoDTO solicitudDTO = toDto(s);
			solicitudesDTO.add(solicitudDTO);
		}	
		return solicitudesDTO;
	}
}
