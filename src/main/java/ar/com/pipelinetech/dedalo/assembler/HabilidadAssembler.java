package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.entities.Habilidad;

@Component
public class HabilidadAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static HabilidadDTO toDto(Habilidad habilidad) {
		HabilidadDTO habilidadDTO = mapper.map(habilidad, HabilidadDTO.class);
		return habilidadDTO;
	}

	public static Habilidad toEntity(HabilidadDTO habilidadDTO) {
		Habilidad habilidad = mapper.map(habilidadDTO, Habilidad.class);
		return habilidad;
	}
	
	public static List<Habilidad> listDTOToEntityList(List<HabilidadDTO> habilidadesDTO) {
		List<Habilidad> habilidades = new ArrayList<Habilidad>();
		for(HabilidadDTO t : habilidadesDTO) {
			Habilidad habilidad = toEntity(t);
			habilidades.add(habilidad);
		}	
		return habilidades;
	}
	
	public static List<HabilidadDTO> listEntityToDTOList(List<Habilidad> habilidades) {
		List<HabilidadDTO> habilidadesDTO = new ArrayList<HabilidadDTO>();
		for(Habilidad t : habilidades) {
			HabilidadDTO habilidadDTO = toDto(t);
			habilidadesDTO.add(habilidadDTO);
		}	
		return habilidadesDTO;
	}

}
