package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.ModeloAeronaveDTO;
import ar.com.pipelinetech.dedalo.entities.ModeloAeronave;

@Component
public class ModeloAeronaveAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static ModeloAeronaveDTO toDto(ModeloAeronave modelo) {
		ModeloAeronaveDTO modeloDTO = mapper.map(modelo, ModeloAeronaveDTO.class);

		return modeloDTO;
	}
	
	public static ModeloAeronave toEntity(ModeloAeronaveDTO modeloDTO) {
		ModeloAeronave modelo = mapper.map(modeloDTO, ModeloAeronave.class);
		
		return modelo;
	}
	
	public static List<ModeloAeronave> listDTOToEntityList(List<ModeloAeronaveDTO> modelosDTO) {
		List<ModeloAeronave> modelos = new ArrayList<ModeloAeronave>();
		for(ModeloAeronaveDTO m : modelosDTO) {
			ModeloAeronave modelo = toEntity(m);
			modelos.add(modelo);
		}	
		return modelos;
	}
	
	public static List<ModeloAeronaveDTO> listEntityToDTOList(List<ModeloAeronave> modelos) {
		List<ModeloAeronaveDTO> modelosDTO = new ArrayList<ModeloAeronaveDTO>();
		for(ModeloAeronave m : modelos) {
			ModeloAeronaveDTO modeloDTO = toDto(m);
			modelosDTO.add(modeloDTO);
		}	
		return modelosDTO;
	}
}