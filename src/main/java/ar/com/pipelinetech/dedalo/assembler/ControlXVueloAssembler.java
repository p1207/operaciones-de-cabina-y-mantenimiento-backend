package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.ControlXVueloDTO;
import ar.com.pipelinetech.dedalo.entities.ControlXVuelo;
import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;

@Component
public class ControlXVueloAssembler {

	private static ModelMapper mapper = new ModelMapper();

	public static ControlXVueloDTO toDTO(ControlXVuelo control) {
		EstadoControlXVuelo estado =control.getEstado();
		PropertyMap<ControlXVueloDTO, ControlXVuelo> Map = new PropertyMap<ControlXVueloDTO, ControlXVuelo>() {
		    @Override
		    protected void configure() {
		        skip(destination.getEstado());
		    }
		};
		TypeMap<ControlXVueloDTO, ControlXVuelo> typeMap = mapper.getTypeMap(ControlXVueloDTO.class, ControlXVuelo.class);
		if (typeMap == null) {
			mapper.addMappings(Map);
		}
		ControlXVueloDTO controlDTO = mapper.map(control, ControlXVueloDTO.class);
		controlDTO.setEstado(estado);
		return controlDTO;
	}
	
	public static ControlXVuelo toEntity(ControlXVueloDTO controlDTO) {
		PropertyMap<ControlXVueloDTO, ControlXVuelo> Map = new PropertyMap<ControlXVueloDTO, ControlXVuelo>() {
		    @Override
		    protected void configure() {
		        skip(destination.getEstado());
		    }
		};
		TypeMap<ControlXVueloDTO, ControlXVuelo> typeMap = mapper.getTypeMap(ControlXVueloDTO.class, ControlXVuelo.class);
		if (typeMap == null) {
			mapper.addMappings(Map);
		}
		ControlXVuelo modelo = mapper.map(controlDTO, ControlXVuelo.class);
		modelo.setEstado(controlDTO.getEstado());
		return modelo;
	}
	
	public static List<ControlXVuelo> listDTOToEntityList(List<ControlXVueloDTO> controlesDTO) {
		List<ControlXVuelo> control = new ArrayList<ControlXVuelo>();
		for(ControlXVueloDTO c : controlesDTO) {
			ControlXVuelo controlXVuelo = toEntity(c);
			control.add(controlXVuelo);
		}	
		return control;
	}
	
	public static List<ControlXVueloDTO> listEntityToDTOList(List<ControlXVuelo> controles) {
		List<ControlXVueloDTO> controlesDTO = new ArrayList<ControlXVueloDTO>();
		for(ControlXVuelo m : controles) {
			ControlXVueloDTO controlDTO = toDTO(m);
			controlesDTO.add(controlDTO);
		}	
		return controlesDTO;
	}
}
