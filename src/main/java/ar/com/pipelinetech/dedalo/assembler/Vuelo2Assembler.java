package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import ar.com.pipelinetech.dedalo.dto.Vuelo2DTO;
import ar.com.pipelinetech.dedalo.entities.Vuelo2;

public class Vuelo2Assembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static Vuelo2DTO toDto(Vuelo2 vuelo2) {
			Vuelo2DTO vuelo2DTO = mapper.map(vuelo2, Vuelo2DTO.class);
			return vuelo2DTO;
	}

	public static Vuelo2 toEntity(Vuelo2DTO vuelo2DTO) {
		Vuelo2 vuelo2 = mapper.map(vuelo2DTO, Vuelo2.class);
		return vuelo2;
	}
	
	
	public static List<Vuelo2> listDTOToEntityList(List<Vuelo2DTO> vuelos2DTO) {
		List<Vuelo2> vuelos = new ArrayList<Vuelo2>();
		for(Vuelo2DTO m : vuelos2DTO) {
			Vuelo2 vuelo = toEntity(m);
			vuelos.add(vuelo);
		}	
		return vuelos;
	}
	
	public static List<Vuelo2DTO> listEntityToDTOList(List<Vuelo2> vuelos) {
		List<Vuelo2DTO> vuelos2DTO = new ArrayList<Vuelo2DTO>();
		for(Vuelo2 v : vuelos) {
			Vuelo2DTO vuelo2DTO = toDto(v);
			vuelos2DTO.add(vuelo2DTO);
		}	
		return vuelos2DTO;
	}

}
