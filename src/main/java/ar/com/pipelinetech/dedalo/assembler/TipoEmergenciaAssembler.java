package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ar.com.pipelinetech.dedalo.dto.TipoEmergenciaDTO;
import ar.com.pipelinetech.dedalo.entities.TipoEmergencia;


@Component
public class TipoEmergenciaAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static TipoEmergenciaDTO toDto(TipoEmergencia tipoEmergencia) {
		TipoEmergenciaDTO tipoEmergenciaDTO = mapper.map(tipoEmergencia, TipoEmergenciaDTO.class);
		tipoEmergenciaDTO.setProtocolo(ProtocoloAssembler.toDto(tipoEmergencia.getIdProtocolo()));
		return tipoEmergenciaDTO;
	}

	public static TipoEmergencia toEntity(TipoEmergenciaDTO tipoEmergenciaDTO) {
		TipoEmergencia tipoEmergencia = mapper.map(tipoEmergenciaDTO, TipoEmergencia.class);
		tipoEmergencia.setIdProtocolo(ProtocoloAssembler.toEntity(tipoEmergenciaDTO.getProtocolo()));
		return tipoEmergencia;
	}
	
	public static List<TipoEmergencia> listDTOToEntityList(List<TipoEmergenciaDTO> tipoEmergenciasDTO) {
		List<TipoEmergencia> tipoEmergencias = new ArrayList<TipoEmergencia>();
		for(TipoEmergenciaDTO e : tipoEmergenciasDTO) {
			TipoEmergencia tipoEmergencia = toEntity(e);
			tipoEmergencias.add(tipoEmergencia);
		}	
		return tipoEmergencias;
	}
	
	public static List<TipoEmergenciaDTO> listEntityToDTOList(List<TipoEmergencia> tipoEmergencias) {
		List<TipoEmergenciaDTO> tipoEmergenciasDTO = new ArrayList<TipoEmergenciaDTO>();
		for(TipoEmergencia e : tipoEmergencias) {
			TipoEmergenciaDTO tipoEmergenciaDTO = toDto(e);
			tipoEmergenciasDTO.add(tipoEmergenciaDTO);
		}	
		return tipoEmergenciasDTO;
	}

}
