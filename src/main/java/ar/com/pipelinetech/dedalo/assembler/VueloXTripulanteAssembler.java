package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.VueloXTripulanteDTO;
import ar.com.pipelinetech.dedalo.entities.VueloXTripulante;

@Component
public class VueloXTripulanteAssembler {

private static ModelMapper mapper = new ModelMapper();
	
	public static VueloXTripulanteDTO toDto(VueloXTripulante vueloxtripulante) {
		VueloXTripulanteDTO vueloxtripulanteDTO = mapper.map(vueloxtripulante, VueloXTripulanteDTO.class);
		return vueloxtripulanteDTO;
	}

	public static VueloXTripulante toEntity(VueloXTripulanteDTO vueloxtripulanteDTO) {
		VueloXTripulante vueloxtripulante = mapper.map(vueloxtripulanteDTO, VueloXTripulante.class);
		return vueloxtripulante;
	}
	
	public static List<VueloXTripulante> listDTOToEntityList(List<VueloXTripulanteDTO> vuelosXTripulantesDTO) {
		List<VueloXTripulante> vuelosxtripulantes = new ArrayList<VueloXTripulante>();
		for(VueloXTripulanteDTO vxt : vuelosXTripulantesDTO) {
			VueloXTripulante vueloxtripulante = toEntity(vxt);
			vuelosxtripulantes.add(vueloxtripulante);
		}	
		return vuelosxtripulantes;
	}
	
	@SuppressWarnings("null")
	public static VueloXTripulanteDTO [] listEntityToDTOList(VueloXTripulante [] vuelosxtripulantes) {
		VueloXTripulanteDTO [] vueloxtripulantesDTO = new VueloXTripulanteDTO [vuelosxtripulantes.length];
		for(int i = 0; i <vuelosxtripulantes.length; i++) {
			VueloXTripulanteDTO vueloxtripulanteDTO = toDto(vuelosxtripulantes[i]);
			vueloxtripulantesDTO[i] = vueloxtripulanteDTO;
		}	
		return vueloxtripulantesDTO;
	}
}
