package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.ControlDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.TareaControlDTO;
import ar.com.pipelinetech.dedalo.entities.Control;
import ar.com.pipelinetech.dedalo.entities.InsumoMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Rol;
import ar.com.pipelinetech.dedalo.entities.TareaControl;
import ar.com.pipelinetech.dedalo.enums.TipoControl;

@Component
public class ControlAssembler {

	
	private static ModelMapper mapper = new ModelMapper();
	
	
	
	public static List<Control> listDTOToEntityList(List<ControlDTO> controlesDTO) {
		List<Control> controles = new ArrayList<Control>();
		for (ControlDTO c : controlesDTO) {
			Control control = toEntity(c);
			controles.add(control);
		}
		return controles;
	}

	public static Control toEntity(ControlDTO c) {
		Rol rol = RolAssembler.toEntity(c.getRol());
		List<TareaControl> tareas = TareaControlAssembler.listDTOToEntityList(c.getTareas());
		List<InsumoMantenimiento> insumos = InsumoMantenimientoAssembler.listDTOToEntityList(c.getInsumos());
		PropertyMap<ControlDTO, Control> controlMap = new PropertyMap<ControlDTO, Control>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		        skip(destination.getTareas());
		        skip(destination.getInsumos());
		    }
		};
		TypeMap<ControlDTO, Control> typeMap = mapper.getTypeMap(ControlDTO.class, Control.class);

	
		if (typeMap == null) {
			mapper.addMappings(controlMap);
		}
		Control control = mapper.map(c, Control.class);
		control.setInsumos(insumos);
		control.setTareas(tareas);
		control.setTipoControl(c.getTipoControl());
		control.setRol(rol);
		
		return control;
	}
	

	public static ControlDTO toDto(Control control) {
		TipoControl tipo= control.getTipoControl();
		RolDTO rolDTO = RolAssembler.toDto(control.getRol());
		List<TareaControlDTO> tareasDTO = TareaControlAssembler.listEntityToDTOList(control.getTareas());
		List<InsumoMantenimientoDTO> insumosDTO = InsumoMantenimientoAssembler.listEntityToDTOList(control.getInsumos());
		PropertyMap<Control, ControlDTO> controlMap = new PropertyMap<Control, ControlDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getTipoControl());
		        skip(destination.getRol());
		        skip(destination.getIdRol());
		        skip(destination.getTareas());
		        skip(destination.getInsumos());
		        
		    }
		};
		TypeMap<Control, ControlDTO> typeMap = mapper.getTypeMap(Control.class, ControlDTO.class);
		if (typeMap == null) {
			mapper.addMappings(controlMap);
		}
		ControlDTO controlDTO = mapper.map(control, ControlDTO.class);
		controlDTO.setInsumos(insumosDTO);
		controlDTO.setTareas(tareasDTO);
		controlDTO.setTipoControl(tipo);
		controlDTO.setRol(rolDTO);
		return controlDTO;
		
	}

	public static List<ControlDTO> listEntityToDTOList(List<Control> controles) {
		List<ControlDTO> controlesDTO = new ArrayList<ControlDTO>();
		for(Control c : controles) {
			ControlDTO controlDTO = toDto(c);
			controlesDTO.add(controlDTO);
		}	
		return controlesDTO;
	}
}
