package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.InsumoCateringDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoCatering;

@Component
public class InsumoCateringAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static InsumoCateringDTO toDto(InsumoCatering insumoCatering) {
		InsumoCateringDTO insumoCateringDTO = mapper.map(insumoCatering, InsumoCateringDTO.class);
		return insumoCateringDTO;
	}

	public static InsumoCatering toEntity(InsumoCateringDTO insumoCateringDTO) {
		InsumoCatering insumoCatering = mapper.map(insumoCateringDTO, InsumoCatering.class);
		return insumoCatering;
	}

	public static List<InsumoCatering> listDTOToEntityList(List<InsumoCateringDTO> insumosCateringDTO) {
		List<InsumoCatering> insumosCatering = new ArrayList<InsumoCatering>();
		for(InsumoCateringDTO i : insumosCateringDTO) {
			InsumoCatering insumoCatering = toEntity(i);
			insumosCatering.add(insumoCatering);
		}	
		return insumosCatering;
	}

	public static List<InsumoCateringDTO> listEntityToDTOList(List<InsumoCatering> insumosCatering) {
		List<InsumoCateringDTO> insumosCateringDTO = new ArrayList<InsumoCateringDTO>();
		for(InsumoCatering i : insumosCatering) {
			InsumoCateringDTO insumoCateringDTO = toDto(i);
			insumosCateringDTO.add(insumoCateringDTO);
		}	
		return insumosCateringDTO;
	}
}
