package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;

import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.UsuarioDTO;
import ar.com.pipelinetech.dedalo.entities.Habilidad;
import ar.com.pipelinetech.dedalo.entities.Rol;
import ar.com.pipelinetech.dedalo.entities.Usuario;

public class UsuarioAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	
	public static UsuarioDTO toDto(Usuario usuario) {
		List<HabilidadDTO> habilidadesDTO = HabilidadAssembler.listEntityToDTOList(usuario.getHabilidades());
		RolDTO rol= RolAssembler.toDto(usuario.getIdRol());
		PropertyMap<Usuario, UsuarioDTO> map = new PropertyMap<Usuario, UsuarioDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		        skip(destination.getHabilidades());
		    }
		};
		TypeMap<Usuario, UsuarioDTO> typeMap = mapper.getTypeMap(Usuario.class, UsuarioDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		UsuarioDTO usuarioDTO = mapper.map(usuario, UsuarioDTO.class);
		usuarioDTO.setRol(rol);
		usuarioDTO.setHabilidades(habilidadesDTO);
		return usuarioDTO;
	}

	public static Usuario toEntity(UsuarioDTO usuarioDTO) {
		List<Habilidad> habilidades = HabilidadAssembler.listDTOToEntityList(usuarioDTO.getHabilidades());
		Rol rol= RolAssembler.toEntity(usuarioDTO.getRol());
		PropertyMap<Usuario, UsuarioDTO> map = new PropertyMap<Usuario, UsuarioDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		        skip(destination.getHabilidades());
		    }
		};
		TypeMap<Usuario, UsuarioDTO> typeMap = mapper.getTypeMap(Usuario.class, UsuarioDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		Usuario usuario = mapper.map(usuarioDTO, Usuario.class);
		usuario.setIdRol(rol);
		usuario.setHabilidades(habilidades);
		return usuario;
	}

	public static List<Usuario> listDTOToEntityList(List<UsuarioDTO> usuariosDTO) {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		for(UsuarioDTO u : usuariosDTO) {
			Usuario usuario = toEntity(u);
			usuarios.add(usuario);
		}	
		return usuarios;
	}

	public static List<UsuarioDTO> listEntityToDTOList(List<Usuario> usuarios) {
		List<UsuarioDTO> usuariosDTO = new ArrayList<UsuarioDTO>();
		for(Usuario u : usuarios) {
			UsuarioDTO usuarioDTO = toDto(u);
			usuariosDTO.add(usuarioDTO);
		}	
		return usuariosDTO;
	}
}
