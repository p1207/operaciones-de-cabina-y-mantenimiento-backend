package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.entities.Aeronave2;


public class Aeronave2Assembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static Aeronave2DTO toDto(Aeronave2 aeronave2) {
		Aeronave2DTO aeronaveDTO = mapper.map(aeronave2, Aeronave2DTO.class);
		return aeronaveDTO;
	}

	public static Aeronave2 toEntity(Aeronave2DTO aeronaveDTO) {
		Aeronave2 aeronave = mapper.map(aeronaveDTO, Aeronave2.class); 
		return aeronave;
	}
	
	
	public static List<Aeronave2> listDTOToEntityList(List<Aeronave2DTO> aeronavesDTO) {
		List<Aeronave2> aeronaves = new ArrayList<Aeronave2>();
		for(Aeronave2DTO a : aeronavesDTO) {
			Aeronave2 aeronave = toEntity(a);
			aeronaves.add(aeronave);
		}	
		return aeronaves;
	}
	
	public static List<Aeronave2DTO> listEntityToDTOList(List<Aeronave2> aeronaves) {
		List<Aeronave2DTO> aeronavesDTO = new ArrayList<Aeronave2DTO>();
		for(Aeronave2 a : aeronaves) {
			Aeronave2DTO aeronaveDTO = toDto(a);
			aeronavesDTO.add(aeronaveDTO);
		}	
		return aeronavesDTO;
	}


}
