package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.AeronaveXMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.SolicitudMantenimientoDTO;
import ar.com.pipelinetech.dedalo.entities.AeronaveXMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Mantenimiento;
import ar.com.pipelinetech.dedalo.entities.SolicitudMantenimiento;

@Component
public class AeronaveXMantenimientoAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static AeronaveXMantenimientoDTO toDto(AeronaveXMantenimiento aeronavexmantenimiento) {
		MantenimientoDTO mantenimiento = MantenimientoAssembler.toDto(aeronavexmantenimiento.getMantenimiento());
		PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> map = new PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getMantenimiento());
		    }
		};
		TypeMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> typeSolicitudMap = mapper.getTypeMap(SolicitudMantenimiento.class, SolicitudMantenimientoDTO.class);
		if (typeSolicitudMap == null) {
			mapper.addMappings(map);
		}
		AeronaveXMantenimientoDTO aeronavexmantenimientoDTO = mapper.map(aeronavexmantenimiento, AeronaveXMantenimientoDTO.class);
		aeronavexmantenimientoDTO.setMantenimiento(mantenimiento);
		return aeronavexmantenimientoDTO;
	}

	public static AeronaveXMantenimiento toEntity(AeronaveXMantenimientoDTO aeronavexmantenimientoDTO) {
		Mantenimiento mantenimiento = MantenimientoAssembler.toEntity(aeronavexmantenimientoDTO.getMantenimiento());
		PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> map = new PropertyMap<SolicitudMantenimiento, SolicitudMantenimientoDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getMantenimiento());
		    }
		};
		TypeMap<SolicitudMantenimiento, SolicitudMantenimientoDTO> typeSolicitudMap = mapper.getTypeMap(SolicitudMantenimiento.class, SolicitudMantenimientoDTO.class);
		if (typeSolicitudMap == null) {
			mapper.addMappings(map);
		}
		AeronaveXMantenimiento aeronave = mapper.map(aeronavexmantenimientoDTO, AeronaveXMantenimiento.class);
		aeronave.setMantenimiento(mantenimiento);
		return aeronave;
	}
	
	public static List<AeronaveXMantenimiento> listDTOToEntityList(List<AeronaveXMantenimientoDTO> rolesDTO) {
		List<AeronaveXMantenimiento> roles = new ArrayList<AeronaveXMantenimiento>();
		for(AeronaveXMantenimientoDTO r : rolesDTO) {
			AeronaveXMantenimiento rol = toEntity(r);
			roles.add(rol);
		}	
		return roles;
	}
	
	public static List<AeronaveXMantenimientoDTO> listEntityToDTOList(List<AeronaveXMantenimiento> roles) {
		List<AeronaveXMantenimientoDTO> rolesDTO = new ArrayList<AeronaveXMantenimientoDTO>();
		for(AeronaveXMantenimiento r : roles) {
			AeronaveXMantenimientoDTO rolDTO = toDto(r);
			rolesDTO.add(rolDTO);
		}	
		return rolesDTO;
	}
}
