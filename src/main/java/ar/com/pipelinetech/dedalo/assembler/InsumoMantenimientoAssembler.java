package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.InsumoMantenimientoDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoMantenimiento;

@Component
public class InsumoMantenimientoAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static InsumoMantenimientoDTO toDto(InsumoMantenimiento insumo) {
		InsumoMantenimientoDTO insumoMantenimientoDTO = mapper.map(insumo, InsumoMantenimientoDTO.class);

		return insumoMantenimientoDTO;
	}
	
	public static InsumoMantenimiento toEntity(InsumoMantenimientoDTO insumoDTO) {
		InsumoMantenimiento insumo = mapper.map(insumoDTO, InsumoMantenimiento.class);
		
		return insumo;
	}
	
	public static List<InsumoMantenimiento> listDTOToEntityList(List<InsumoMantenimientoDTO> insumosDTO) {
		List<InsumoMantenimiento> insumos = new ArrayList<InsumoMantenimiento>();
		for(InsumoMantenimientoDTO m : insumosDTO) {
			InsumoMantenimiento insumo = toEntity(m);
			insumos.add(insumo);
		}	
		return insumos;
	}
	
	public static List<InsumoMantenimientoDTO> listEntityToDTOList(List<InsumoMantenimiento> insumos) {
		List<InsumoMantenimientoDTO> insumosDTO = new ArrayList<InsumoMantenimientoDTO>();
		for(InsumoMantenimiento m : insumos) {
			InsumoMantenimientoDTO insumoDTO = toDto(m);
			insumosDTO.add(insumoDTO);
		}	
		return insumosDTO;
	}
}
