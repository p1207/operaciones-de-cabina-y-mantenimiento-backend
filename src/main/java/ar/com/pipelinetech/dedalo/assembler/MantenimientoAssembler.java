package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.entities.Habilidad;
import ar.com.pipelinetech.dedalo.entities.InsumoMantenimiento;
import ar.com.pipelinetech.dedalo.entities.Mantenimiento;
import ar.com.pipelinetech.dedalo.entities.Tarea;
import ar.com.pipelinetech.dedalo.enums.TipoMantenimiento;

@Component
public class MantenimientoAssembler {

	private static ModelMapper mapper = new ModelMapper();
	
	public static MantenimientoDTO toDto(Mantenimiento mantenimiento) {
		TipoMantenimiento tipo= mantenimiento.getTipo();
		List<TareaDTO> tareasDTO = TareaAssembler.listEntityToDTOList(mantenimiento.getTareas());
		List<HabilidadDTO> habilidadesDTO = HabilidadAssembler.listEntityToDTOList(mantenimiento.getHabilidades());
		List<InsumoMantenimientoDTO> insumosDTO = InsumoMantenimientoAssembler.listEntityToDTOList(mantenimiento.getInsumos());
		PropertyMap<MantenimientoDTO, Mantenimiento> tipoMantenimientoMap = new PropertyMap<MantenimientoDTO, Mantenimiento>() {
		    @Override
		    protected void configure() {
		        skip(destination.getTipo());
		    }
		};
		TypeMap<MantenimientoDTO, Mantenimiento> typeMap = mapper.getTypeMap(MantenimientoDTO.class, Mantenimiento.class);
		if (typeMap == null) {
			mapper.addMappings(tipoMantenimientoMap);
		}
		MantenimientoDTO mantenimientoDTO = mapper.map(mantenimiento, MantenimientoDTO.class);
		mantenimientoDTO.setListaInsumos(insumosDTO);
		mantenimientoDTO.setListaTareas(tareasDTO);
		mantenimientoDTO.setTipoMantenimiento(tipo);
		mantenimientoDTO.setHabilidades(habilidadesDTO);
		
		return mantenimientoDTO;
	}

	public static Mantenimiento toEntity(MantenimientoDTO mantenimientoDTO) {
		List<Tarea> tareas = TareaAssembler.listDTOToEntityList(mantenimientoDTO.getListaTareas());
		List<Habilidad> habilidades = HabilidadAssembler.listDTOToEntityList(mantenimientoDTO.getHabilidades());
		List<InsumoMantenimiento> insumos = InsumoMantenimientoAssembler.listDTOToEntityList(mantenimientoDTO.getListaInsumos());
		PropertyMap<MantenimientoDTO, Mantenimiento> tipoMantenimientoMap = new PropertyMap<MantenimientoDTO, Mantenimiento>() {
		    @Override
		    protected void configure() {
		        skip(destination.getTipo());
		    }
		};
		TypeMap<MantenimientoDTO, Mantenimiento> typeMap = mapper.getTypeMap(MantenimientoDTO.class, Mantenimiento.class);

	
		if (typeMap == null) {
			mapper.addMappings(tipoMantenimientoMap);
		}
		Mantenimiento mantenimiento = mapper.map(mantenimientoDTO, Mantenimiento.class);
		mantenimiento.setInsumos(insumos);
		mantenimiento.setTareas(tareas);
		mantenimiento.setTipo(mantenimientoDTO.getTipoMantenimiento());
		mantenimiento.setHabilidades(habilidades);
		return mantenimiento;
	}
	
	public static List<Mantenimiento> listDTOToEntityList(List<MantenimientoDTO> mantenimientosDTO) {
		List<Mantenimiento> mantenimientos = new ArrayList<Mantenimiento>();
		for(MantenimientoDTO m : mantenimientosDTO) {
			Mantenimiento mantenimiento = toEntity(m);
			mantenimientos.add(mantenimiento);
		}	
		return mantenimientos;
	}
	
	public static List<MantenimientoDTO> listEntityToDTOList(List<Mantenimiento> mantenimientos) {
		List<MantenimientoDTO> mantenimientosDTO = new ArrayList<MantenimientoDTO>();
		for(Mantenimiento m : mantenimientos) {
			MantenimientoDTO mantenimientoDTO = toDto(m);
			mantenimientosDTO.add(mantenimientoDTO);
		}	
		return mantenimientosDTO;
	}
}
