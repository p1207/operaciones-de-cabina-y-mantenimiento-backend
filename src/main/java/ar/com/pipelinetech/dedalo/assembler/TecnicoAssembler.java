package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.entities.Habilidad;
import ar.com.pipelinetech.dedalo.entities.Rol;
import ar.com.pipelinetech.dedalo.entities.Tecnico;
import ar.com.pipelinetech.dedalo.entities.Usuario;
import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.dto.UsuarioDTO;
@Component
public class TecnicoAssembler {

	private static ModelMapper mapper = new ModelMapper();
	public static TecnicoDTO toDto(Tecnico tecnico) {
		List<HabilidadDTO> habilidadesDTO = HabilidadAssembler.listEntityToDTOList(tecnico.getHabilidades());
		RolDTO rol= RolAssembler.toDto(tecnico.getIdRol());
		PropertyMap<Usuario, UsuarioDTO> map = new PropertyMap<Usuario, UsuarioDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		        skip(destination.getHabilidades());
		    }
		};
		TypeMap<Usuario, UsuarioDTO> typeMap = mapper.getTypeMap(Usuario.class, UsuarioDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		TecnicoDTO tecnicoDTO = mapper.map(tecnico, TecnicoDTO.class);
		tecnicoDTO.setRol(rol);
		tecnicoDTO.setHabilidades(habilidadesDTO);
		return tecnicoDTO;
	}

	public static Tecnico toEntity(TecnicoDTO tecnicoDTO) {
		List<Habilidad> habilidades = HabilidadAssembler.listDTOToEntityList(tecnicoDTO.getHabilidades());
		Rol rol= RolAssembler.toEntity(tecnicoDTO.getRol());
		PropertyMap<Usuario, UsuarioDTO> map = new PropertyMap<Usuario, UsuarioDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getRol());
		        skip(destination.getHabilidades());
		    }
		};
		TypeMap<Usuario, UsuarioDTO> typeMap = mapper.getTypeMap(Usuario.class, UsuarioDTO.class);
		if (typeMap == null) {
			mapper.addMappings(map);
		}
		Tecnico tecnico = mapper.map(tecnicoDTO, Tecnico.class);
		tecnico.setIdRol(rol);
		tecnico.setHabilidades(habilidades);
		return tecnico;
	}

	public static List<Tecnico> listDTOToEntityList(List<TecnicoDTO> tecnicosDTO) {
		List<Tecnico> tecnicos = new ArrayList<Tecnico>();
		for (TecnicoDTO t : tecnicosDTO) {
			Tecnico tecnico = toEntity(t);
			tecnicos.add(tecnico);
		}
		return tecnicos;
	}

	public static List<TecnicoDTO> listEntityToDTOList(List<Tecnico> tecnicos) {
		List<TecnicoDTO> tecnicosDTO = new ArrayList<TecnicoDTO>();
		for (Tecnico t : tecnicos) {
			TecnicoDTO tecnicoDTO = toDto(t);
			tecnicosDTO.add(tecnicoDTO);
		}
		return tecnicosDTO;
	}
}
