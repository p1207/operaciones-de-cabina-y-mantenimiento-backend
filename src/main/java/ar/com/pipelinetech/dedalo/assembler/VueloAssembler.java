//package ar.com.pipelinetech.dedalo.assembler;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.modelmapper.ModelMapper;
//import org.modelmapper.PropertyMap;
//import org.modelmapper.TypeMap;
//
//import ar.com.pipelinetech.dedalo.dto.TripulanteDTO;
//import ar.com.pipelinetech.dedalo.dto.VueloDTO;
//import ar.com.pipelinetech.dedalo.entities.Tripulante;
//import ar.com.pipelinetech.dedalo.entities.Vuelo;
//import ar.com.pipelinetech.dedalo.enums.EstadoVuelo;
//import ar.com.pipelinetech.dedalo.enums.TipoVuelo;
//
//public class VueloAssembler {
//private static ModelMapper mapper = new ModelMapper();
//	
//	public static VueloDTO toDto(Vuelo vuelo) {
//		TipoVuelo tipo = vuelo.getTipo();
//		EstadoVuelo estado = vuelo.getEstado();
//		List<TripulanteDTO> tripulacionDTO = TripulanteAssembler.listEntityToDTOList(vuelo.getTripulacion());
//		PropertyMap<VueloDTO, Vuelo> vueloMap = new PropertyMap<VueloDTO, Vuelo>() {
//		    @Override
//		    protected void configure() {
//		        skip(destination.getTipo());
//		        skip(destination.getEstado());
//		    }
//		};
//		TypeMap<VueloDTO, Vuelo> typeMap = mapper.getTypeMap(VueloDTO.class, Vuelo.class);
//		if (typeMap == null) {
//			mapper.addMappings(vueloMap);
//		}
//		VueloDTO vueloDTO = mapper.map(vuelo, VueloDTO.class);
//		vueloDTO.setTripulacion(tripulacionDTO);
//		vueloDTO.setTipo(tipo);
//		vueloDTO.setEstado(estado);
//		
//		return vueloDTO;
//	}
//
//	public static Vuelo toEntity(VueloDTO vueloDTO) {
//		List<Tripulante> tripulacion = TripulanteAssembler.listDTOToEntityList(vueloDTO.getTripulacion());
//		PropertyMap<VueloDTO, Vuelo> vueloMap = new PropertyMap<VueloDTO, Vuelo>() {
//		    @Override
//		    protected void configure() {
//		        skip(destination.getTipo());
//		        skip(destination.getEstado());
//		    }
//		};
//		TypeMap<VueloDTO, Vuelo> typeMap = mapper.getTypeMap(VueloDTO.class, Vuelo.class);
//
//	
//		if (typeMap == null) {
//			mapper.addMappings(vueloMap);
//		}
//		Vuelo vuelo = mapper.map(vueloDTO, Vuelo.class);
//		vuelo.setTipo(vueloDTO.getTipo());
//		vuelo.setEstado(vueloDTO.getEstado());
//		vuelo.setTripulacion(tripulacion);
//		return vuelo;
//	}
//	
//	public static List<Vuelo> listDTOToEntityList(List<VueloDTO> vuelosDTO) {
//		List<Vuelo> vuelos = new ArrayList<Vuelo>();
//		for(VueloDTO m : vuelosDTO) {
//			Vuelo vuelo = toEntity(m);
//			vuelos.add(vuelo);
//		}	
//		return vuelos;
//	}
//	
//	public static List<VueloDTO> listEntityToDTOList(List<Vuelo> vuelos) {
//		List<VueloDTO> vuelosDTO = new ArrayList<VueloDTO>();
//		for(Vuelo v : vuelos) {
//			VueloDTO vueloDTO = toDto(v);
//			vuelosDTO.add(vueloDTO);
//		}	
//		return vuelosDTO;
//	}
//
//}
