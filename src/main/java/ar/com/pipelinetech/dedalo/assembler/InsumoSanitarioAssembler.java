package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.InsumoSanitarioDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoSanitario;

@Component
public class InsumoSanitarioAssembler {
	
private static ModelMapper mapper = new ModelMapper();
	
	public static InsumoSanitarioDTO toDto(InsumoSanitario insumoSanitario) {
		InsumoSanitarioDTO insumoSanitarioDTO = mapper.map(insumoSanitario, InsumoSanitarioDTO.class);
		return insumoSanitarioDTO;
	}

	public static InsumoSanitario toEntity(InsumoSanitarioDTO insumoSanitarioDTO) {
		InsumoSanitario insumoSanitario = mapper.map(insumoSanitarioDTO, InsumoSanitario.class);
		return insumoSanitario;
	}

	public static List<InsumoSanitario> listDTOToEntityList(List<InsumoSanitarioDTO> insumosSanitarioDTO) {
		List<InsumoSanitario> insumosSanitario = new ArrayList<InsumoSanitario>();
		for(InsumoSanitarioDTO i : insumosSanitarioDTO) {
			InsumoSanitario insumoSanitario = toEntity(i);
			insumosSanitario.add(insumoSanitario);
		}	
		return insumosSanitario;
	}

	public static List<InsumoSanitarioDTO> listEntityToDTOList(List<InsumoSanitario> insumosSanitario) {
		List<InsumoSanitarioDTO> insumosSanitarioDTO = new ArrayList<InsumoSanitarioDTO>();
		for(InsumoSanitario i : insumosSanitario) {
			InsumoSanitarioDTO insumoSanitarioDTO = toDto(i);
			insumosSanitarioDTO.add(insumoSanitarioDTO);
		}	
		return insumosSanitarioDTO;
	}
}
