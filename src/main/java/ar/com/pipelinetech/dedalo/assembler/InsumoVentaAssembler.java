package ar.com.pipelinetech.dedalo.assembler;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import ar.com.pipelinetech.dedalo.dto.InsumoVentaDTO;
import ar.com.pipelinetech.dedalo.entities.InsumoVenta;

@Component
public class InsumoVentaAssembler {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public static InsumoVentaDTO toDto(InsumoVenta insumoVenta) {
		InsumoVentaDTO insumoVentaDTO = mapper.map(insumoVenta, InsumoVentaDTO.class);
		return insumoVentaDTO;
	}

	public static InsumoVenta toEntity(InsumoVentaDTO insumoVentaDTO) {
		InsumoVenta insumoVenta = mapper.map(insumoVentaDTO, InsumoVenta.class);
		return insumoVenta;
	}

	public static List<InsumoVenta> listDTOToEntityList(List<InsumoVentaDTO> insumosVentaDTO) {
		List<InsumoVenta> insumosVenta = new ArrayList<InsumoVenta>();
		for(InsumoVentaDTO i : insumosVentaDTO) {
			InsumoVenta insumoVenta = toEntity(i);
			insumosVenta.add(insumoVenta);
		}	
		return insumosVenta;
	}

	public static List<InsumoVentaDTO> listEntityToDTOList(List<InsumoVenta> insumosVenta) {
		List<InsumoVentaDTO> insumosVentaDTO = new ArrayList<InsumoVentaDTO>();
		for(InsumoVenta i : insumosVenta) {
			InsumoVentaDTO insumoVentaDTO = toDto(i);
			insumosVentaDTO.add(insumoVentaDTO);
		}	
		return insumosVentaDTO;
	}
}
