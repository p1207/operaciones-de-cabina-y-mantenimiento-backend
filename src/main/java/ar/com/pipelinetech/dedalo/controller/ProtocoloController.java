package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.ProtocoloDTO;
import ar.com.pipelinetech.dedalo.services.ProtocoloService;

@RestController
@RequestMapping ("/Protocolo")
public class ProtocoloController {
	
	@Autowired
	private ProtocoloService protocoloService;
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<ProtocoloDTO>> getProtocolos() {
		List <ProtocoloDTO> result = protocoloService.getProtocolos();
		ResponseEntity<List<ProtocoloDTO>> response = new ResponseEntity<List<ProtocoloDTO>>(result,HttpStatus.OK);
		return response;
	}	
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/{id}")
	public ResponseEntity<ProtocoloDTO> getProtocolo(@PathVariable Integer id) {
		ProtocoloDTO result = protocoloService.getProtocoloId(id);
		ResponseEntity<ProtocoloDTO> response = new ResponseEntity<ProtocoloDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public void saveMantenimiento(@RequestBody final ProtocoloDTO protocoloDTO) {
		protocoloService.saveProtocolo(protocoloDTO);
	}
	

	
}

