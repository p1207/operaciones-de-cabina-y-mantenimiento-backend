package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.TareaProtocoloDTO;
import ar.com.pipelinetech.dedalo.services.TareaProtocoloService;


@RestController
@RequestMapping ("/TareaProtocolo")
public class TareaProtocoloController {
	
	@Autowired
	private TareaProtocoloService tareaProtocoloService;
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<TareaProtocoloDTO>> getTareasProtocolo() {
		List<TareaProtocoloDTO> result = tareaProtocoloService.getTareasProtocolo();
		ResponseEntity<List<TareaProtocoloDTO>> response = new ResponseEntity<List<TareaProtocoloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/{idPadre}")
	public ResponseEntity<List<TareaProtocoloDTO>> getTareasProtocolo(@PathVariable Integer idPadre) {
		List<TareaProtocoloDTO> result = tareaProtocoloService.getTareasProtocoloId(idPadre);
		ResponseEntity<List<TareaProtocoloDTO>> response = new ResponseEntity<List<TareaProtocoloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public void saveMantenimiento(@RequestBody final TareaProtocoloDTO tareaProtocoloDTO) {
		tareaProtocoloService.saveTareaProtocolo(tareaProtocoloDTO);
	}

}
