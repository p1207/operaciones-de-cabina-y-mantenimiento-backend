package ar.com.pipelinetech.dedalo.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.CantidadMenuDTO;
import ar.com.pipelinetech.dedalo.dto.InsumoCateringDTO;
import ar.com.pipelinetech.dedalo.dto.PasajeroDTO;
import ar.com.pipelinetech.dedalo.services.InsumoCateringService;

@RestController
@RequestMapping ("/Insumo/Catering")
public class InsumoCateringController {
	
	@Autowired
	private InsumoCateringService insumoCateringService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<InsumoCateringDTO>> getInsumosCatering() {
		List<InsumoCateringDTO> result = insumoCateringService.getInsumosCatering();
		ResponseEntity<List<InsumoCateringDTO>> response = new ResponseEntity<List<InsumoCateringDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<InsumoCateringDTO> saveInsumo(@RequestBody final InsumoCateringDTO insumo) {
		InsumoCateringDTO result = insumoCateringService.saveInsumo(insumo);
		ResponseEntity<InsumoCateringDTO> response = new ResponseEntity<InsumoCateringDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/pasajeros/{codigoVuelo}")
	public ResponseEntity<List<PasajeroDTO>> getPasajeros(@PathVariable String codigoVuelo) {
		if(codigoVuelo == null) {
			List<PasajeroDTO> result = new ArrayList<PasajeroDTO>();
			ResponseEntity<List<PasajeroDTO>> response = new ResponseEntity<List<PasajeroDTO>>(result,HttpStatus.OK);
			return response;
		}
			
		List<PasajeroDTO> result = insumoCateringService.getPasajeros(codigoVuelo);
		ResponseEntity<List<PasajeroDTO>> response = new ResponseEntity<List<PasajeroDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/menus/{codigoVuelo}")
	public ResponseEntity<CantidadMenuDTO> getMenus(@PathVariable String codigoVuelo) {
		if(codigoVuelo == null) {
			CantidadMenuDTO result = new CantidadMenuDTO();
			ResponseEntity<CantidadMenuDTO> response = new ResponseEntity<CantidadMenuDTO>(result,HttpStatus.OK);
			return response;
		}
			
		CantidadMenuDTO result = insumoCateringService.getMenus(codigoVuelo);
		ResponseEntity<CantidadMenuDTO> response = new ResponseEntity<CantidadMenuDTO>(result,HttpStatus.OK);
		return response;
	}
	
	
}
