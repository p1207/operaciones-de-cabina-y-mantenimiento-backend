package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.HabilidadDTO;
import ar.com.pipelinetech.dedalo.services.HabilidadService;

@RestController
@RequestMapping ("/Habilidades")
public class HabilidadController {
	@Autowired
	private HabilidadService habilidadService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<HabilidadDTO>> getHabilidad() {
		List<HabilidadDTO> result = habilidadService.getHabilidades();
		ResponseEntity<List<HabilidadDTO>> response = new ResponseEntity<List<HabilidadDTO>>(result,HttpStatus.OK);
		return response;
	}
}
