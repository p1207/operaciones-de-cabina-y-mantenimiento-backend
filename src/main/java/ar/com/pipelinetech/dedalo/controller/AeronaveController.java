package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.Aeronave2DTO;
import ar.com.pipelinetech.dedalo.services.AeronaveService;
//import ar.com.pipelinetech.dedalo.dto.AeronaveDTO;

@RestController
@RequestMapping ("/Aeronaves")
public class AeronaveController{

		@Autowired
		private AeronaveService aeronaveService; 
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/all")
		public ResponseEntity<List<Aeronave2DTO>> getAeronaves2() {
			List<Aeronave2DTO> result = aeronaveService.getAeronaves2();
			ResponseEntity<List<Aeronave2DTO>> response = new ResponseEntity<List<Aeronave2DTO>>(result,HttpStatus.OK);
			return response;
		}

}
