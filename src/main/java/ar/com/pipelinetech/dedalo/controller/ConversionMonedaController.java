package ar.com.pipelinetech.dedalo.controller;

import java.io.IOException;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.services.ConversionMonedaService;

@RestController
@RequestMapping ("/ConversionMoneda")
public class ConversionMonedaController {

		@Autowired
		private ConversionMonedaService monedaService; 
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping("/DolarAPesos/{valor}")
		public ResponseEntity<BigDecimal> getValorEnPesos(@PathVariable BigDecimal valor) throws IOException {
			BigDecimal result = monedaService.getValorEnPesos(valor);
			ResponseEntity<BigDecimal> valorEnPesos = new	ResponseEntity<BigDecimal>(result, HttpStatus.OK);	
			return valorEnPesos;
		}
		
		@GetMapping("/PesosADolar/{valor}")
		public ResponseEntity<BigDecimal> getValorEnDolares(@PathVariable BigDecimal valor) throws IOException {
			BigDecimal result = monedaService.getValorEnDolares(valor);
			ResponseEntity<BigDecimal> valorEnPesos = new	ResponseEntity<BigDecimal>(result, HttpStatus.OK);	
			return valorEnPesos;
		}
	}
