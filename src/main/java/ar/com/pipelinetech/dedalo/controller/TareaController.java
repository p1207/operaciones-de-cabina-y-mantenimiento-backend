package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.TareaDTO;
import ar.com.pipelinetech.dedalo.services.TareaService;

@RestController
@RequestMapping ("/Tareas")
public class TareaController {
	@Autowired
	private TareaService tareaService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<TareaDTO>> getTarea() {
		List<TareaDTO> result = tareaService.getTareas();
		ResponseEntity<List<TareaDTO>> response = new ResponseEntity<List<TareaDTO>>(result,HttpStatus.OK);
		return response;
	}
}
