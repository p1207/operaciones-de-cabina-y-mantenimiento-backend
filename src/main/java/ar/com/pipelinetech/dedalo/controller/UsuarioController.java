package ar.com.pipelinetech.dedalo.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.assembler.VueloXTripulanteAssembler;
import ar.com.pipelinetech.dedalo.dto.IdPersonalDTO;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.dto.UsuarioDTO;
import ar.com.pipelinetech.dedalo.dto.VueloXTripulanteDTO;
import ar.com.pipelinetech.dedalo.services.UsuarioService;

@RestController
@CrossOrigin
@RequestMapping ("/Usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<UsuarioDTO>> getUsuarios() {
		List<UsuarioDTO> result = usuarioService.getUsuarios();
		ResponseEntity<List<UsuarioDTO>> response = new ResponseEntity<List<UsuarioDTO>>(result,HttpStatus.OK);
		return response;
	}

	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<UsuarioDTO> saveUsuario(@RequestBody final UsuarioDTO usuario) {
		UsuarioDTO result = usuarioService.saveUsuario(usuario);
		ResponseEntity<UsuarioDTO> response = new ResponseEntity<UsuarioDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/saveTecnico")
    public ResponseEntity<TecnicoDTO> saveUsuarioTecnico(@RequestBody final TecnicoDTO usuario) {
		TecnicoDTO result = usuarioService.saveUsuarioTecnico(usuario);
		ResponseEntity<TecnicoDTO> response = new ResponseEntity<TecnicoDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/autenticacion")
    public ResponseEntity<UsuarioDTO> autenticarUsuario(@RequestBody final UsuarioDTO usuario) {
		UsuarioDTO result = usuarioService.autenticarUsuario(usuario);
		ResponseEntity<UsuarioDTO> response = new ResponseEntity<UsuarioDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/guardarTripulantes")
    public ResponseEntity<String> guardarTripulantes() {
		String result = usuarioService.saveUsuariosTripulantes();
		ResponseEntity<String> response = new ResponseEntity<String>(result,HttpStatus.OK);
		return response;
	}

	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/getVueloByTripulante")
    public ResponseEntity<VueloXTripulanteDTO[]> obtenerVueloByTripulante(@RequestBody final IdPersonalDTO idpersonal_fk){
		VueloXTripulanteDTO [] result = VueloXTripulanteAssembler.listEntityToDTOList(usuarioService.getTripulantesVuelo(idpersonal_fk));
		ResponseEntity<VueloXTripulanteDTO[]> response = new ResponseEntity<VueloXTripulanteDTO[]>(result,HttpStatus.OK);
		return response;
	}
	
}
