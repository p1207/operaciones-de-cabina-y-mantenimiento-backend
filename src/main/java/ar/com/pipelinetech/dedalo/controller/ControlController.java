package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.ControlDTO;
import ar.com.pipelinetech.dedalo.dto.RolDTO;
import ar.com.pipelinetech.dedalo.services.ControlService;

@RestController
@RequestMapping ("/Controles")
public class ControlController {
	
	@Autowired
	private ControlService controlService;
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<ControlDTO>> getControles(){
		List<ControlDTO> result = controlService.getControles();
		ResponseEntity<List<ControlDTO>> response = new ResponseEntity<List<ControlDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<ControlDTO>  saveControl(@RequestBody final ControlDTO control) {
		ControlDTO result = controlService.saveControl(control);
		ResponseEntity<ControlDTO> c = new ResponseEntity<ControlDTO>(result,HttpStatus.OK);
		return c;
		 
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@DeleteMapping("/delete/{id}")
    public void softDeleteControl(@PathVariable Integer id) {	
		controlService.softDeleteControl(id);
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allRoles")
	public ResponseEntity<List<RolDTO>> getRoles(){
		List<RolDTO> result = controlService.getRoles();
		ResponseEntity<List<RolDTO>> response = new ResponseEntity<List<RolDTO>>(result,HttpStatus.OK);
		return response;
	}
	
}