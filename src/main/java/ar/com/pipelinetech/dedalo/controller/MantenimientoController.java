package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.AeronaveXMantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoDTO;
import ar.com.pipelinetech.dedalo.dto.MantenimientoXAeronaveDTO;
import ar.com.pipelinetech.dedalo.services.MantenimientoService;

@RestController
@RequestMapping ("/Mantenimientos")
public class MantenimientoController{

		@Autowired
		private MantenimientoService mantenimientoService; 
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/all")
		public ResponseEntity<List<MantenimientoDTO>> getMantenimientos() {
			List<MantenimientoDTO> result = mantenimientoService.getMantenimientos();
			ResponseEntity<List<MantenimientoDTO>> response = new ResponseEntity<List<MantenimientoDTO>>(result,HttpStatus.OK);
			return response;
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@PostMapping ("/save")
	    public void saveMantenimiento(@RequestBody final MantenimientoDTO mantenimiento) {
			
			mantenimientoService.saveMantenimiento(mantenimiento);
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@DeleteMapping("/delete/{id}")
	    public void softDeleteMantenimiento(@PathVariable Integer id) {
			
			mantenimientoService.softDeleteMantenimiento(id);
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@PutMapping("/update")  
		private void update(@RequestBody final MantenimientoDTO mantenimiento)   
		{  
			mantenimientoService.updateMantenimiento(mantenimiento);
		}		
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/allByAeronave/{matricula}")
		public ResponseEntity<MantenimientoXAeronaveDTO> getSolicitudesMantenimientosXAeronave(@PathVariable String matricula) {
			MantenimientoXAeronaveDTO result = mantenimientoService.solicitudMantenimientoxAeronave(matricula);
			ResponseEntity<MantenimientoXAeronaveDTO> response = new ResponseEntity<MantenimientoXAeronaveDTO>(result,HttpStatus.OK);
			return response;
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@PostMapping ("/saveAeronaveXMantenimiento")
	    public void saveAeronaveXMantenimiento(@RequestBody final AeronaveXMantenimientoDTO aeronavexmantenimiento) {			
			mantenimientoService.saveAeronaveXMantenimiento(aeronavexmantenimiento);
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/allMantenimientosByAeronave/{matricula}")
		public ResponseEntity<List<AeronaveXMantenimientoDTO>> getMantenimientosXAeronave(@PathVariable String matricula) {
			List<AeronaveXMantenimientoDTO> result = mantenimientoService.mantenimientosxAeronave(matricula);
			ResponseEntity<List<AeronaveXMantenimientoDTO>> response = new ResponseEntity<List<AeronaveXMantenimientoDTO>>(result,HttpStatus.OK);
			return response;
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/savesMantenimientosAll")
	    public void saveAllMantenimientos() {			
			mantenimientoService.saveAllAeronaveXMantenimiento();
		}
		

}
