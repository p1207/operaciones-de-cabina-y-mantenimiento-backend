package ar.com.pipelinetech.dedalo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.ControlXVueloDTO;
import ar.com.pipelinetech.dedalo.dto.ControlesXVueloDTO;
import ar.com.pipelinetech.dedalo.services.ControlXVueloService;

@RestController
@RequestMapping ("/ControlXVuelo")
public class ControlXVueloController {

	@Autowired
	private ControlXVueloService controlXVueloService;
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping("/all")
	public ResponseEntity<List<ControlXVueloDTO>> getControlXVuelo(){
		List<ControlXVueloDTO> result = controlXVueloService.getControles();
		ResponseEntity<List<ControlXVueloDTO>> controles = new ResponseEntity<List<ControlXVueloDTO>>(result,HttpStatus.OK);
		return controles;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<ControlXVueloDTO> saveControlXVuelo(@RequestBody final ControlXVueloDTO controlVuelo) {
		controlVuelo.setFecha_creacion(new Date());
		ControlXVueloDTO result =controlXVueloService.saveControlXVuelo(controlVuelo);
		ResponseEntity<ControlXVueloDTO> control = new ResponseEntity<ControlXVueloDTO>(result,HttpStatus.OK);
		return control;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@DeleteMapping("/delete/{id}")
    public void softDeleteControlXVuelo(@PathVariable Integer id) {
		controlXVueloService.deleteControlXVuelo(id);
	}

	@ResponseStatus (code = HttpStatus.OK)
	@PutMapping("/update")  
	private void update(@RequestBody final ControlXVueloDTO controlVuelo){  
		controlXVueloService.updateControlXVuelo(controlVuelo);
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allByVuelo/{vuelo}")
	public ResponseEntity<ControlesXVueloDTO> getControlesXVuelo(@PathVariable String vuelo) {
		ControlesXVueloDTO result = controlXVueloService.controlesXVuelo(vuelo);
		
		ResponseEntity<ControlesXVueloDTO> response = new ResponseEntity<ControlesXVueloDTO>(result,HttpStatus.OK);
		return response;
	}
	
}
