package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.TecnicoDTO;
import ar.com.pipelinetech.dedalo.services.TecnicoService;

@RestController
@RequestMapping ("/Tecnicos")
public class TecnicoController {

	@Autowired
	private TecnicoService tecnicoService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<TecnicoDTO>> getTecnicos() {
		List<TecnicoDTO> result = tecnicoService.getTecnicos();
		ResponseEntity<List<TecnicoDTO>> response = new ResponseEntity<List<TecnicoDTO>>(result,HttpStatus.OK);
		return response;
	}
}
