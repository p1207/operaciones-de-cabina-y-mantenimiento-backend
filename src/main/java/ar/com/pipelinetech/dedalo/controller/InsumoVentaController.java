package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.InsumoVentaDTO;
import ar.com.pipelinetech.dedalo.services.InsumoVentaService;


@RestController
@RequestMapping ("/Insumo/Venta")
public class InsumoVentaController {
	
	@Autowired
	private InsumoVentaService insumoVentaService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<InsumoVentaDTO>> getInsumosVenta() {
		List<InsumoVentaDTO> result = insumoVentaService.getInsumosVenta();
		ResponseEntity<List<InsumoVentaDTO>> response = new ResponseEntity<List<InsumoVentaDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<InsumoVentaDTO> saveInsumo(@RequestBody final InsumoVentaDTO insumo) {
		InsumoVentaDTO result = insumoVentaService.saveInsumo(insumo);
		ResponseEntity<InsumoVentaDTO> response = new ResponseEntity<InsumoVentaDTO>(result,HttpStatus.OK);
		return response;
	}
	
}
