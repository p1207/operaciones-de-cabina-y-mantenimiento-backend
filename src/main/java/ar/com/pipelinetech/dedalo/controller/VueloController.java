package ar.com.pipelinetech.dedalo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.ControlDTO;
import ar.com.pipelinetech.dedalo.dto.ControlXVueloDTO;
import ar.com.pipelinetech.dedalo.dto.Vuelo2DTO;
import ar.com.pipelinetech.dedalo.enums.EstadoControlXVuelo;
import ar.com.pipelinetech.dedalo.services.ControlService;
import ar.com.pipelinetech.dedalo.services.ControlXVueloService;
import ar.com.pipelinetech.dedalo.services.InsumoCateringService;
import ar.com.pipelinetech.dedalo.services.VueloService;

@RestController
@RequestMapping ("/Vuelo")
public class VueloController {

	@Autowired
	private VueloService vueloService; 
	
	@Autowired 
	private ControlXVueloService controlXVueloService;
	
	@Autowired
	private ControlService controlService;
	
	@Autowired
	private InsumoCateringService insumoCateringService;
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<Vuelo2DTO>> getVuelos2() { //Lista de todos los vuelos
		List<Vuelo2DTO> result2 = vueloService.getVuelos2();
		ResponseEntity<List<Vuelo2DTO>> response = new ResponseEntity<List<Vuelo2DTO>>(result2,HttpStatus.OK);
		return response;
	}

	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/estado/{codigoVuelo}") //Ver estado de un vuelo por id
	public ResponseEntity<Vuelo2DTO> getEstado(@PathVariable String codigoVuelo) {
		if(codigoVuelo == null) {
			Vuelo2DTO result = new Vuelo2DTO();
			ResponseEntity<Vuelo2DTO> response = new ResponseEntity<Vuelo2DTO>(result,HttpStatus.OK);
			return response;
		}
			
		Vuelo2DTO result = vueloService.getEstado(codigoVuelo);
		ResponseEntity<Vuelo2DTO> response = new ResponseEntity<Vuelo2DTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping("/setearControlesXVueloGeneral")
	public ResponseEntity<String> setearControlesATodos(){
		List<ControlDTO> controles = controlService.getControles();
		List<Vuelo2DTO> vuelos = vueloService.getVuelos2();
		System.out.println(vuelos);
		for(Vuelo2DTO v : vuelos) {
			for(ControlDTO c : controles) {
				System.out.println(controles);
				ControlXVueloDTO controlxvuelo = new ControlXVueloDTO(null, c.getId(), v.getIdvuelo(), new Date() ,"Pendiente", EstadoControlXVuelo.PENDIENTE, c );
				controlXVueloService.saveControlXVuelo(controlxvuelo);
			}
		}
		ResponseEntity<String> response = new ResponseEntity<String>("Se cargaron todos los controles",HttpStatus.OK);
		return response;
	}
	
	
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping("/saveControlesXVuelo/{codigoVuelo}")
	public ResponseEntity<String> setearControlAVuelo(@PathVariable String codigoVuelo){
		List<ControlDTO> controles = controlService.getControles();
		for(ControlDTO c : controles) {
			ControlXVueloDTO controlxvuelo = new ControlXVueloDTO(null, c.getId(), codigoVuelo, new Date() ,"Pendiente", EstadoControlXVuelo.PENDIENTE, c );
			controlXVueloService.saveControlXVuelo(controlxvuelo);
		}
		ResponseEntity<String> response = new ResponseEntity<String>(codigoVuelo,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping("/saveInsumosXVuelo/{codigoVuelo}")
	public ResponseEntity<String> setearInsumosAVuelo(@PathVariable String codigoVuelo){
		String result = vueloService.setearInsumos(codigoVuelo);
		ResponseEntity<String> response = new ResponseEntity<String>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping("/saveInsumosXVuelosEnGeneral")
	public ResponseEntity<String> setearInsumosAVuelos(){
		List<Vuelo2DTO> vuelos = vueloService.getVuelos2();
		for(Vuelo2DTO v: vuelos) {
			if(v.getAeronave_matricula_fk()!=null) {
				vueloService.setearInsumos(v.getIdvuelo());
			}
		}
		ResponseEntity<String> response = new ResponseEntity<String>("Se cargaron todos los insumos",HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping("/saveInsumosCateringXVuelo/{codigoVuelo}")
	public ResponseEntity<String> setearAlimentacionAVuelo(@PathVariable String codigoVuelo){
		String result = insumoCateringService.setearAlimentacion(codigoVuelo);
		ResponseEntity<String> response = new ResponseEntity<String>(result,HttpStatus.OK);
		return response;
	}
	
	
	
}
