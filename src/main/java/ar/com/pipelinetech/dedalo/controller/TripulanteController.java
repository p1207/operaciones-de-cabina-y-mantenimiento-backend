package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.Tripulante2DTO;
import ar.com.pipelinetech.dedalo.dto.TripulanteDTO;
import ar.com.pipelinetech.dedalo.services.TripulanteService;


@RestController
@RequestMapping ("/Tripulantes")
public class TripulanteController {

		@Autowired
		private TripulanteService tripulanteService;
		
		@ResponseStatus (code = HttpStatus.OK)
		@GetMapping ("/all")
		public ResponseEntity<List<TripulanteDTO>> getTripulantes() {
			List<TripulanteDTO> result = tripulanteService.getTripulantes();
			ResponseEntity<List<TripulanteDTO>> response = new ResponseEntity<List<TripulanteDTO>>(result,HttpStatus.OK);
			return response;
		}
		
		@ResponseStatus (code = HttpStatus.OK)
		@PostMapping ("/ext")
	    public ResponseEntity <Tripulante2DTO []> getTripulantes(@RequestBody String idvuelo) {
			Tripulante2DTO [] result = tripulanteService.getTripulantesVuelo(idvuelo);
			ResponseEntity<Tripulante2DTO []> response = new ResponseEntity<Tripulante2DTO []>(result,HttpStatus.OK);
			return response;
		}
		
		
}
