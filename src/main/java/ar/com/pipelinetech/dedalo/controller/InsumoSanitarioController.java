package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.InsumoSanitarioDTO;
import ar.com.pipelinetech.dedalo.services.InsumoSanitarioService;


@RestController
@RequestMapping ("/Insumo/Sanitario")
public class InsumoSanitarioController {
	
	@Autowired
	private InsumoSanitarioService insumoSanitarioService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<InsumoSanitarioDTO>> getInsumosSanitario() {
		List<InsumoSanitarioDTO> result = insumoSanitarioService.getInsumosSanitario();
		ResponseEntity<List<InsumoSanitarioDTO>> response = new ResponseEntity<List<InsumoSanitarioDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<InsumoSanitarioDTO> saveInsumo(@RequestBody final InsumoSanitarioDTO insumo) {
		InsumoSanitarioDTO result = insumoSanitarioService.saveInsumo(insumo);
		ResponseEntity<InsumoSanitarioDTO> response = new ResponseEntity<InsumoSanitarioDTO>(result,HttpStatus.OK);
		return response;
	}
	
}