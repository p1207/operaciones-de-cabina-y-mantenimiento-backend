package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.TipoEmergenciaDTO;
import ar.com.pipelinetech.dedalo.services.TipoEmergenciaService;


@RestController
@RequestMapping ("/TipoEmergencia")
public class TipoEmergenciaController {

	@Autowired
	private TipoEmergenciaService tipoEmergenciaService;

	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<TipoEmergenciaDTO>> getTipoEmergencias() {
		List<TipoEmergenciaDTO> result = tipoEmergenciaService.getTipoEmergencias();
		ResponseEntity<List<TipoEmergenciaDTO>> response = new ResponseEntity<List<TipoEmergenciaDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/{id}")
	public ResponseEntity <TipoEmergenciaDTO> getTipoEmergenciasByID(@PathVariable Integer id) {
		TipoEmergenciaDTO result = tipoEmergenciaService.getTipoEmergenciaByID(id);
		ResponseEntity<TipoEmergenciaDTO> response = new ResponseEntity<TipoEmergenciaDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity <TipoEmergenciaDTO> saveTipoEmergencia(@RequestBody final TipoEmergenciaDTO tipoEmergenciaDTO) {
		TipoEmergenciaDTO result = tipoEmergenciaService.saveTipoEmergencia(tipoEmergenciaDTO);
		ResponseEntity<TipoEmergenciaDTO> response = new ResponseEntity<TipoEmergenciaDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@DeleteMapping("/delete/{id}")
    public void deleteTipoMantenimiento(@PathVariable Integer id) {
		tipoEmergenciaService.softDeleteTipoEmergencia(id);
	}
	

} 
