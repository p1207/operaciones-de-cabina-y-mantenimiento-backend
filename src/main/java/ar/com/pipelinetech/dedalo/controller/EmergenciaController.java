package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.EmergenciaDTO;
import ar.com.pipelinetech.dedalo.services.EmergenciaService;


@RestController
@RequestMapping ("/Emergencia")
public class EmergenciaController {
	
	@Autowired
	private EmergenciaService emergenciaService; 
	

	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<EmergenciaDTO>> getEmergencias() {
		List<EmergenciaDTO> result = emergenciaService.getEmergencias();
		ResponseEntity<List<EmergenciaDTO>> response = new ResponseEntity<List<EmergenciaDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allByVuelo/{idVuelo}")
	public ResponseEntity<List<EmergenciaDTO>> getEmergenciasByVuelo(@PathVariable String idVuelo) {
		List<EmergenciaDTO> result = emergenciaService.getEmergenciasByVuelo(idVuelo);
		ResponseEntity<List<EmergenciaDTO>> response = new ResponseEntity<List<EmergenciaDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public void saveMantenimiento(@RequestBody final EmergenciaDTO emergenciaDTO) {
		emergenciaService.saveEmergencia(emergenciaDTO);
	}

}
