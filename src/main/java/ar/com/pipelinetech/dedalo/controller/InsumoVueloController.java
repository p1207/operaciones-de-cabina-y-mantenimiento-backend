package ar.com.pipelinetech.dedalo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.InsumoVueloDTO;
import ar.com.pipelinetech.dedalo.services.InsumoVueloService;


@RestController
@RequestMapping ("/Insumo/Vuelo")
public class InsumoVueloController {
	
	
	@Autowired
	private InsumoVueloService insumoVueloService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<InsumoVueloDTO>> getInsumosVuelo() {
		List<InsumoVueloDTO> result = insumoVueloService.getInsumosVuelos();
		ResponseEntity<List<InsumoVueloDTO>> response = new ResponseEntity<List<InsumoVueloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	
	@ResponseStatus (code = HttpStatus.OK)
	@PutMapping ("/updateCantidadFinal")
	public void updateCantidadFinalVuelo(@RequestBody final InsumoVueloDTO insumoVuelo) {
		insumoVueloService.updateCantidadFinal(insumoVuelo);
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping (path = "/nuevoItemVuelo")
	public void nuevoItemVuelo(@RequestBody final InsumoVueloDTO insumoVueloDTO) {
		insumoVueloService.nuevoInsumoVuelo(insumoVueloDTO);
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@DeleteMapping("/delete/{id}")
    public void deleteInsumoXVuelo(@PathVariable Integer id) {
		insumoVueloService.deleteInsumoXVuelo(id);
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all/{vuelo}")
	public ResponseEntity <List<InsumoVueloDTO>> getInsumosByCodigo(@PathVariable String vuelo) {
		List<InsumoVueloDTO> result = insumoVueloService.getInsumosByVuelo(vuelo);
		ResponseEntity <List<InsumoVueloDTO>> response = new ResponseEntity <List<InsumoVueloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allVentaByVuelo/{vuelo}")
	public ResponseEntity <List<InsumoVueloDTO>> getInsumosVentaXVueloByCodigo(@PathVariable String vuelo) {
		List<InsumoVueloDTO> result = insumoVueloService.getInsumosVentaXVueloByCodigo(vuelo);
		ResponseEntity <List<InsumoVueloDTO>> response = new ResponseEntity <List<InsumoVueloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allSanitarioByVuelo/{vuelo}")
	public ResponseEntity <List<InsumoVueloDTO>> getInsumosSanitarioXVueloByCodigo(@PathVariable String vuelo) {
		List<InsumoVueloDTO> result = insumoVueloService.getInsumosSanitarioXVueloByCodigo(vuelo);
		ResponseEntity <List<InsumoVueloDTO>> response = new ResponseEntity <List<InsumoVueloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/allCateringByVuelo/{vuelo}")
	public ResponseEntity <List<InsumoVueloDTO>> getInsumosCateringXVueloByCodigo(@PathVariable String vuelo) {
		List<InsumoVueloDTO> result = insumoVueloService.getInsumosCateringXVueloByCodigo(vuelo);
		ResponseEntity <List<InsumoVueloDTO>> response = new ResponseEntity <List<InsumoVueloDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/cantidadFinalByCodVuelo/{vuelo}")
	public ResponseEntity <Integer> obtenerCantidadFinalInsumos(@PathVariable String vuelo) {
		Integer result = insumoVueloService.obtenerCantidadFinalInsumos(vuelo);
		ResponseEntity <Integer> response = new ResponseEntity <Integer>(result,HttpStatus.OK);
		return response;
	}

	
}
