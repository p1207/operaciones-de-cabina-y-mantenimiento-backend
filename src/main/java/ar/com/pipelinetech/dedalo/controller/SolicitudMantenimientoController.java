package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ar.com.pipelinetech.dedalo.dto.SolicitudMantenimientoDTO;
import ar.com.pipelinetech.dedalo.services.SolicitudMantenimientoService;

@RestController
@RequestMapping ("/Solicitudes")
public class SolicitudMantenimientoController {

	@Autowired
	private SolicitudMantenimientoService solicitudService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/all")
	public ResponseEntity<List<SolicitudMantenimientoDTO>> getSolicitudesMantenimiento() {
		List<SolicitudMantenimientoDTO> result = solicitudService.getSolicitudes();
		ResponseEntity<List<SolicitudMantenimientoDTO>> response = new ResponseEntity<List<SolicitudMantenimientoDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/RequestById/{id}")
	public ResponseEntity<SolicitudMantenimientoDTO> getSolicitudMantenimientoById(@PathVariable Integer id) {
		SolicitudMantenimientoDTO result = solicitudService.getSolicitudById(id);
		ResponseEntity<SolicitudMantenimientoDTO> response = new ResponseEntity<SolicitudMantenimientoDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/RequestByIdTecnico/{id}")
	public ResponseEntity<List<SolicitudMantenimientoDTO>> getSolicitudMantenimientoByIdTecnico(@PathVariable Integer id) {
		List<SolicitudMantenimientoDTO> result = solicitudService.getSolicitudByIdTecnico(id);
		ResponseEntity<List<SolicitudMantenimientoDTO>> response = new ResponseEntity<List<SolicitudMantenimientoDTO>>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping ("/save")
    public ResponseEntity<SolicitudMantenimientoDTO> saveSolicitudMantenimiento(@RequestBody final SolicitudMantenimientoDTO solicitud) {	
		SolicitudMantenimientoDTO result = solicitudService.saveSolicitudMantenimiento(solicitud);
		ResponseEntity<SolicitudMantenimientoDTO> response = new ResponseEntity<SolicitudMantenimientoDTO>(result,HttpStatus.OK);
		return response;
	}
	
	@ResponseStatus (code = HttpStatus.OK)
	@PostMapping("/delete/{id}")
	public void deleteSolicitudMantenimiento(@PathVariable Integer id) {
		
		solicitudService.deleteSolicitudMantenimiento(id);
	}
	

	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping ("/cargarAllSolicitudes")
	public ResponseEntity<String> cargarSolicitudesAll() {
		String result = solicitudService.saveSolicitudesMasivo();
		ResponseEntity<String> response = new ResponseEntity<String>(result,HttpStatus.OK);
		return response;
	}
	
	
}
