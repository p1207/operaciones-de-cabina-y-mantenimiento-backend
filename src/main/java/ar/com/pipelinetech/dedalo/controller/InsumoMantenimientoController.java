package ar.com.pipelinetech.dedalo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ar.com.pipelinetech.dedalo.dto.InsumoMantenimientoDTO;
import ar.com.pipelinetech.dedalo.services.InsumoMantenimientoService;

@RestController
@RequestMapping ("/InsumoMantenimiento")
public class InsumoMantenimientoController {

	@Autowired 
	private InsumoMantenimientoService insumoMantenimientoService; 
	
	@ResponseStatus (code = HttpStatus.OK)
	@GetMapping("/all")
	public ResponseEntity<List<InsumoMantenimientoDTO>> getInsumoMantenimiento() {
		List<InsumoMantenimientoDTO> result = insumoMantenimientoService.getInsumos();
		ResponseEntity<List<InsumoMantenimientoDTO>> insumos = new 	ResponseEntity<List<InsumoMantenimientoDTO>>(result, HttpStatus.OK);				
		return insumos;
		
	}
}
