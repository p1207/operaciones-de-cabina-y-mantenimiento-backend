package ar.com.pipelinetech.dedalo.config;

import javax.servlet.http.HttpServletRequest;
import org.hibernate.envers.RevisionListener;
import org.springframework.beans.factory.annotation.Autowired;
import ar.com.pipelinetech.dedalo.audit.Revision;

public class CustomRevisionListener implements RevisionListener{
	
	@Autowired 
	private HttpServletRequest request;
	
	public void newRevision (Object revisionEntity) {
		final Revision revision = (Revision) revisionEntity;
		revision.setUser(getUsuario());
		revision.setIp(getIp());
	}

	private String getIp() {
		return request.getRemoteHost();
	}

	public String getUsuario() {
		String user = "Usuario no identificado";
		if(request.getUserPrincipal()!= null) {
			user = request.getUserPrincipal().toString();
			return user;			
		}
		return user;
    }
}
