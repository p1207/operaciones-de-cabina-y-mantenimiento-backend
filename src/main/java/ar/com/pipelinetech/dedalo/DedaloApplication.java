package ar.com.pipelinetech.dedalo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class DedaloApplication {

	public static void main(String[] args) {
		SpringApplication.run(DedaloApplication.class, args);
	}


}
