package ar.com.pipelinetech.dedalo;

import java.net.URI;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ar.com.pipelinetech.dedalo.dto.IdPersonalDTO;


public class CallExternalServices {

	private static RestTemplate restTemplate = new RestTemplate();
	
	/* Post default Media Type */
	public static Object doPostService(String url, IdPersonalDTO request, MediaType mediaType, Class<?> clase) {
		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
			HttpEntity<IdPersonalDTO> entity = new HttpEntity<IdPersonalDTO>(request, httpHeaders);

			Object o = restTemplate.postForObject(url, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	public static Object doPostService(String url, String request, MediaType mediaType, Class<?> clase) {
		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
			HttpEntity<String> entity = new HttpEntity<String>(request, httpHeaders);

			Object o = restTemplate.postForObject(url, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	/* Post custom request */
	public static Object doPostService(String url, MultiValueMap<String, String> request, MediaType mediaType, Class<?> clase) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
			
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(request, httpHeaders);

			Object o = restTemplate.postForObject(url, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	/* Post custom request */
	public static Object doPostServiceInteger(String url, MultiValueMap<String, Integer> request, MediaType mediaType, Class<?> clase) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
			
			HttpEntity<MultiValueMap<String, Integer>> entity = new HttpEntity<MultiValueMap<String, Integer>>(request, httpHeaders);

			Object o = restTemplate.postForObject(url, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}

	/* Post custom Headers */
	public static Object doPostService(String url, String request, MultiValueMap<String, String> headers, Class<?> clase) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.addAll(headers);
			HttpEntity<String> entity = new HttpEntity<String>(request, httpHeaders);

			return restTemplate.postForObject(url, entity, clase);

		} catch (RestClientException e) {
			
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	/* Put custom Headers */
	public static Object doPutService(String url, String request, MultiValueMap<String, String> headers, Class<?> clase) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.addAll(headers);
			HttpEntity<String> entity = new HttpEntity<String>(request, httpHeaders);

			Object o = restTemplate.exchange(url, HttpMethod.PUT, entity, clase);
			return o;

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	/* Put custom Headers */
	public static Object doPutService2(String url, Map<String, ?> params, MediaType mediaType, ParameterizedTypeReference<?> clase) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
	
			HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);

			Object o = restTemplate.exchange(url, HttpMethod.PUT, entity, clase, params);
			return o;

		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	
	/* Delete custom Headers */
	public static Object doDeleteService(String url, MultiValueMap<String, String> headers, Class<?> clase) {
		
		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.addAll(headers);
			HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);

			Object o = restTemplate.exchange(url, HttpMethod.DELETE, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}

	public static Object doGetService(String url, Map<String, ?> params, MediaType mediaType, ParameterizedTypeReference<?> clase) {

		try {
			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(mediaType);
			HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
			
			Object o = restTemplate.exchange(url, HttpMethod.GET, entity, clase, params);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
//			throw new RestClientException(e.getMessage() +" : "+ Constants.ERROR_EN_SERVICIO_EXTERNO + " : url="+url);
			return null;
		}
	}

	public static Object doGetService(String url, MultiValueMap<String, String> headers, ParameterizedTypeReference<?> clase) {
		
		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.addAll(headers);
			HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);

			Object o = restTemplate.exchange(url, HttpMethod.GET, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}
	
	public static Object doGetService2(String url, MultiValueMap<String, String> headers, ParameterizedTypeReference<?> clase) {
		RestTemplate  restTemplate = new RestTemplate();

		restTemplate.getInterceptors().add((request, body, execution) -> {
            ClientHttpResponse response = execution.execute(request,body);
            response.getHeaders().setContentType(MediaType.APPLICATION_XML);
            return response;
        });
		
		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.addAll(headers);
			HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);

			Object o = restTemplate.exchange(url, HttpMethod.GET, entity, clase);
			return o;
		} catch (RestClientException e) {
			e.printStackTrace();
			throw new RestClientException(e.getMessage());
		}
	}

	public static Object doGetService(URI uri, Class<?> clase) {

		Object o = restTemplate.getForObject(uri, clase);

		return o;
	}

	public static RestTemplate getRestTemplate() {
		return restTemplate;
	}


	
	
}